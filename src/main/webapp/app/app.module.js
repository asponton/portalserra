(function() {
    'use strict';

    angular
        .module('plataformaserraApp', [
            'ngStorage', 
            'ngResource',
            'ngCookies',
            'ngAria',
            'ngCacheBuster',
            'ngFileUpload',
            'ui.bootstrap',
            'ui.bootstrap.datetimepicker',
            'ui.router',
            'infinite-scroll',
            // jhipster-needle-angularjs-add-module JHipster will add new module here
            'angular-loading-bar',
            'ui.calendar',
            'chart.js',
            'ui.utils.masks',
            'ng-fusioncharts',
            'isteven-multi-select',
            'ui-notification',
            'rzModule',
            'ngMask'
        ])
        .run(run);

    run.$inject = ['stateHandler'];

    function run(stateHandler) {
        stateHandler.initialize();
    }
})();
