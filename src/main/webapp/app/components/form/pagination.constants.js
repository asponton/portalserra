(function() {
    'use strict';

    angular
        .module('plataformaserraApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
