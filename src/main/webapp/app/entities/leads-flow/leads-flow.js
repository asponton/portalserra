(function() {
'use strict';

angular.module('plataformaserraApp').config(stateConfig);

stateConfig.$inject = ['$stateProvider'];

function stateConfig($stateProvider) {
        $stateProvider
            .state('leads-flow', {
                parent: 'entity',
                url: '/leads-flow',
                data: {
                    authorities: [],
                    pageTitle: 'Leads'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/leads-flow/leads-flow.html',
                        controller: 'LeadsFlowController'
                    }
                }
            })
            .state('leads-flow-dialog', {
                parent: 'leads-flow',
                url: '/{id}',
                data: {
                    authorities: ['DIRETOR','GERENTE','SUPERVISOR','VENDEDOR'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/leads-flow/leads-flow-dialog.html',
                        controller: 'LeadsFlowDialogController',
                        size: 'lg',
                        backdrop: 'static', 
                        keyboard: false,
                        resolve: {
                            entity: ['LeadId', function(Leads) {
                            		return Leads.get({id : $stateParams.id})
                            		}
                            	]
                        }
                    }).result.then(function(result) {
                        $state.go('leads-flow', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('leads-flow-cobranca', {
                parent: 'leads-flow',
                url: '/{id}',
                data: {
                    authorities: ['DIRETOR','GERENTE','SUPERVISOR','VENDEDOR'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/leads-flow/leads-flow-pendencias-usuario.html',
                        controller: 'LeadsFlowPendenciasUsuarioController',
                        size: 'lg',
                        backdrop: 'static', 
                        keyboard: false,
                        resolve: {
                            entity: ['Pendencias', function(Pendencias) {
                            		return Pendencias.get({userId : $stateParams.id})
                            		}
                            	]
                        }
                    }).result.then(function(result) {
                        $state.go('leads-flow', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    }
})();
