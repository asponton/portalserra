(function() {
'use strict';

angular
.module('plataformaserraApp')
.controller('LeadsFlowDialogController', LeadsFlowDialogController);

LeadsFlowDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', '$timeout', 'entity', 'Principal','$http','User'];

function LeadsFlowDialogController($scope, $stateParams, $uibModalInstance, $timeout, entity, Principal, $http, User) {       
        
        $scope.lead = entity;
        $scope.usuarios = [];
        if ($scope.lead.dataRetorno != null) {
        	$scope.lead.dataRetorno = new Date($scope.lead.dataRetorno);
        } else {
        	$scope.lead.dataRetorno = null;
        } 
        
        var autoridade;
        
		Principal.identity().then(function(account) {
            var authorityConta = account.authority.name;
            $scope.isManager = authorityConta == 'DIRETOR' || authorityConta == 'GERENTE' || authorityConta == 'SUPERVISOR';
            $scope.isVendedor = authorityConta == 'VENDEDOR';
            autoridade = authorityConta;
            $scope.usuarios = User.cargo({cargo : 'VENDEDOR'});
            
        });        
        
        var data; 
        
        $scope.tiposPlanos = ["INDIVIDUAL", "FAMILIAR", "EMPRESARIAL"]
        var passos = {};
        passos[1] = "CONTATO";
        passos[2] = "NEGOCIACAO";
        
        $scope.contato = function(){
        	$scope.passoAtual = passos[1];
        }
                
        $scope.negociacao = function(){
        	$scope.passoAtual = passos[2];
            if ($scope.lead.dataRetorno != null) {
            	$scope.lead.dataRetorno = new Date($scope.lead.dataRetorno);
            } else {
            	$scope.lead.dataRetorno = null;
            } 
        }
        
        var iniciarFluxo = function() {
        	$scope.contato();
        }
        
        iniciarFluxo();
        
        var onSaveSuccess = function (result) {
            $scope.isSaving = false;
            $uibModalInstance.close(result);
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };
        
        $scope.atribuir = function (responsavel) {
        	$scope.lead.responsavel = responsavel;
        	$scope.lead.fromAuthority = autoridade;
        } 

        $scope.salvar = function () {
            $scope.isSaving = true;
            data = angular.toJson($scope.lead);
            $http.put('api/lead/atualizar', data, {
	            headers: {
                    "Accept": "application/json"
	            }
	        }).success(function (response) {
	        	$scope.lead = response;
	        	$scope.lead.dataRetorno = new Date($scope.lead.dataRetorno);
	        	onSaveSuccess(true);
	        }).error(function (response) {
	        	onSaveError(true);
            });  
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
    
})();
