(function() {
'use strict';

angular
.module('plataformaserraApp')
.controller('LeadsFlowController', LeadsFlowController);

LeadsFlowController.$inject = ['$scope','Principal','ParseLinks','$http','Notificacoes'];

 function LeadsFlowController($scope, Principal, ParseLinks, $http, Notificacoes) {
        $scope.leads = [];
        var idConta;
        var authorityConta;
        
		Principal.identity().then(function(account) {
            $scope.currentAccount = account;
            idConta = account.idUser;
            authorityConta = account.cargo.name;
            $scope.notificacoes = Notificacoes.query({userId: idConta});
            var data = {idUser: idConta, cargo: authorityConta};

	        return $http.post('api/lead/pool/user', data, {
	            headers: {
                    "Accept": "application/json"
	            }
	        }).success(function (response) {
	        	$scope.leads = response;        	
	        });
	        
            
        });
        $scope.page = 1;
        $scope.loadAll = function () {};
        $scope.loadPage = function (page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.AtualizarLeads = function () {
            var data = {idUser: idConta, cargo: authorityConta};

	        return $http.post('api/lead/pool/user', data, {
	            headers: {
                    "Accept": "application/json"
	            }
	        }).success(function (response) {
	        	$scope.leads = response;        	
	        });
	        
        };
        
        $scope.BloquearVendedor = function (pendencia, indeterminado) {
            var data = angular.toJson(pendencia); 
            if (indeterminado) {
    	        return $http.post('api/users/bloquear/indeterminado', data, {
    	            headers: {
                        "Accept": "application/json"
    	            }
    	        }).success(function (response) {
    	        });
            } else {
		        return $http.post('api/users/bloquear', data, {
		            headers: {
	                    "Accept": "application/json"
		            }
		        }).success(function (response) {
		        });
            }
        };
        
        $scope.existemPendencias = function () {
        	if ($scope.notificacoes[0] != null) {
        		return $scope.notificacoes[0].statusPendencia == 'PENDENTE';
        	}
        };
        
        $scope.GerarJustificativa = function (pendencia) {
        	if (pendencia.mensagemJustificativa == null || pendencia.mensagemJustificativa == "" || pendencia.mensagemJustificativa.trim() == "") {
        		alert("Preencha sua justificativa!");
        		return;
        	}
            var data = angular.toJson(pendencia); 

	        return $http.post('api/lead/notificacoes/atualizar/pendencia', data, {
	            headers: {
                    "Accept": "application/json"
	            }
	        }).success(function (response) {
	        });        	
        };    
    }
})();
