(function() {
'use strict';

angular
.module('plataformaserraApp')
.controller('HistoricoController', HistoricoController);

HistoricoController.$inject = ['$scope', '$state', '$timeout', '$compile', '$uibModal', 'Auth', 'Principal', 'Repescagem', 'User'];

 function HistoricoController ($scope, $state, $timeout, $compile, $uibModal, Auth, Principal, Repescagem, User) {
        Principal.identity().then(function(account) {
        	
        	$scope.usuarios = User.cargo({cargo : 'VENDEDOR'});
        	
        	$scope.usuariosOutput = [];
        	
            $scope.atribuir = function (responsavel) {
            	$scope.responsavel = responsavel;
            } 
        	
        });
    }
})();