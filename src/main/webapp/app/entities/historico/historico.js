(function() {
'use strict';

angular.module('plataformaserraApp').config(stateConfig);

stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('historico', {
                parent: 'entity',
                url: '/historico',
                data: {
                    authorities: [],
                    pageTitle: 'historico'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/historico/historico.html',
                        controller: 'HistoricoController'
                    }
                }
            })
            .state('buscar-historico', {
                parent: 'historico',
                url: '/buscar/de/:inicio/ate/:fim/vendedor/:idUser',
                data: {
                    authorities: ['DIRETOR','GERENTE'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/historico/busca-historico.html',
                        controller: 'HistoricoBuscaController',
                        size: 'lg',
                        resolve: {
                        	entity: ['Historico', function(Historico) {
                            		return Historico.get({inicio: new Date($stateParams.inicio), fim: new Date($stateParams.fim), idUser: $stateParams.idUser})
                        		}
                        	]
                        }
                    }).result.then(function(result) {
                        $state.go('historico', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    }
})();