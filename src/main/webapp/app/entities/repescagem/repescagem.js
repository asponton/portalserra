(function() {
'use strict';

angular.module('plataformaserraApp').config(stateConfig);

stateConfig.$inject = ['$stateProvider'];

function stateConfig($stateProvider) {
        $stateProvider
            .state('repescagem', {
                parent: 'entity',
                url: '/repescagem',
                data: {
                    authorities: [],
                    pageTitle: 'repescagem'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/repescagem/repescagem.html',
                        controller: 'RepescagemController'
                    }
                }
            })
            .state('buscar', {
                parent: 'repescagem',
                url: '/buscar/de/:inicio/ate/:fim/status/:status',
                data: {
                    authorities: ['DIRETOR','GERENTE'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/repescagem/busca-repescagem.html',
                        controller: 'RepescagemBuscaController',
                        size: 'lg',
                        resolve: {
                        	entity: ['Repescagem', function(Repescagem) {
                            		return Repescagem.get({inicio: new Date($stateParams.inicio), fim: new Date($stateParams.fim), status: $stateParams.status})
                        		}
                        	]
                        }
                    }).result.then(function(result) {
                        $state.go('repescagem', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });

    }
    
})();
