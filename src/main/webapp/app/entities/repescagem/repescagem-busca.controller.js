'use strict';

angular.module('plataformaserraApp').controller('RepescagemBuscaController',
    ['$scope', '$stateParams', '$uibModalInstance', '$timeout', 'entity', 'User','Principal','Categoria','Time','$http',
        function($scope, $stateParams, $uibModalInstance, $timeout, entity, User, Principal, Categoria, Time, $http) {       
    	
    	$scope.leads = entity;
    	
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        
        $scope.DefinirComoRepescagem = function (leads) {
            var data = angular.toJson(leads);

	        return $http.put('api/lead/repescagem/aprovar', data, {
	            headers: {
                    "Accept": "application/json"
	            }
	        }).success(function (response) {
	        	alert("Leads atualizados para repescagem");
	        	$uibModalInstance.close(response);
	        });
	        
        };
    
    }]);
