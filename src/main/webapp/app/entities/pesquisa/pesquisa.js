(function() {
'use strict';

angular.module('plataformaserraApp').config(stateConfig);

stateConfig.$inject = ['$stateProvider'];

function stateConfig($stateProvider) {
        $stateProvider
	        .state('pesquisa', {
	            parent: 'site',
	            url: '/pesquisa/:idLead',
	            data: {
	                authorities: [],
	            },
	            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
	                $uibModal.open({
	                    templateUrl: 'app/entities/pesquisa/pesquisa.html',
	                    controller: 'PesquisaController', function ($stateParams) {
	                        expect($stateParams).toBe({leadId: idLead});
	                    },
	                    backdrop: 'static', 
	                    keyboard: false,
	                    size: 'lg'
	                }).result.then(function(result) {
	                    $state.go('pesquisa', null, { reload: true });
	                }, function() {
	                    $state.go('^');
	                })
	            }]
	        });
    }
})();