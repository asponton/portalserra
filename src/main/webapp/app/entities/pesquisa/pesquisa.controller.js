(function() {
'use strict';

angular
.module('plataformaserraApp')
.controller('PesquisaController', PesquisaController);

PesquisaController.$inject = ['$scope', '$stateParams', '$uibModalInstance', '$timeout','Principal','$http','User'];

    function PesquisaController($scope, $stateParams, $uibModalInstance, $timeout, Principal, $http, User) {
			
        $scope.otimo;
        $scope.normal;
        $scope.pessimo;
        
        $scope.mostrarRuim = function () {
    		$('#pessimo').show();
    		$('#otimo').hide();
    		$('#regular').hide();

        };
        
        $scope.mostrarRegular = function () {
    		$('#regular').show();
    		$('#pessimo').hide();
    		$('#otimo').hide();

        };
        
        $scope.mostrarOtimo = function () {
    		$('#otimo').show();
    		$('#pessimo').hide();
    		$('#regular').hide();
        };
        
        $scope.Salvar = function (idLead) {
        	var data;
        	
        	if ($scope.otimo != null) {
        		data = {motivo: $scope.otimo, tipoAtendimento: "OTIMO", idLead: $stateParams.idLead};
        	} else if ($scope.normal != null) {
        		data = {motivo: $scope.normal, tipoAtendimento: "NORMAL", idLead: $stateParams.idLead};
        	} else {
        		data = {motivo: $scope.pessimo, tipoAtendimento: "PESSIMO", idLead: $stateParams.idLead};
        	}

	        return $http.post('api/lead/pesquisa/user', data, {
	            headers: {
                    "Accept": "application/json"
	            }
	        }).success(function (response) {
	        	window.location.replace("http://serracorretora.com.br");     	
	        });
	        
        };
        
    }
    
})();
