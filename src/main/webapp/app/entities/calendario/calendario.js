(function() {
'use strict';

angular.module('plataformaserraApp').config(stateConfig);

stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('calendario', {
                parent: 'entity',
                url: '/calendario',
                data: {
                    authorities: [],
                    pageTitle: 'calendario'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/calendario/calendario.html',
                        controller: 'CalendarioController'
                    }
                }
            }).state('calendario-view', {
                parent: 'entity',
                url: '/calendario/view',
                data: {
                    authorities: [],
                    pageTitle: 'calendario.view'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/calendario/calendario-view.html',
                        controller: 'CalendarioController'
                    }
                }
            });
    }
})();
