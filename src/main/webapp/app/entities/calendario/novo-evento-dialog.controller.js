(function() {
'use strict';

	angular
	.module('plataformaserraApp')
    .controller('NovoEventoDialogController', NovoEventoDialogController);
	
	NovoEventoDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'evento', 'User', 'Language'];

    function NovoEventoDialogController($scope, $stateParams, $uibModalInstance, evento, User, Language) {

        $scope.evento = evento;
        $scope.authorities = ["ROLE_USER", "ROLE_ADMIN"];
        Language.getAll().then(function (languages) {
            $scope.languages = languages;
        });
        var sucesso = function (result) {
            $scope.salvando = false;
            $uibModalInstance.close(result);
        };

        var erro = function (result) {
            $scope.salvando = false;
        };

        $scope.salvar = function () {
            $scope.salvando = true;
            
            sucesso($scope.evento);
            
            if ($scope.user.id != null) {
                User.update($scope.user, onSaveSuccess, onSaveError);
            } else {
                User.save($scope.user, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('Cancelar');
        };
}

})();
