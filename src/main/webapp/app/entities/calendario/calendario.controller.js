(function() {
'use strict';

	angular
	.module('plataformaserraApp')
    .controller('CalendarioController', CalendarioController);
	
	CalendarioController.$inject = ['$scope', '$timeout', '$compile', '$uibModal', 'Auth', 'Principal', 'UserPlantao', '$http', 'User', 'Time', '$cookies'];
    
    function CalendarioController ($scope, $timeout, $compile, $uibModal, Auth, Principal, UserPlantao, $http, User, Time, $cookies) {
    	var resourcesResult;
    	var filtersResult;
    	var isAdmin;
    	var isUser;
    	var usuariosSelecionadosParaPlantao = [];
    	var timesSelecionadosParaPlantao = [];
    	var scheduler = $("#scheduler").data("kendoScheduler");
    	
    	
    	$scope.usuarios = [];
    	$scope.usuariosOutput = [];
    	$scope.timesOutput = [];
    	$scope.times = [];
    	$scope.aberturaMensal = [];
    	
        var passos = {};
        passos[1] = "CONFIGURACOES";
        passos[2] = "LISTA";
        
        $scope.configuracaoes = function(){
        	$scope.passoAtual = passos[1];
        }
    	
        var iniciarFluxo = function() {
        	$scope.configuracaoes();
        }
        
        iniciarFluxo();


    	
        Principal.identity().then(function(account) {
        	

        	
        	$scope.usuarios = User.cargo({cargo : 'VENDEDOR'});
        	$scope.times = Time.query();
        	
	        	if (account != null && !$('#foto').is(":visible")) {
	        		$('#fotoPadrao').hide();
	        		$('#foto').html('<img src="'+account.urlFotoPainel+'" />');
	        		$('#foto').show();
	        	}
	        	
	        	isAdmin = account.cargo.name == 'DIRETOR' || account.cargo.name == 'GERENTE' ? true : false;
	        	isUser = account.cargo.name == 'SUPERVISOR' || account.cargo.name == 'VENDEDOR' ? true : false;
	        	
	  		    UserPlantao.get({userId: account.idUser}, function(result) {
    		    	resourcesResult = jQuery.parseJSON(JSON.stringify(result.resourceDTO));
    		    	filtersResult = jQuery.parseJSON(JSON.stringify(result.filterDTO));
    		    });	  
	        	
				var data = new Date();
				  var mesAtual = data.getMonth()+1;
	    		  var hours = data.getHours();
	    		  var minutes = data.getMinutes();
	    		  var ampm = hours >= 12 ? 'PM' : 'AM';
	    		  hours = hours % 12;
	    		  hours = hours ? hours : 12; // the hour '0' should be '12'
	    		  minutes = minutes < 10 ? '0'+minutes : minutes;
	    		  var strTime = "07:00 AM";
	    		  var strTimeFinal = "06:00 PM";
	    		  var dataTimeStamp = data.getFullYear() + "/" + mesAtual + "/" + data.getDate() + " " + strTime;
	    		  var dataTimeFinal = data.getFullYear() + "/" + mesAtual + "/" + data.getDate() + " " + strTimeFinal;
	    		  var dataNormal = data.getFullYear() + "/" + mesAtual + "/" + data.getDate();


	    		  var baseUrl = '//kendo.cdn.telerik.com/2016.2.714/js/messages/kendo.messages.';
	    		    $.getScript(baseUrl + "pt-BR.min.js", function () {
	    		    	setTimeout(createScheduler, 2000);
	    		    });
	    		    
	    		    
		    function createScheduler() {
		    	
		    	if (scheduler != null) {
		    		scheduler.destroy();
		    	}
		    	
		    	$("#scheduler").empty();
		    	
		        var urlBase = "api/evento";
		        	
	    	    $("#scheduler").kendoScheduler({
	    	        date: new Date(dataNormal),
	    	        startTime: new Date(dataTimeStamp),
	    	        workDayEnd: new Date(dataTimeFinal),
	    	        height: 600,
	    	        views: [
	    	            "day",
	    	            "workWeek",
	    	            "week",
	    	            { type: "month", selected: isAdmin},
	    	            { type: "agenda", selected: isUser},
	    	            { type: "timeline", eventHeight: 50}
	    	        ],
	    	        timezone: "America/Sao_Paulo",
	    	        editable: {
	    	            "confirmation": "Você tem certeza que deseja excluir este plantão?",
	    	            "create": isAdmin,
	    	            "destroy": isAdmin,
	    	            "update": isAdmin,
	    	            "move": isAdmin,
	    	            "resize": isAdmin
	    	        },
	    	        messages: {
	    	            "today": "Hoje",
	    	            "save": "Salvar",
	    	            "cancel": "Cancelar",
	    	            "destroy": "Apagar",
	    	            "event": "Evento",
	    	            "date": "Data",
	    	            "time": "Horário",
	    	            "allDay": "Dia inteiro",
	    	            "showFullDay": "Mostrar 24h",
	    	            "showWorkDay": "Mostrar horário comercial",
	    	            "deleteWindowTitle": "Remover plantão",
	    	            "views": {
	    	                "day": "Dia",
	    	                "week": "Semana",
	    	                "month": "Mês"
	    	            }
    	            },
	    	        dataSource: {
    	        	   cache: false,
    	        	   sync: function() {
    	        		   this.read();
    	        	   },

	    	            transport: {
	    	                read: {
	    	                    url: urlBase+"/carregar/"+account.idUser,
    	                        type: "GET",
    	                        dataType: "json",
    	                        contentType: "application/json"
	    	                },
	    	                update: {
	    	                    url: urlBase+"/atualizar/evento",
	    	                    dataType: "json",
    	                        type: "PUT",
    	                        contentType: "application/json"
	    	                },
	    	                create: {
	    	                   url: urlBase+"/criar/evento",
	    	                   type: "POST",
	    	                   dataType: "json",
    	                       contentType: "application/json"
	    	                },
	    	                destroy: {
	    	                    url: urlBase+"/apagar/evento",
	    	                    dataType: "json",
    	                        type: "DELETE",
    	                        contentType: "application/json"
	    	                },
	    	                parameterMap: function(options, operation) {
	    	                    if (operation !== "read" && options.models) {
	    	                        return JSON.stringify(options).replace('{"models":[',"").replace("]}","");
	    	                    }
	    	                    if (operation !== "read" && !options.models) {
	    	                    	return JSON.stringify(options);
	    	                    }	    	                    
	    	                    return JSON.stringify(options);
	    	                }
	    	            },
	    	            schema: {
	    	                model: {
	    	                    id: "id",
	    	                    fields: {
	    	                        taskId: { from: "taskId", type: "number" },
	    	                        title: { from: "title", defaultValue: "", validation: { required: true } },
	    	                        start: { type: "date", from: "start" },
	    	                        end: { type: "date", from: "end" },
	    	                        startTimezone: { from: "startTimezone" },
	    	                        endTimezone: { from: "endTimezone" },
	    	                        description: { from: "description" },
	    	                        recurrenceId: { from: "recurrenceId" },
	    	                        recurrenceRule: { from: "recurrenceRule" },
	    	                        recurrenceException: { from: "recurrenceException" },
	    	                        ownerId: { from: "ownerId", validation: { required: true } },
	    	                        isAllDay: { type: "boolean", from: "isAllDay" }
	    	                    }
	    	                }
	    	            },
	    	            filter: {
	    	                logic: "or",
	    	                filters: filtersResult
	    	            }
	    	        },
	    	        resources: [
	    	            {
	    	                field: "ownerId",
	    	                title: "Usuário",
	    	                dataSource: resourcesResult
	    	            }
	    	        ]
	    	    });
		    };
	
	    	    $("#people :checkbox").change(function(e) {
	    	        var checked = $.map($("#people :checked"), function(checkbox) {
	    	            return parseInt($(checkbox).val());
	    	        });
	
	    	        var scheduler = $("#scheduler").data("kendoScheduler");
	
	    	        scheduler.dataSource.filter({
	    	            operator: function(task) {
	    	                return $.inArray(task.ownerId, checked) >= 0;
	    	            }
	    	        });
	    	    });
        });
        
        $scope.atribuir = function (responsavel) {
        	$("#desabilitarTime").click();
        	var indexUser = usuariosSelecionadosParaPlantao.indexOf(responsavel.idUser);
        	if (indexUser == -1) {
        		usuariosSelecionadosParaPlantao.push(responsavel.idUser);	
        	} else {
        		usuariosSelecionadosParaPlantao.splice(indexUser, 1);
        	}
        	timesSelecionadosParaPlantao = [];
        } 
        
        $scope.atribuirTime = function (time) {
        	$("#desabilitarUser").click();
        	var indexTime = timesSelecionadosParaPlantao.indexOf(time.id);
        	if (indexTime == -1) {
        		timesSelecionadosParaPlantao.push(time.id);	
        	} else {
        		timesSelecionadosParaPlantao.splice(indexTime, 1);
        	}
        	usuariosSelecionadosParaPlantao = [];
        }
        
        
        $scope.abrirMes = function() {
        	$scope.aberturaMensal.vendedores = usuariosSelecionadosParaPlantao;
        	$scope.aberturaMensal.times = timesSelecionadosParaPlantao;
        	
        	var data = {vendedores: $scope.aberturaMensal.vendedores, times: $scope.aberturaMensal.times, periodoInicial: $scope.aberturaMensal.periodoInicial, 
        				periodoFinal: $scope.aberturaMensal.periodoFinal};
        	
            $http.post('/api/evento/abrir-mes', data, {
	            headers: {
                    "Accept": "application/json"
	            }
	        }).success(function (response) {
	        	$scope.passoAtual = passos[2];
	        	$scope.plantoesUsuarios = angular.fromJson(response);
	        }); 
        }
        
        $scope.gerar = function() {
        	var dataJson = angular.toJson($scope.plantoesUsuarios);
            $http.post('/api/evento/iniciar-plantao-mensal', dataJson, {
	            headers: {
                    "Accept": "application/json"
	            }
	        }).success(function (response) {
	        	$("#irAgenda").click();
	        }); 
        }
        
        $scope.enviarEmail = function() {
            $http.get('/api/evento/plantao-mensal/notificar/corretores', {
	            headers: {
                    "Accept": "application/json"
	            }
	        }).success(function(response) {
	        }).error(function(response) {
	        });
        }             
        
    }

})();
