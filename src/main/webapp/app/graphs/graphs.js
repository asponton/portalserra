'use strict';

angular.module('plataformaserraApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('metas', {
                parent: 'site',
                url: '/metas',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/graphs/metas.html',
                        controller: 'GraphsController'
                    }
                }
            })
        .state('campeoes', {
            parent: 'site',
            url: '/campeoes',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'scripts/app/graphs/campeoes.html',
                    controller: 'GraphsController'
                }
            }
        })
        .state('tipos-de-planos', {
            parent: 'site',
            url: '/tipos-de-planos',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'scripts/app/graphs/tipo-planos.html',
                    controller: 'GraphsController'
                }
            }
        })
        .state('monitoramento', {
            parent: 'site',
            url: '/monitoramento',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'scripts/app/graphs/monitoramento.html',
                    controller: 'GraphsController'
                }
            }
        })
        .state('aproveitamento-geral', {
            parent: 'site',
            url: '/aproveitamento-geral',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'scripts/app/graphs/aproveitamento-geral.html',
                    controller: 'GraphsController'
                }
            }
        });        
    });
