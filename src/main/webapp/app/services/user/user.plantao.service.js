(function () {
    'use strict';

    angular
        .module('plataformaserraApp')
        .factory('UserPlantao', UserPlantao);

    UserPlantao.$inject = ['$resource'];

    function UserPlantao ($resource) {
        var service = $resource('api/users/plantonistas/{userId}', {userId:'@userId'}, {
	        'query': {method: 'GET', isArray: true},
	        'get': {
	            method: 'GET',
	            transformResponse: function (data) {
	                data = angular.fromJson(data);
	                return data;
	            }
	        }
	    });

        return service;
    }
})();
