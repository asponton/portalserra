(function () {
    'use strict';

    angular
        .module('plataformaserraApp')
        .factory('Time', Time);

    Time.$inject = ['$resource'];

    function Time ($resource) {
        var service = $resource('api/times', {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'save': { method:'POST' },
            'update': { method:'PUT' },
            'delete':{ method:'DELETE'}
        });

        return service;
    }
})();
