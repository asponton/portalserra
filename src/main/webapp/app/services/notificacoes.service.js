(function () {
    'use strict';

    angular
        .module('plataformaserraApp')
        .factory('Notificacoes', Notificacoes);

    Notificacoes.$inject = ['$resource'];
 function Notificacoes($resource) {
        var service = $resource('api/lead/notificacoes/{userId}', {userId:'@id'}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            }
        });
        return service;
    }
})();
