(function () {
    'use strict';

    angular
        .module('plataformaserraApp')
        .factory('Historico', Historico);

    Historico.$inject = ['$resource'];

    function Historico ($resource) {
        var service =  $resource('api/lead/historico/indicacoes', {inicio:'@inicio', fim:'@fim', idUser:'@idUser'}, {
        	'query': {method: 'get', isArray: true},        	
            'get': {
                method: 'GET', isArray: true,
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            }
        });
        return service;
    }
    
})();