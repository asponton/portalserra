(function () {
    'use strict';

    angular
        .module('plataformaserraApp')
        .factory('Lead', Lead);

    Lead.$inject = ['$resource'];

    function Lead ($resource) {
        var service = $resource('api/lead/aproveitamento', {}, {
        	'query': {method: 'GET', isArray: true},        	
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            }
        });
        return service;
    }
    
})();
