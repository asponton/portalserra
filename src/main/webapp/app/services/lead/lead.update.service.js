(function () {
    'use strict';

    angular
        .module('plataformaserraApp')
        .factory('LeadUpdate', LeadUpdate);

    LeadUpdate.$inject = ['$resource'];

    function LeadUpdate ($resource) {
        var service = $resource('api/lead/atualizar', {leadDTO:'@leadDTO'}, {
        	'query': {method: 'PUT', isArray: true},        	
            'put': {
                method: 'PUT',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            }
        });
        return service;
    }
    
})();