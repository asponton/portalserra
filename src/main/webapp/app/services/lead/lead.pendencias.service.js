(function () {
    'use strict';

    angular
        .module('plataformaserraApp')
        .factory('Pendencias', Pendencias);

    Pendencias.$inject = ['$resource'];

    function Pendencias ($resource) {
        var service = $resource('api/lead/notificacoes', {userId:'@userId'}, {
        	'query': {method: 'get', isArray: true},        	
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            }
        });
        return service;
    }
    
})();