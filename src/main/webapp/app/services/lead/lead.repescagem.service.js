(function () {
    'use strict';

    angular
        .module('plataformaserraApp')
        .factory('Repescagem', Repescagem);

    Repescagem.$inject = ['$resource'];

    function Repescagem ($resource) {
        var service = $resource('api/lead/repescagem/lista', {inicio:'@inicio', fim:'@fim', status:'@status'}, {
        	'query': {method: 'get', isArray: true},        	
            'get': {
                method: 'GET', isArray: true,
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            }
        });
        return service;
    }
    
})();