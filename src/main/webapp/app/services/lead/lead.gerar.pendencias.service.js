(function () {
    'use strict';

    angular
        .module('plataformaserraApp')
        .factory('GerarPendencia', GerarPendencia);

    GerarPendencia.$inject = ['$resource'];

    function GerarPendencia ($resource) {
        var service = $resource('api/lead/notificacoes/pendencia', {userId:'id'}, {
        	'query': {method: 'post', isArray: true},        	
            'post': {
                method: 'POST',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            }
        });
        return service;
    }
    
})();