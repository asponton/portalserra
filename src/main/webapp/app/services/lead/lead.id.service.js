(function () {
    'use strict';

    angular
        .module('plataformaserraApp')
        .factory('LeadId', LeadId);

    LeadId.$inject = ['$resource'];

    function LeadId ($resource) {
        var service = $resource('api/lead/pool/{id}', {id:'@id'}, {
        	'query': {method: 'GET', isArray: true},        	
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            }
        });
        return service;
    }
    
})();

