(function() {
    'use strict';

    angular
        .module('plataformaserraApp')
        .controller('UserManagementController', UserManagementController);

    UserManagementController.$inject = ['$scope', 'Principal', 'User', 'ParseLinks', 'AlertService', '$state', 'pagingParams', 'paginationConstants'];

    function UserManagementController($scope, Principal, User, ParseLinks, AlertService, $state, pagingParams, paginationConstants) {
        var vm = this;

        vm.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        vm.currentAccount = null;
        vm.languages = null;
        vm.loadAll = loadAll;
        vm.setActive = setActive;
        vm.users = [];
        vm.page = 1;
        vm.totalItems = null;
        vm.clear = clear;
        vm.links = null;
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.transition = transition;

        vm.loadAll();
        
        Principal.identity().then(function(account) {
            vm.currentAccount = account;
            if (!$('#foto').is(":visible")) {
	    		$('#fotoPadrao').hide();
	    		$('#foto').html('<img src="'+account.urlFotoPainel+'" />');
	    		$('#foto').show();
            }
        });

        function setActive (user, isActivated) {
            user.activated = isActivated;
            User.update(user, function () {
                vm.loadAll();
                vm.clear();
            });
        }

        function loadAll () {
            User.query({
                page: pagingParams.page - 1,
                size: vm.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);
        }

        function onSuccess(data, headers) {
            //hide anonymous user from user management: it's a required user for Spring Security
            var hiddenUsersSize = 0;
            for (var i in data) {
                if (data[i]['login'] === 'anonymoususer') {
                    data.splice(i, 1);
                    hiddenUsersSize++;
                }
            }
            vm.links = ParseLinks.parse(headers('link'));
            vm.totalItems = headers('X-Total-Count') - hiddenUsersSize;
            vm.queryCount = vm.totalItems;
            vm.page = pagingParams.page;
            vm.users = data;
        }

        function onError(error) {
            AlertService.error(error.data.message);
        }

        function clear () {
            vm.user = {
                id: null, idUser:null, login: null, firstName: null, lastName: null, email: null,
                activated: null, langKey: null, createdBy: null, createdDate: null,
                lastModifiedBy: null, lastModifiedDate: null, resetDate: null,
                resetKey: null, authorities: null
            };
        }
        
        $scope.carregarFoto = function (user) {
    	    cloudinary.openUploadWidget({ cloud_name: 'asapps', upload_preset: 'hougfxji', theme: 'white',
    	    							  text: {
    	    								  "powered_by_cloudinary": "Powered by Cloudinary - Image management in the cloud",
    	    								  "sources.local.title": "Meus arquivos",
    	    								  "sources.local.drop_file": "Arraste aqui",
    	    								  "sources.local.drop_files": "Arraste seus arquivos aqui",
    	    								  "sources.local.drop_or": "Ou",
    	    								  "sources.local.select_file": "Selecione Arquivo",
    	    								  "sources.local.select_files": "Selecione Arquivos",
    	    								  "sources.url.title": "Endereço na Web",
    	    								  "sources.url.note": "Url pública de uma imagem:",
    	    								  "sources.url.upload": "Carregar",
    	    								  "sources.url.error": "Por favor um URL HTTP válida.",
    	    								  "sources.camera.title": "Câmera",
    	    								  "sources.camera.note": "Certifique-se que seu browser permita o uso de câmera, posicione-se e tire a foto:",
    	    								  "sources.camera.capture": "Capturar",
    	    								  "progress.uploading": "Carregando...",
    	    								  "progress.upload_cropped": "Carregar",
    	    								  "progress.processing": "Processando...",
    	    								  "progress.retry_upload": "Tente novamente",
    	    								  "progress.use_succeeded": "OK",
    	    								  "progress.failed_note": "Algumas das imagens não foram carregadas."
    	    								}
    	    							}, 
    	    	      function(error, result) {
    	    			if (error == null) {					
	    	    			user.urlFoto = result[0].eager[0].url;
	    	    			user.urlFotoPainel = result[0].eager[1].url;
	    	    			console.log(error, result);
	    	                User.update(user, function () {
	    	                    vm.loadAll();
	    	                    if (vm.currentAccount.cpf == user.cpf) {
	    	                    	$('#foto').html('<img src="'+result[0].eager[1].url+'" />');
	    	                    }
	    	                });
    	    			}
	    			  });
    	    
    	    
        };            

        function sort () {
            var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
            if (vm.predicate !== 'id') {
                result.push('id');
            }
            return result;
        }

        function loadPage (page) {
            vm.page = page;
            vm.transition();
        }

        function transition () {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }
    }
})();
