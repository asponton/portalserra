(function() {
    'use strict';

    angular
        .module('plataformaserraApp')
        .controller('UserManagementDialogController',UserManagementDialogController);

    UserManagementDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', '$timeout', 'entity', 'User', 'Time','Principal'];

    function UserManagementDialogController ($scope, $stateParams, $uibModalInstance, $timeout, entity, User, Time, Principal) {
    	
    	var quantidadeLeadsIndividual;
    	var quantidadeLeadsFamiliar;
    	var quantidadeLeadsPme;
    	var quantidadeLeadsEmpresarial;
    	var rangeMinimo;
    	var rangeMaximo;
    	
        var vm = this;

        vm.clear = clear;
        vm.languages = null;
        vm.save = save;
        
        Time.query(function (result) {
        	vm.times = result;
	    });
        
        var definirCargos = function() {
        	vm.cargos = ["DIRETOR","GERENTE","SUPERVISOR","VENDEDOR","GESTOR","OPERADOR_CONTRATO","ENCARREGADO_ALMOXARIFADO","ENTREGADOR_ALMOXARIFADO","OPERADOR_SCANNER","SUPORTE"];
        }
        
        var definirAuthorities = function(){
        	Principal.hasAuthority("GERENCIA_INTERNA").then(function (result) {
        		if(result){
        			vm.authorities = ["VENDAS"];
        		}
        	});
        	
        	Principal.hasAuthority("DIRETORIA").then(function (result) {
        		if(result){
        			vm.authorities = ["DIRETORIA", "GERENCIA_INTERNA", "GERENCIA_EXTERNA", "SUPERVISAO", 
            		                      "SUPORTE", "MARKETING", "VENDAS"];
        		}
        	});        	
        };
        
        var carregarAtribuicoesAutomaticas = function(){

        	$scope.individual = {
                    value: quantidadeLeadsIndividual,
                    options: {
                        ceil: 20,
                        floor: 0,
                        showTicksValues: true,
                        ticksValuesTooltip: function (v) {
                            return 'Tooltip for ' + v;
                        }
                    }
                };
            
        	$scope.familiar = {
                    value: quantidadeLeadsFamiliar,
                    options: {
                        ceil: 20,
                        floor: 0,
                        showTicksValues: true,
                        ticksValuesTooltip: function (v) {
                            return 'Tooltip for ' + v;
                        }
                    }
                };
            
        	$scope.pme = {
                    value: quantidadeLeadsPme,
                    options: {
                        ceil: 20,
                        floor: 0,
                        showTicksValues: true,
                        ticksValuesTooltip: function (v) {
                            return 'Tooltip for ' + v;
                        }
                    }
                };
            
        	$scope.empresarial = {
                    value: quantidadeLeadsEmpresarial,
                    options: {
                        ceil: 20,
                        floor: 0,
                        showTicksValues: true,
                        ticksValuesTooltip: function (v) {
                            return 'Tooltip for ' + v;
                        }
                    }
                };
            
        	$scope.range = {
                    minValue: rangeMinimo,
                    maxValue: rangeMaximo,
                    options: {
                        ceil: 99,
                        floor: 0,
                        draggableRange: true
                    }
                };        
            
        	$scope.$on("slideEnded", function() {
            	vm.user.limiteLeads = $scope.individual.value + $scope.familiar.value + $scope.pme.value + $scope.empresarial.value;
            	vm.user.quantidadeLeadsIndividual = $scope.individual.value;
            	vm.user.quantidadeLeadsFamiliar = $scope.familiar.value;
            	vm.user.quantidadeLeadsPme = $scope.pme.value;
            	vm.user.quantidadeLeadsEmpresarial = $scope.empresarial.value;
            	vm.user.rangeDddMinimo = $scope.range.minValue;
            	vm.user.rangeDddMaximo = $scope.range.maxValue;
           });
        	
        };
        
        
        definirCargos();
        definirAuthorities();
        
        
        if(entity){
    		User.get({login : entity}, function(result){
					vm.user = result
					quantidadeLeadsIndividual = vm.user.quantidadeLeadsIndividual;
					quantidadeLeadsFamiliar = vm.user.quantidadeLeadsFamiliar;
					quantidadeLeadsPme = vm.user.quantidadeLeadsPme;
					quantidadeLeadsEmpresarial = vm.user.quantidadeLeadsEmpresarial;
					rangeMinimo = vm.user.rangeDddMinimo;
					rangeMaximo = vm.user.rangeDddMaximo;
					vm.user.limiteLeads = quantidadeLeadsIndividual + quantidadeLeadsFamiliar + quantidadeLeadsPme + quantidadeLeadsEmpresarial;
					vm.prepararResponsaveis();
					carregarAtribuicoesAutomaticas();
					$timeout(function () { $scope.$broadcast('rzSliderForceRender'); },170);
				});
    		
    	} else {
    		vm.user = {
                    id: null, login: null, nome: null, cpf: null, email: null,
                    categoria: null, time: null, contatos: null, responsavel: null,
                    ativo: true, createdBy: null, createdDate: null,
                    contatos:[],
                    lastModifiedBy: null, lastModifiedDate: null, resetDate: null,
                    resetKey: null, authority: null, quantidadeLeadsIndividual: 0,
                    quantidadeLeadsFamiliar: 0, quantidadeLeadsPme: 0, quantidadeLeadsEmpresarial: 0,
                    rangeDddMinimo: 0, rangeDddMaximo:0
                };
			rangeMinimo = vm.user.rangeDddMinimo;
			rangeMaximo = vm.user.rangeDddMaximo;
    		carregarAtribuicoesAutomaticas();
    		$timeout(function () { $scope.$broadcast('rzSliderForceRender'); },170);
    	}    	

        vm.prepararResponsaveis = function(){
        	if(vm.user.cargo) {
        		var cargo = vm.user.cargo.name       	
            	
            	if(cargo != 'VENDEDOR'){
            		var responsaveis = [];
            		User.cargo({cargo: 'GERENTE'}, function(result) {
            			Array.prototype.push.apply(responsaveis, result);
                    });
            		
            		if(cargo == 'VENDEDOR'){
            			User.cargo({cargo: 'SUPERVISOR'}, function(result) {
            				Array.prototype.push.apply(responsaveis, result);
                        });
            		}
            		
            		vm.usuariosResponsaveis = responsaveis;
            	}
        	}
        }        
        
        
        var telefone = {id: null, valor: null, tipo: 'CELULAR'}
        vm.adicionarContato = function() {
        	vm.user.contatos.push(angular.copy(telefone))
        }
        
        vm.removerContato = function(index) {
        	alert(index)
        	vm.user.contatos.splice(index, 1)
        }        

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function onSaveSuccess (result) {
            vm.isSaving = false;
            $uibModalInstance.close(result);
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        function save () {
            vm.isSaving = true;
            if (vm.user.id !== null) {
                User.update(vm.user, onSaveSuccess, onSaveError);
            } else {
                vm.user.langKey = 'en';
                User.save(vm.user, onSaveSuccess, onSaveError);
            }
        }
    }
})();
