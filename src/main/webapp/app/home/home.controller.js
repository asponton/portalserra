(function() {
    'use strict';

    angular
        .module('plataformaserraApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'Principal', 'LoginService', '$state', 'Lead', 'Notification', 'Notificacoes'];

    function HomeController ($scope, Principal, LoginService, $state, Lead, Notification, Notificacoes) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
                
                $scope.onlyGraphs = function () {
                	return false;
                };
                
            	$(document).ready(function(){
            	  	$(document).bind("contextmenu",function(e){
            			return false;	
            	  	});
            	});
                
            	if (account != null && !$('#foto').is(":visible")) {
            		$('#fotoPadrao').hide();
            		$('#foto').html('<img src="'+account.urlFotoPainel+'" />');
            		$('#foto').show();
            	}
            	
              	var logado = Principal.isAuthenticated();
            	var pendencias;
            	var timeUser;
            	
            	var multiLevelGraphIntegrantesTime1;
            	var multiLevelGraphIntegrantesTime2;
            	var multiLevelGraphIntegrantesTime3;
            	var multiLevelGraphIntegrantesTime4;
            	var multiLevelGraphIntegrantesTime5;
            	var multiLevelGraphIntegrantesTime;
            	
            	var dadosGraficoGaugeTime1;
            	var dadosGraficoGaugeTime2;
            	var dadosGraficoGaugeTime3;
            	var dadosGraficoGaugeTime4;
            	var dadosGraficoGaugeTime5;   	
            	var dadosGraficoGaugeTime;            	
            	
                $scope.isAuthenticated = Principal.isAuthenticated;
                $scope.deco = {};
                $scope.lead = {};
                
                $scope.load = function () {
    	            Lead.get(function(result) {
    	                console.log("Atualizando Gráfico");
    	                
    	                
    	                multiLevelGraphIntegrantesTime1 = jQuery.parseJSON(JSON.stringify(result.dadosGraficoMultiLevelTime1));
    	                multiLevelGraphIntegrantesTime2 = jQuery.parseJSON(JSON.stringify(result.dadosGraficoMultiLevelTime2));
    	                multiLevelGraphIntegrantesTime3 = jQuery.parseJSON(JSON.stringify(result.dadosGraficoMultiLevelTime3));
    	                multiLevelGraphIntegrantesTime4 = jQuery.parseJSON(JSON.stringify(result.dadosGraficoMultiLevelTime4));
    	                multiLevelGraphIntegrantesTime5 = jQuery.parseJSON(JSON.stringify(result.dadosGraficoMultiLevelTime5));
    	                
    	                dadosGraficoGaugeTime1 = jQuery.parseJSON(JSON.stringify(result.dadosGraficoGaugeTime1));
    	                dadosGraficoGaugeTime2 = jQuery.parseJSON(JSON.stringify(result.dadosGraficoGaugeTime2));
    	                dadosGraficoGaugeTime3 = jQuery.parseJSON(JSON.stringify(result.dadosGraficoGaugeTime3));
    	                dadosGraficoGaugeTime4 = jQuery.parseJSON(JSON.stringify(result.dadosGraficoGaugeTime4));
    	                dadosGraficoGaugeTime5 = jQuery.parseJSON(JSON.stringify(result.dadosGraficoGaugeTime5));
    	                
    	                
    	                $scope.type = 'DoughnutCtrl';
    	                $scope.labels = [result.vendasTimePorPeriodo[0].nomeTime, result.vendasTimePorPeriodo[1].nomeTime, result.vendasTimePorPeriodo[2].nomeTime, result.vendasTimePorPeriodo[3].nomeTime, result.vendasTimePorPeriodo[4].nomeTime];
    	                $scope.data = [result.vendasTimePorPeriodo[0].valorTimeMensal, result.vendasTimePorPeriodo[1].valorTimeMensal, result.vendasTimePorPeriodo[2].valorTimeMensal, result.vendasTimePorPeriodo[3].valorTimeMensal, result.vendasTimePorPeriodo[4].valorTimeMensal];
    	                
    	                $scope.labels2 = result.emNegociacaoPorPeriodo.mes;
    	                $scope.series2 = ['Vendidos', "Perdidos", "Em negociação"];
    	                $scope.data2 = [
    	                  result.vendidosPorPeriodo.valor,
    	                  result.perdidosPorPeriodo.valor,
    	                  result.emNegociacaoPorPeriodo.valor
    	                ];
    	                $scope.colours = ['#00ff00','#ff0000','#6666ff'];
    	                
    	                
    	                if(vm.account.cargo.name == 'SUPERVISOR' || vm.account.cargo.name == 'VENDEDOR') {
    	                	if (result.vendasTimePorPeriodo[0].nomeTime == vm.account.time.nome) {
    	                		timeUser = result.vendasTimePorPeriodo[0];
    	                		multiLevelGraphIntegrantesTime = multiLevelGraphIntegrantesTime1;
    	                		dadosGraficoGaugeTime = dadosGraficoGaugeTime1;
    	                	}
    	                	
    	                	if (result.vendasTimePorPeriodo[1].nomeTime == vm.account.time.nome) {
    	                		timeUser = result.vendasTimePorPeriodo[1];
    	                		multiLevelGraphIntegrantesTime = multiLevelGraphIntegrantesTime2;
    	                		dadosGraficoGaugeTime = dadosGraficoGaugeTime2;
    	                	} 
    	
    	                	if (result.vendasTimePorPeriodo[2].nomeTime == vm.account.time.nome) {
    	                		timeUser = result.vendasTimePorPeriodo[2];
    	                		multiLevelGraphIntegrantesTime = multiLevelGraphIntegrantesTime3;
    	                		dadosGraficoGaugeTime = dadosGraficoGaugeTime3;
    	                	} 
    	
    	                	if (result.vendasTimePorPeriodo[3].nomeTime == vm.account.time.nome) {
    	                		timeUser = result.vendasTimePorPeriodo[3];
    	                		multiLevelGraphIntegrantesTime = multiLevelGraphIntegrantesTime4;
    	                		dadosGraficoGaugeTime = dadosGraficoGaugeTime4;
    	                	} 
    	
    	                	if (result.vendasTimePorPeriodo[4].nomeTime == vm.account.time.nome) {
    	                		timeUser = result.vendasTimePorPeriodo[4];
    	                		multiLevelGraphIntegrantesTime = multiLevelGraphIntegrantesTime5;
    	                		dadosGraficoGaugeTime = dadosGraficoGaugeTime5;
    	                	} 
    	            	}
                    	
    	                FusionCharts.ready(function () {
    	                    var ratingsChart = new FusionCharts({
    	                        type: 'line',
    	                        renderAt: 'chart-top-vendedores',
    	                        width: '500',
    	                        height: '290',
    	                        dataFormat: 'json',
    	                        dataSource: {
    	                            "chart": {
    	                                "xAxisName": "Mês",
    	                                "yAxisName": "Vendas",
    	                                "yaxisminvalue": "0",
    	                                "yaxismaxvalue": "10",
    	                                "yAxisValuesPadding": "100",
    	                                "valuePosition" : "below",
    	                                "numDivlines": "5",
    	                                "lineAlpha": "1",
    	                                "anchorAlpha": "100",
    	                                "bgcolor" : "#FFFFFF",
    	                                "borderColor": "#FFFFFF",	 	                                
    	                                //Theme
    	                                "theme":"fint"
    	                            },
    	                            "data": [
    	                                {
    	                                    "label": result.vendidosPorPeriodo.mes[0],
    	                                    "value": result.vendidosPorPeriodo.campeoesDoMes[0] == null ? "0" : result.vendidosPorPeriodo.campeoesDoMes[0].percentualAtingido,
    	                                    "displayValue": result.vendidosPorPeriodo.campeoesDoMes[0] == null ? "?" : result.vendidosPorPeriodo.campeoesDoMes[0].nome + ": " + result.vendidosPorPeriodo.campeoesDoMes[0].percentualAtingido + "%",
    	                                    "tooltext": result.vendidosPorPeriodo.campeoesDoMes[0] == null ? "?" : result.vendidosPorPeriodo.campeoesDoMes[0].nome + ": " + result.vendidosPorPeriodo.campeoesDoMes[0].percentualAtingido + "%",
    	                                    "anchorImageUrl": result.vendidosPorPeriodo.campeoesDoMes[0] == null ? "" : result.vendidosPorPeriodo.campeoesDoMes[0].urlFotoPainel
    	                                    
    	                                }, 
    	                                {
    	                                    "label": result.vendidosPorPeriodo.mes[1],
    	                                    "value": result.vendidosPorPeriodo.campeoesDoMes[1] == null ? "0" : result.vendidosPorPeriodo.campeoesDoMes[1].percentualAtingido,
    	                                    "displayValue": result.vendidosPorPeriodo.campeoesDoMes[1] == null ? "?" : result.vendidosPorPeriodo.campeoesDoMes[1].nome + ": " + result.vendidosPorPeriodo.campeoesDoMes[1].percentualAtingido + "%",
    	                                    "tooltext": result.vendidosPorPeriodo.campeoesDoMes[1] == null ? "?" : result.vendidosPorPeriodo.campeoesDoMes[1].nome + ": " + result.vendidosPorPeriodo.campeoesDoMes[1].percentualAtingido + "%",
    	                                    "anchorImageUrl": result.vendidosPorPeriodo.campeoesDoMes[1] == null ? "" : result.vendidosPorPeriodo.campeoesDoMes[1].urlFotoPainel
    	                                }, 
    	                                {
    	                                    "label": result.vendidosPorPeriodo.mes[2],
    	                                    "value": result.vendidosPorPeriodo.campeoesDoMes[2] == null ? "0" : result.vendidosPorPeriodo.campeoesDoMes[2].percentualAtingido,
    	                                    "displayValue": result.vendidosPorPeriodo.campeoesDoMes[2] == null ? "?" : result.vendidosPorPeriodo.campeoesDoMes[2].nome + ": " + result.vendidosPorPeriodo.campeoesDoMes[2].percentualAtingido + "%",
    	                                    "tooltext": result.vendidosPorPeriodo.campeoesDoMes[2] == null ? "?" : result.vendidosPorPeriodo.campeoesDoMes[2].nome + ": " + result.vendidosPorPeriodo.campeoesDoMes[2].percentualAtingido + "%",
    	                                    "anchorImageUrl": result.vendidosPorPeriodo.campeoesDoMes[2] == null ? "" : result.vendidosPorPeriodo.campeoesDoMes[2].urlFotoPainel
    	                                }, 
    	                                {
    	                                    "label": result.vendidosPorPeriodo.mes[3],
    	                                    "value": result.vendidosPorPeriodo.campeoesDoMes[3] == null ? "0" : result.vendidosPorPeriodo.campeoesDoMes[3].percentualAtingido,
    	                                    "displayValue": result.vendidosPorPeriodo.campeoesDoMes[3] == null ? "?" : result.vendidosPorPeriodo.campeoesDoMes[3].nome + ": " + result.vendidosPorPeriodo.campeoesDoMes[3].percentualAtingido + "%",
    	                                    "tooltext": result.vendidosPorPeriodo.campeoesDoMes[3] == null ? "?" : result.vendidosPorPeriodo.campeoesDoMes[3].nome + ": " + result.vendidosPorPeriodo.campeoesDoMes[3].percentualAtingido + "%",
    	                                    "anchorImageUrl": result.vendidosPorPeriodo.campeoesDoMes[3] == null ? "" : result.vendidosPorPeriodo.campeoesDoMes[3].urlFotoPainel
    	                                }, 
    	                                {
    	                                    "label": result.vendidosPorPeriodo.mes[4],
    	                                    "value": result.vendidosPorPeriodo.campeoesDoMes[4] == null ? "0" : result.vendidosPorPeriodo.campeoesDoMes[4].percentualAtingido,
    	                                    "displayValue": result.vendidosPorPeriodo.campeoesDoMes[4] == null ? "?" : result.vendidosPorPeriodo.campeoesDoMes[4].nome + ": " + result.vendidosPorPeriodo.campeoesDoMes[4].percentualAtingido + "%",
    	                                    "tooltext": result.vendidosPorPeriodo.campeoesDoMes[4] == null ? "?" : result.vendidosPorPeriodo.campeoesDoMes[4].nome + ": " + result.vendidosPorPeriodo.campeoesDoMes[4].percentualAtingido + "%",
    	                                    "anchorImageUrl": result.vendidosPorPeriodo.campeoesDoMes[4] == null ? "" : result.vendidosPorPeriodo.campeoesDoMes[4].urlFotoPainel
    	                                }, 
    	                                {
    	                                    "label": result.vendidosPorPeriodo.mes[5],
    	                                    "value": result.vendidosPorPeriodo.campeoesDoMes[5] == null ? "0" : result.vendidosPorPeriodo.campeoesDoMes[5].percentualAtingido,
                                    		"displayValue": result.vendidosPorPeriodo.campeoesDoMes[5] == null ? "?" : result.vendidosPorPeriodo.campeoesDoMes[5].nome + ": " + result.vendidosPorPeriodo.campeoesDoMes[5].percentualAtingido + "%",
    	                                    "tooltext": result.vendidosPorPeriodo.campeoesDoMes[5] == null ? "?" : result.vendidosPorPeriodo.campeoesDoMes[5].nome + ": " + result.vendidosPorPeriodo.campeoesDoMes[5].percentualAtingido + "%",
    	                                    "anchorImageUrl": result.vendidosPorPeriodo.campeoesDoMes[5] == null ? "" : result.vendidosPorPeriodo.campeoesDoMes[5].urlFotoPainel
    	                                }
    	                            ]
    	                        }
    	                    });

    	                    ratingsChart.render();
    	                });	                	                
                    	
    	                
    	                
    	                if (vm.account.cargo.name == 'DIRETOR' || vm.account.cargo.name == 'GERENTE') {
    		                FusionCharts.ready(function () {
    		                    var cpuGauge = new FusionCharts({
    		                        type: 'hlineargauge',
    		                        renderAt: 'chart-gauge',
    		                        width: '500',
    		                        height: '290',
    		                        dataFormat: 'json',
    		                        dataSource: {
    		                            "chart": {
    		                                "theme": "fint",
    		                                "subcaptionFontBold": "0",
    		                                "lowerLimit": "0",
    		                                "upperLimit": "100",
    		                                "numberSuffix": "%",
    		                                "valueAbovePointer": "0",
    		                                "chartBottomMargin": "20",  
    		                                "valueFontSize": "11",
    		                                "bgcolor" : "#FFFFFF",
    		                                "borderColor": "#FFFFFF",	                                
    		                                "valueFontBold": "0"  
    		                            },
    		                            "colorRange": {
    		                                "color": [
    		                                    {
    		                                        "minValue": "0",
    		                                        "maxValue": "35",
    		                                        "label": "Péssimo",
    		                                        "code": "#ff3535"
    		                                    }, 
    		                                    {
    		                                        "minValue": "35",
    		                                        "maxValue": "80",
    		                                        "label": "Regular",
    		                                        "code": "#ffdb00"
    		                                    }, 
    		                                    {
    		                                        "minValue": "80",
    		                                        "maxValue": "100",
    		                                        "label": "Ótimo",
    		                                        "code": "#00FF38"
    		                                    }
    		                                ]
    		                            },
    		                            "pointers": {
    		                                //Multiple pointers defined here
    		                                "pointer": [
    		                                    {
    		                                        "value": result.vendasTimePorPeriodo[0].percentualAtingido,
    		                                        "bgColor": "#0044DD",
    		                                        "bgAlpha": "80",
    		                                        "tooltext": result.vendasTimePorPeriodo[0].nomeTime + ": $value%"
    		                                    },
    		                                    {
    		                                        "value": result.vendasTimePorPeriodo[1].percentualAtingido,
    		                                        "bgColor": "#0077CC",
    		                                        "bgAlpha": "80",
    		                                        "tooltext": result.vendasTimePorPeriodo[1].nomeTime + ": $value%"
    		                                    },
    		                                    {
    		                                        "value": result.vendasTimePorPeriodo[2].percentualAtingido,
    		                                        "bgColor": "#0077CC",
    		                                        "bgAlpha": "80",
    		                                        "tooltext": result.vendasTimePorPeriodo[2].nomeTime + ": $value%"
    		                                    },
    		                                    {
    		                                        "value": result.vendasTimePorPeriodo[3].percentualAtingido,
    		                                        "bgColor": "#0077CC",
    		                                        "bgAlpha": "80",
    		                                        "tooltext": result.vendasTimePorPeriodo[3].nomeTime + ": $value%"
    		                                    },
    		                                    {
    		                                        "value": result.vendasTimePorPeriodo[4].percentualAtingido,
    		                                        "bgColor": "#0077CC",
    		                                        "bgAlpha": "80",
    		                                        "tooltext": result.vendasTimePorPeriodo[4].nomeTime + ": $value%"
    		                                    }
    		                                ]
    		                            }
    		                        }
    		                    })
    		                    .render();
    		                });
    	                }
    	                
    	                if (vm.account.cargo.name == 'SUPERVISOR' || vm.account.cargo.name == 'VENDEDOR') {
    		                FusionCharts.ready(function () {
    		                    var cpuGauge = new FusionCharts({
    		                        type: 'hlineargauge',
    		                        renderAt: 'chart-gauge-supervisao',
    		                        width: '500',
    		                        height: '290',
    		                        dataFormat: 'json',
    		                        dataSource: {
    		                            "chart": {
    		                                "theme": "fint",
    		                                "subcaptionFontBold": "0",
    		                                "lowerLimit": "0",
    		                                "upperLimit": "100",
    		                                "numberSuffix": "%",
    		                                "valueAbovePointer": "0",
    		                                "chartBottomMargin": "20",  
    		                                "valueFontSize": "11",
    		                                "bgcolor" : "#FFFFFF",
    		                                "borderColor": "#FFFFFF",	                                
    		                                "valueFontBold": "0"  
    		                            },
    		                            "colorRange": {
    		                                "color": [
    		                                    {
    		                                        "minValue": "0",
    		                                        "maxValue": "35",
    		                                        "label": "Péssimo",
    		                                        "code": "#ff3535"
    		                                    }, 
    		                                    {
    		                                        "minValue": "35",
    		                                        "maxValue": "80",
    		                                        "label": "Regular",
    		                                        "code": "#ffdb00"
    		                                    }, 
    		                                    {
    		                                        "minValue": "80",
    		                                        "maxValue": "100",
    		                                        "label": "Ótimo",
    		                                        "code": "#00FF38"
    		                                    }
    		                                ]
    		                            },
    		                            "pointers": {
    		                                //Multiple pointers defined here
    		                                "pointer": dadosGraficoGaugeTime
    		                            }
    		                        }
    		                    })
    		                    .render();
    		                });
    	                }
    	                
    	                
    	                if (vm.account.cargo.name == 'DIRETOR' || vm.account.cargo.name == 'GERENTE') {
    		                FusionCharts.ready(function() {
    		                    var revenueChart = new FusionCharts({
    		                        type: 'bar3d',
    		                        renderAt: 'chart-tipoPlanos',
    		                        width: '500',
    		                        height: '290',
    		                        dataFormat: 'json',
    		                        dataSource: {
    		                            "chart": {
    		                                "alignCaptionWithCanvas": "0",
    		                                "canvasBgAlpha": "0",
    		                                //Theme  
    		                                "theme" : "fint"
    		                            },
    		                            "data": [
    		                                {
    		                                    "label": "Individual",
    		                                    "value": result.resultadoPlanoIndividual.total,
    		                                    "color": "#008ee4"
    		                                }, 
    		                                {
    		                                    "label": "Familiar",
    		                                    "value": result.resultadoPlanoFamiliar.total,
    		                                    "color": "#ff6600"
    		                                }, 
    		                                {
    		                                    "label": "Pme",
    		                                    "value": result.resultadoPlanoPme.total,
    		                                    "color": "#ff99cc"
    		                                }, 
    		                                {
    		                                    "label": "Empresarial",
    		                                    "value": result.resultadoPlanoEmpresarial.total,
    		                                    "color": "#6baa01"
    		                                }
    		                            ]
    		                        }
    		                    }).render();
    		                });
    	                }

    	                
    	                if (vm.account.cargo.name == 'DIRETOR' || vm.account.cargo.name == 'GERENTE') {
    		                FusionCharts.ready(function () {
    		                    var topProductsChart = new FusionCharts({
    		                        type: 'multilevelpie',
    		                        renderAt: 'chart-container',
    		                        width: '1000',
    		                        height: '580',
    		                        dataFormat: 'json',
    		                        dataSource: {
    		                            "chart": {
    		                                "baseFontColor" : "#333333",
    		                                "baseFont" : "Helvetica Neue,Arial",   
    		                                "basefontsize": "9",
    		                                "subcaptionFontBold": "0",
    		                                "bgColor" : "#ffffff",
    		                                "canvasBgColor" : "#ffffff",
    		                                "showBorder" : "0",
    		                                "showShadow" : "0",
    		                                "showCanvasBorder": "0",
    		                                "pieFillAlpha": "60",
    		                                "pieBorderThickness": "2",
    		                                "hoverFillColor": "#cccccc",
    		                                "pieBorderColor": "#ffffff",
    		                                "useHoverColor": "1",
    		                                "showValuesInTooltip": "1",
    		                                "showPercentInTooltip": "0",
    		                                "numberPrefix": "",
    		                                "plotTooltext": "$label, $value, $percentValue"
    		                            },
    		                            "category": [
    		                                {
    		                                    "label": "VENDAS SERRA",
    		                                    "color": "#ffffff",
    		                                    "value": result.vendasTimePorPeriodo[0].valorTimeMensal+result.vendasTimePorPeriodo[1].valorTimeMensal+result.vendasTimePorPeriodo[2].valorTimeMensal+result.vendasTimePorPeriodo[3].valorTimeMensal+result.vendasTimePorPeriodo[4].valorTimeMensal,
    		                                    "category": [
    		                                        {
    		                                            "label": result.vendasTimePorPeriodo[0].nomeTime,
    		                                            "color": result.vendasTimePorPeriodo[0].statusMetaTime,
    		                                            "value": result.vendasTimePorPeriodo[0].valorTimeMensal,
    		                                            "category": multiLevelGraphIntegrantesTime1
    		                                        },
    		                                        {
    		                                            "label": result.vendasTimePorPeriodo[1].nomeTime,
    		                                            "color": result.vendasTimePorPeriodo[1].statusMetaTime,
    		                                            "value": result.vendasTimePorPeriodo[1].valorTimeMensal,
    		                                            "category": multiLevelGraphIntegrantesTime2
    		                                        },
    		                                        {
    		                                            "label": result.vendasTimePorPeriodo[2].nomeTime,
    		                                            "color": result.vendasTimePorPeriodo[2].statusMetaTime,
    		                                            "value": result.vendasTimePorPeriodo[2].valorTimeMensal,
    		                                            "category": multiLevelGraphIntegrantesTime3
    		                                        },
    		                                        {
    		                                            "label": result.vendasTimePorPeriodo[3].nomeTime,
    		                                            "color": result.vendasTimePorPeriodo[3].statusMetaTime,
    		                                            "value": result.vendasTimePorPeriodo[3].valorTimeMensal,
    		                                            "category": multiLevelGraphIntegrantesTime4
    		                                        },
    		                                        {
    		                                            "label": result.vendasTimePorPeriodo[4].nomeTime,
    		                                            "color": result.vendasTimePorPeriodo[4].statusMetaTime,
    		                                            "value": result.vendasTimePorPeriodo[4].valorTimeMensal,
    		                                            "category": multiLevelGraphIntegrantesTime5
    		                                        }
    		                                    ]
    		                                }
    		                            ]
    		                        }
    		                    });
    		                    
    		                    topProductsChart.render();
    		                });
    	                }
    	                
    	                if (vm.account.cargo.name == 'SUPERVISOR') {
    		                FusionCharts.ready(function () {
    		                    var topProductsChart = new FusionCharts({
    		                        type: 'multilevelpie',
    		                        renderAt: 'chart-container-supervisao',
    		                        width: '1000',
    		                        height: '580',
    		                        dataFormat: 'json',
    		                        dataSource: {
    		                            "chart": {
    		                                "baseFontColor" : "#333333",
    		                                "baseFont" : "Helvetica Neue,Arial",   
    		                                "basefontsize": "9",
    		                                "subcaptionFontBold": "0",
    		                                "bgColor" : "#ffffff",
    		                                "canvasBgColor" : "#ffffff",
    		                                "showBorder" : "0",
    		                                "showShadow" : "0",
    		                                "showCanvasBorder": "0",
    		                                "pieFillAlpha": "60",
    		                                "pieBorderThickness": "2",
    		                                "hoverFillColor": "#cccccc",
    		                                "pieBorderColor": "#ffffff",
    		                                "useHoverColor": "1",
    		                                "showValuesInTooltip": "1",
    		                                "showPercentInTooltip": "0",
    		                                "numberPrefix": "",
    		                                "plotTooltext": "$label, $$valueK, $percentValue"
    		                            },
    		                            "category": [
    		                                {
    		                                    "label": "VENDAS SERRA",
    		                                    "color": "#ffffff",
    		                                    "value": timeUser.valorTimeMensal,
    		                                    "category": [
    		                                        {
    		                                            "label": timeUser.nomeTime,
    		                                            "color": timeUser.statusMetaTime,
    		                                            "value": timeUser.valorTimeMensal,
    		                                            "category": multiLevelGraphIntegrantesTime
    		                                        }
    		                                            ]
    		                                        }
    		                                    ]
    		                                }
    		                    	});
    		                    
    		                    topProductsChart.render();
    		                });
    	                }	                
    	                
    	                
    	            });
    	            
    	            Notificacoes.query({userId: vm.account.idUser}, function(result) {
            			if (result[0] != null) {
            				if (vm.account.cargo.name == 'GERENTE' || vm.account.cargo.name == 'DIRETOR') {
            					Notification.primary({delay: 7000, message: '<div align="center"><br>Pendências foram atualizadas<br><a href="#/leads-flow"><b>AQUI</b></a></div><br>', title: 'ATENÇÃO!', positionY:'bottom', positionX:'right'});
            				} else {
            					if (result[0].statusPendencia == 'PENDENTE') {
            						Notification.error({delay: 7000, message: '<div align="center"><br>' + result[0].mensagemDiretoria + '<br><a href="#/leads-flow"><b>AQUI</b></a></div><br>', title: 'ATENÇÃO!', positionY:'bottom', positionX:'right'});
            					} else {
            						Notification.primary({delay: 7000, message: '<div align="center"><br>' + result[0].mensagemDiretoria + '<br><a href="#/leads-flow"><b>AQUI</b></a></div><br>', title: 'ATENÇÃO!', positionY:'bottom', positionX:'right'});
            					}
            				}
            			} else {
            				Notification.success({delay: 7000, message: '<div align="center"><br>Você está em dia com suas tarefas. <b>Continue assim!</b><br></div><br>', title: 'Parabéns!', positionY:'bottom', positionX:'right'});
            			}
                    });
    	           
    	            
                };
                
                if (logado) {
                	$scope.load();
                }

                $scope.ReloadChart = function () {
                	$scope.load();
                };
                
                
                setInterval(function() {
                	  $("#renovar").click();
                	}, 1000 * 600 * 1);
                
            });
        }
        
        function register () {
            $state.go('register');
        }
    }
})();
