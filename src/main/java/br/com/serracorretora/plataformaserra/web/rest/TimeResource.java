package br.com.serracorretora.plataformaserra.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import br.com.serracorretora.plataformaserra.repository.TimeRepository;
import br.com.serracorretora.plataformaserra.service.dto.TimeDTO;

@RestController
@RequestMapping("/api")
public class TimeResource {

    private final Logger log = LoggerFactory.getLogger(TimeResource.class);

    @Inject
    private TimeRepository timeRepository;

    @RequestMapping(value = "/times",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<TimeDTO>> getAllTimes()
        throws URISyntaxException {
        log.debug("Buscando todos os times");
        List<TimeDTO> times = timeRepository.findAll().stream().map(time -> new TimeDTO(time)).collect(Collectors.toList());
       return new ResponseEntity<>(times, HttpStatus.OK);
    }

   
}
