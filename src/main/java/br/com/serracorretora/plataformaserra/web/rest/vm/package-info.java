/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.serracorretora.plataformaserra.web.rest.vm;
