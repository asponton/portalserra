package br.com.serracorretora.plataformaserra.web.rest.util;

import java.util.List;

import br.com.serracorretora.plataformaserra.service.dto.Feriado;


public class RespostaApiFeriado {
	
	private int status;
	
	private List<Feriado> holidays;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Feriado> getHolidays() {
		return holidays;
	}

	public void setHolidays(List<Feriado> holidays) {
		this.holidays = holidays;
	}

}
