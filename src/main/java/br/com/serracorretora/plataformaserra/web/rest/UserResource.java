package br.com.serracorretora.plataformaserra.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.codahale.metrics.annotation.Timed;

import br.com.serracorretora.plataformaserra.config.Constants;
import br.com.serracorretora.plataformaserra.domain.Authority;
import br.com.serracorretora.plataformaserra.domain.Cargo;
import br.com.serracorretora.plataformaserra.domain.User;
import br.com.serracorretora.plataformaserra.repository.UserRepository;
import br.com.serracorretora.plataformaserra.security.AuthoritiesConstants;
import br.com.serracorretora.plataformaserra.service.MailService;
import br.com.serracorretora.plataformaserra.service.UserService;
import br.com.serracorretora.plataformaserra.service.dto.NotificacoesDTO;
import br.com.serracorretora.plataformaserra.service.dto.UserDTO;
import br.com.serracorretora.plataformaserra.service.dto.UserPlantaoDTO;
import br.com.serracorretora.plataformaserra.web.rest.util.HeaderUtil;
import br.com.serracorretora.plataformaserra.web.rest.util.PaginationUtil;
import br.com.serracorretora.plataformaserra.web.rest.vm.ManagedUserVM;

/**
 * REST controller for managing users.
 *
 * <p>This class accesses the User entity, and needs to fetch its collection of authorities.</p>
 * <p>
 * For a normal use-case, it would be better to have an eager relationship between User and Authority,
 * and send everything to the client side: there would be no View Model and DTO, a lot less code, and an outer-join
 * which would be good for performance.
 * </p>
 * <p>
 * We use a View Model and a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities, because people will
 * quite often do relationships with the user, and we don't want them to get the authorities all
 * the time for nothing (for performance reasons). This is the #1 goal: we should not impact our users'
 * application because of this use-case.</li>
 * <li> Not having an outer join causes n+1 requests to the database. This is not a real issue as
 * we have by default a second-level cache. This means on the first HTTP call we do the n+1 requests,
 * but then all authorities come from the cache, so in fact it's much better than doing an outer join
 * (which will get lots of data from the database, for each HTTP call).</li>
 * <li> As this manages users, for security reasons, we'd rather have a DTO layer.</li>
 * </ul>
 * <p>Another option would be to have a specific JPA entity graph to handle this case.</p>
 */
@RestController
@RequestMapping("/api")
public class UserResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    @Inject
    private UserRepository userRepository;

    @Inject
    private MailService mailService;

    @Inject
    private UserService userService;

    /**
     * POST  /users  : Creates a new user.
     * <p>
     * Creates a new user if the login and email are not already used, and sends an
     * mail with an activation link.
     * The user needs to be activated on creation.
     * </p>
     *
     * @param managedUserVM the user to create
     * @param request the HTTP request
     * @return the ResponseEntity with status 201 (Created) and with body the new user, or with status 400 (Bad Request) if the login or email is already in use
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/users",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<?> createUser(@RequestBody ManagedUserVM managedUserVM, HttpServletRequest request) throws URISyntaxException {
        log.debug("REST request to save User : {}", managedUserVM);

        //Lowercase the user login before comparing with database
        if (userRepository.findOneByLogin(managedUserVM.getLogin().toLowerCase()).isPresent()) {
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert("userManagement", "userexists", "Login already in use"))
                .body(null);
        } else if (userRepository.findOneByEmail(managedUserVM.getEmail()).isPresent()) {
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert("userManagement", "emailexists", "Email already in use"))
                .body(null);
        } else if (userRepository.findOneByCpf(managedUserVM.getCpf()).isPresent()) {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert("userManagement", "cpfexists", "Este CPF já esta em uso"))
                    .body(null);
        } else {
            User newUser = userService.createUser(managedUserVM);
            String baseUrl = request.getScheme() + // "http"
            "://" +                                // "://"
            request.getServerName() +              // "myhost"
            ":" +                                  // ":"
            request.getServerPort() +              // "80"
            request.getContextPath();              // "/myContextPath" or "" if deployed in root context
            mailService.sendCreationEmail(newUser, baseUrl);
            return ResponseEntity.created(new URI("/api/users/" + newUser.getLogin()))
                .headers(HeaderUtil.createAlert( "A user is created with identifier " + newUser.getLogin(), newUser.getLogin()))
                .body(newUser);
        }
    }

    /**
     * PUT  /users : Updates an existing User.
     *
     * @param managedUserVM the user to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated user,
     * or with status 400 (Bad Request) if the login or email is already in use,
     * or with status 500 (Internal Server Error) if the user couldn't be updated
     */
    @RequestMapping(value = "/users",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public ResponseEntity<ManagedUserVM> updateUser(@RequestBody ManagedUserVM managedUserVM) {
        log.debug("REST request to update User : {}", managedUserVM);
        Optional<User> existingUser = userRepository.findOneByEmail(managedUserVM.getEmail());
        if (existingUser.isPresent() && existingUser.get().getId().longValue() != managedUserVM.getIdUser().longValue()) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("userManagement", "emailexists", "E-mail already in use")).body(null);
        }
        existingUser = userRepository.findOneByLogin(managedUserVM.getLogin().toLowerCase());
        if (existingUser.isPresent() && existingUser.get().getId().longValue() != managedUserVM.getIdUser().longValue()) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("userManagement", "userexists", "Login already in use")).body(null);
        }
        existingUser = userRepository.findOneByCpf(managedUserVM.getCpf());
        if (existingUser.isPresent() && existingUser.get().getId().longValue() != managedUserVM.getIdUser().longValue()) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("userManagement", "userexists", "Este CPF já esta em uso")).body(null);
        }
        userService.updateUser(managedUserVM.getIdUser(), managedUserVM.getLogin(), managedUserVM.getFirstName(),
            managedUserVM.getLastName(), managedUserVM.getEmail(), managedUserVM.isActivated(), managedUserVM.getLangKey(), managedUserVM.getAuthorities(), 
            managedUserVM.getCargo(), managedUserVM.getTime(), managedUserVM.getCpf(), managedUserVM.getMeta(), managedUserVM.getLimiteLeads(), managedUserVM.getContatos(),
            managedUserVM.getQuantidadeLeadsIndividual(), managedUserVM.getQuantidadeLeadsFamiliar(), managedUserVM.getQuantidadeLeadsPme(), managedUserVM.getQuantidadeLeadsEmpresarial(),
            managedUserVM.getRangeDddMinimo(), managedUserVM.getRangeDddMaximo(), managedUserVM.getResponsavel(), managedUserVM.getUrlFoto(), managedUserVM.getUrlFotoPainel());

        return ResponseEntity.ok()
            .headers(HeaderUtil.createAlert("Usuário foi atualizado " + managedUserVM.getLogin(), managedUserVM.getLogin()))
            .body(new ManagedUserVM(userService.getUserWithAuthorities(managedUserVM.getIdUser())));
    }

    /**
     * GET  /users : get all users.
     * 
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and with body all users
     * @throws URISyntaxException if the pagination headers couldn't be generated
     */
    @RequestMapping(value = "/users",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<ManagedUserVM>> getAllUsers(Pageable pageable)
        throws URISyntaxException {
        Page<User> page = userRepository.findAllWithAuthorities(pageable);
        List<ManagedUserVM> managedUserVMs = page.getContent().stream()
            .map(ManagedUserVM::new)
            .collect(Collectors.toList());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/users");
        return new ResponseEntity<>(managedUserVMs, headers, HttpStatus.OK);
    }
    
    /**
     * GET  /users : get all team users.
     * 
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and with body all users
     * @throws URISyntaxException if the pagination headers couldn't be generated
     */
    @RequestMapping(value = "/users/time/{timeId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ManagedUserVM>> getAllUsersByTime(@RequestParam long timeId, Pageable pageable)
        throws URISyntaxException {
        Page<User> page = userRepository.findAllWithAuthoritiesByTime(timeId, pageable);
        List<ManagedUserVM> managedUserVMs = page.getContent().stream()
            .map(ManagedUserVM::new)
            .collect(Collectors.toList());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/users");
        return new ResponseEntity<>(managedUserVMs, headers, HttpStatus.OK);
    }    
    
    @RequestMapping(value = "/users/plantonistas/{userId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        @Transactional(readOnly = true)
        public ResponseEntity<UserPlantaoDTO> getAllUsersPlantao(@RequestParam long userId)
            throws URISyntaxException {
    		List<User> usuarios = null;
            User usuarioLogado = userRepository.findOne(userId);
            Set<String> perfis = new HashSet<String>();
            for (Authority perfil : usuarioLogado.getAuthorities()) {
				perfis.add(perfil.getName());
			}
            if (perfis.contains(AuthoritiesConstants.GERENCIA_INTERNA)) {
            	usuarios = userRepository.findAll().stream().filter(user -> !"GERENTE".equals(user.getCargo().getName()) && !"DIRETOR".equals(user.getCargo().getName())).collect(Collectors.toList());
            } else if (perfis.contains(AuthoritiesConstants.GERENCIA_EXTERNA)) {
            	usuarios = userRepository.findAll().stream().filter(user -> "SUPERVISOR".equals(user.getCargo().getName())).collect(Collectors.toList());
            } else {
            	usuarios = new ArrayList<User>();
            	usuarios.add(usuarioLogado);
            }
            return new ResponseEntity<>(new UserPlantaoDTO(usuarios), HttpStatus.OK);        
        }
    
    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public ModelAndView home() {
            return new ModelAndView("redirect:" + "#/");
    }
    
    @RequestMapping(value = "/users/bloquear",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        //@Secured(AuthoritiesConstants.ADMIN)
        public ResponseEntity<Void> bloquearVendedor(@RequestBody NotificacoesDTO pendencia) {
            log.debug("REST request to block responsavel: {}", pendencia.getResponsavel().getFirstName());
            try{
                userService.inativarUser(pendencia.getResponsavel());
                return ResponseEntity.ok().headers(HeaderUtil.createAlert(String.format( "O usuáriol %s foi inativado.", pendencia.getResponsavel().getFirstName()))).build();
            } catch (Exception e){
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }   
        }
    
    @RequestMapping(value = "/users/bloquear/indeterminado",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        //@Secured(AuthoritiesConstants.ADMIN)
        public ResponseEntity<Void> bloquearVendedorPorTempoIndeterminado(@RequestBody NotificacoesDTO pendencia) {
            log.debug("REST request to block responsavel: {}", pendencia.getResponsavel().getFirstName());
            try{
                userService.inativarUserPorTempoIndeterminado(pendencia.getResponsavel());
                return ResponseEntity.ok().headers(HeaderUtil.createAlert(String.format( "O usuáriol %s foi inativado.", pendencia.getResponsavel().getFirstName()))).build();
            } catch (Exception e){
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }   
        }        
    

    /**
     * GET  /users/:login : get the "login" user.
     *
     * @param login the login of the user to find
     * @return the ResponseEntity with status 200 (OK) and with body the "login" user, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/users/{login:" + Constants.LOGIN_REGEX + "}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ManagedUserVM> getUser(@PathVariable String login) {
        log.debug("REST request to get User : {}", login);
        ResponseEntity<ManagedUserVM> user = userService.getUserWithAuthoritiesByLogin(login)
        .map(ManagedUserVM::new)
        .map(managedUserVM -> new ResponseEntity<>(managedUserVM, HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        return user;
    }

    /**
     * DELETE /users/:login : delete the "login" User.
     *
     * @param login the login of the user to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/users/{login:" + Constants.LOGIN_REGEX + "}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteUser(@PathVariable String login) {
        log.debug("REST request to delete User: {}", login);
        userService.deleteUser(login);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert( "A user is deleted with identifier " + login, login)).build();
    }
    
    @RequestMapping(value = "/users/cargo/{cargo}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<UserDTO>> getUsuariosPorCargo(@PathVariable String cargo) {
        log.debug("REST request to get Users do cargo : {}", cargo);
        User userLogado = userService.getUserWithAuthorities();
        List<User> usuarios = null;
        if (userLogado.getCargo().getName().equals("SUPERVISOR")) {
        	usuarios = userLogado.getTime().getIntegrantes();
        } else {
        	usuarios = userRepository.findAllByCargo(new Cargo(cargo));
        }
        List<UserDTO> usuariosPorCargo = usuarios.stream()
        		.map(UserDTO::new)
                .collect(Collectors.toList());
        
        return new ResponseEntity<>(usuariosPorCargo, HttpStatus.OK);
    }    
    
}
