package br.com.serracorretora.plataformaserra.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import br.com.serracorretora.plataformaserra.domain.Lead;
import br.com.serracorretora.plataformaserra.domain.Lead.Status;
import br.com.serracorretora.plataformaserra.domain.Notificacao;
import br.com.serracorretora.plataformaserra.domain.Notificacao.StatusPendencia;
import br.com.serracorretora.plataformaserra.domain.Repescagem;
import br.com.serracorretora.plataformaserra.repository.LeadRepository;
import br.com.serracorretora.plataformaserra.repository.TimeRepository;
import br.com.serracorretora.plataformaserra.repository.UserRepository;
import br.com.serracorretora.plataformaserra.service.LeadService;
import br.com.serracorretora.plataformaserra.service.NotificacaoService;
import br.com.serracorretora.plataformaserra.service.RepescagemService;
import br.com.serracorretora.plataformaserra.service.dto.LeadDTO;
import br.com.serracorretora.plataformaserra.service.dto.NotificacoesDTO;
import br.com.serracorretora.plataformaserra.service.dto.PainelGeralLeadsDTO;
import br.com.serracorretora.plataformaserra.service.dto.PesquisaDTO;
import br.com.serracorretora.plataformaserra.service.dto.RepescagemDTO;
import br.com.serracorretora.plataformaserra.service.dto.TimeDTO;
import br.com.serracorretora.plataformaserra.service.dto.UserDTO;
import br.com.serracorretora.plataformaserra.web.rest.util.HeaderUtil;

/**
 * REST controller for managing users.
 *
 * <p>This class accesses the User entity, and needs to fetch its collection of authorities.</p>
 * <p>
 * For a normal use-case, it would be better to have an eager relationship between User and Authority,
 * and send everything to the client side: there would be no DTO, a lot less code, and an outer-join
 * which would be good for performance.
 * </p>
 * <p>
 * We use a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities, because people will
 * quite often do relationships with the user, and we don't want them to get the authorities all
 * the time for nothing (for performance reasons). This is the #1 goal: we should not impact our users'
 * application because of this use-case.</li>
 * <li> Not having an outer join causes n+1 requests to the database. This is not a real issue as
 * we have by default a second-level cache. This means on the first HTTP call we do the n+1 requests,
 * but then all authorities come from the cache, so in fact it's much better than doing an outer join
 * (which will get lots of data from the database, for each HTTP call).</li>
 * <li> As this manages users, for security reasons, we'd rather have a DTO layer.</li>
 * </p>
 * <p>Another option would be to have a specific JPA entity graph to handle this case.</p>
 */
@RestController
@RequestMapping("/api/lead")
public class LeadResource {
	
	private final Logger log = LoggerFactory.getLogger(LeadResource.class);

    @Inject
    private LeadRepository leadRepository;
    
    @Inject
    private TimeRepository timeRepository;    
    
    @Inject
    private LeadService leadService;
    
    @Inject
    private NotificacaoService notificacaoService;
    
    @Inject
    private RepescagemService repescagemService;    
    
    @Inject
    private UserRepository userRepository;    
    
    /**
     * GET  /pool -> get all leads disponíveis.
     */
	@RequestMapping(value = "/pool",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<LeadDTO>> getAllLeads() throws URISyntaxException {
		log.debug("REST request to search all available Lead");
        List<LeadDTO> managedLeadDTOs = leadRepository.findAllByStatus(Status.DISPONIVEL).stream()
            .map(lead -> new LeadDTO(lead))
            .collect(Collectors.toList());
        return new ResponseEntity<>(managedLeadDTOs, HttpStatus.OK);
    }
	
    /**
     * GET  /pool -> get all leads disponíveis.
     */
	@RequestMapping(value = "/pool/gerenciar",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public void getAllLeadsEmAtrasoENotificar() throws URISyntaxException {
		log.debug("REST request to search all available Lead");
        List<LeadDTO> managedLeadDTOs = leadRepository.findAll().stream()
            .filter(lead -> Status.DISPONIVEL.equals(lead.getStatus()) || Status.EM_ANDAMENTO.equals(lead.getStatus())).map(lead -> new LeadDTO(lead))
            .collect(Collectors.toList());
        for (LeadDTO leadDTO : managedLeadDTOs) {
        	if (leadDTO.isLimiteAtingido() && "VENDEDOR".equals(leadDTO.getResponsavel().getCargo().getName())) notificacaoService.inserirNotificacao(leadDTO, StatusPendencia.PENDENTE);
        	if (leadDTO.isLimiteAtingido() && "GERENTE".equals(leadDTO.getResponsavel().getCargo().getName())) this.updateLead(leadDTO, null);
		}
    }	
	
    /**
     * GET  /pool -> get a lead
     */
	@RequestMapping(value = "/pool/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<LeadDTO> getLead(@RequestParam long id) throws URISyntaxException {
		log.debug("REST request to search all available Lead");
         LeadDTO leadDTO = new LeadDTO(leadRepository.findOneById(id).get());
        return new ResponseEntity<>(leadDTO, HttpStatus.OK);
    }	
	
    /**
     * POST  /pool/user -> get all leads disponíveis por usuário.
     */
	@RequestMapping(value = "/pool/user",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<LeadDTO>> getAllLeadsDoUsuario(@RequestBody UserDTO usuario) throws URISyntaxException {
		log.debug("REST request to search all available Lead from one specified User");
		List<LeadDTO> managedLeadDTOs = new ArrayList<LeadDTO>();
			managedLeadDTOs = leadService.buscarLeadsPorResponsavel(usuario).stream()
		            .map(lead -> new LeadDTO(lead))
		            .collect(Collectors.toList());	
			
			if ("GERENTE".equals(usuario.getCargo().getName()) || "DIRETOR".equals(usuario.getCargo().getName())) {
		         for (LeadDTO leadSemResponsavel : leadRepository.findAllByStatus(Status.DISPONIVEL).stream().map(lead -> new LeadDTO(lead)).filter(lead -> lead.getResponsavel() == null).collect(Collectors.toList())) {
		        	 managedLeadDTOs.add(leadSemResponsavel);
		         }
			}
			managedLeadDTOs.sort(new Comparator<LeadDTO>() {

				@Override
				public int compare(LeadDTO o1, LeadDTO o2) {
					return o1.getDataEntrada().compareTo(o2.getDataEntrada());
				}
			});
        return new ResponseEntity<>(managedLeadDTOs, HttpStatus.OK);
    }	
	

	/**
    * POST  /pool/user -> get all leads disponíveis por usuário por status.
    */
	@RequestMapping(value = "/pool/user/status",
       method = RequestMethod.GET,
       produces = MediaType.APPLICATION_JSON_VALUE)
   @Timed
   @Transactional(readOnly = true)
   public ResponseEntity<List<LeadDTO>> getAllLeadsDoUsuarioPorStatus(@RequestParam Long userId, @RequestParam Status status) throws URISyntaxException {
		log.debug("REST request to search all available Lead by Status from one specified User");
		UserDTO userDTO = new UserDTO(userRepository.findOne(userId));
		List<LeadDTO> managedLeadDTOs = new ArrayList<LeadDTO>();
			managedLeadDTOs = leadService.buscarLeadsPorResponsavelEStatus(userDTO, status).stream()
		            .map(lead -> new LeadDTO(lead))
		            .collect(Collectors.toList());	
			
			if ("GERENTE".equals(userDTO.getCargo().getName()) || "DIRETOR".equals(userDTO.getCargo().getName())) {
		         for (LeadDTO leadSemResponsavel : leadRepository.findAllByStatus(Status.DISPONIVEL).stream().map(lead -> new LeadDTO(lead)).filter(lead -> lead.getResponsavel() == null).collect(Collectors.toList())) {
		        	 managedLeadDTOs.add(leadSemResponsavel);
		         }
			}
			managedLeadDTOs.sort(new Comparator<LeadDTO>() {

				@Override
				public int compare(LeadDTO o1, LeadDTO o2) {
					return o1.getDataEntrada().compareTo(o2.getDataEntrada());
				}
			});
       return new ResponseEntity<>(managedLeadDTOs, HttpStatus.OK);
   }	
	
	
    /**
     * GET  /aproveitamento -> get stats dos leads.
     */
	@RequestMapping(value = "/aproveitamento",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<PainelGeralLeadsDTO> getPainelLeads() throws URISyntaxException {
		log.debug("REST request to search all available Lead");
		int ano = ZonedDateTime.now().getYear();
		ZonedDateTime dataInicio = ZonedDateTime.of(ano, 1, 1, 0, 0, 0, 0, ZoneId.of("America/Sao_Paulo"));
		ZonedDateTime dataFim = ZonedDateTime.of(ano, ZonedDateTime.now().getMonthValue(), ZonedDateTime.now().getDayOfMonth(), 23, 59, 59, 0, ZoneId.of("America/Sao_Paulo"));
		List<TimeDTO> times = timeRepository.findAll().stream().map(time -> new TimeDTO(time)).collect(Collectors.toList());
		
        List<LeadDTO> managedLeadsVendidosDTOs = leadRepository.findByStatusAndDataEntradaBetween(Status.VENDIDO, dataInicio, dataFim).stream()
            .map(lead -> new LeadDTO(lead))
            .collect(Collectors.toList());
        List<LeadDTO> managedLeadsEmNegociacaoDTOs = leadRepository.findByStatusAndDataEntradaBetween(Status.EM_ANDAMENTO, dataInicio, dataFim).stream()
                .map(lead -> new LeadDTO(lead))
                .collect(Collectors.toList());        
        List<LeadDTO> managedLeadsPerdidosDTOs = leadRepository.findByStatusAndDataEntradaBetween(Status.SEM_INTERESSE, dataInicio, dataFim).stream()
                .map(lead -> new LeadDTO(lead))
                .collect(Collectors.toList());        
        return new ResponseEntity<>(new PainelGeralLeadsDTO(managedLeadsVendidosDTOs, managedLeadsEmNegociacaoDTOs, managedLeadsPerdidosDTOs, times), HttpStatus.OK);
    }	
	
	/**
     * POST  /enviar -> Creates a new lead.
     * <p>
     * Creates a new lead.
     * </p>
     */
    @RequestMapping(value = "/enviar",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    //@Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<?> createLead(@RequestBody LeadDTO leadDTO) throws URISyntaxException {
        log.debug("REST request to save Lead : {}", leadDTO);
        Optional<Lead> leadEmail = leadRepository.findOneByEmail(leadDTO.getEmail());
        Optional<Lead> leadTelefone = leadRepository.findOneByTelefone(leadDTO.getTelefone());
        if (leadEmail.isPresent()) {
                return ResponseEntity.badRequest()
                        .headers(HeaderUtil.createFailureAlert("lead-dto", "leadexists", "Este lead já existe."))
                        .body(null);
        } else if (leadTelefone.isPresent()) {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert("lead-dto", "leadexists", "Este lead já existe."))
                    .body(null); 
        } else {
            Lead newLead = leadService.createLead(leadDTO);
            //TODO -alimentar sistema de notificao de um user admin dizendo que chegou mais um lead. Manda um e-mail será opcional mailService.sendCreationEmail(newLead, baseUrl);
            return ResponseEntity.created(new URI("/api/lead/" + newLead.getId()))
                .headers(HeaderUtil.createAlert( String.format("Lead %s cadastrado com sucesso", newLead.getNome())))
                .body(newLead.getId());
        }
    }
    
    @RequestMapping(value = "/status/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        @Transactional
        //@Secured(AuthoritiesConstants.ADMIN)
        public ResponseEntity<?> statusLead(@RequestParam Long leadId) throws URISyntaxException {
            log.debug("REST request to get Notificacoes para o usuário : {}", leadId);
            Lead lead = leadRepository.findOne(leadId);
             return ResponseEntity.ok()
                     .headers(HeaderUtil.createAlert(String.format("%s Lead encontrado", lead.getId())))
                     .body(lead.getStatus().toString());
        }           
    
    /**
     * PUT  /users -> Updates an existing User.
     */
    @RequestMapping(value = "/atualizar",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    //@Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<LeadDTO> updateLead(@RequestBody LeadDTO leadDTO, HttpServletRequest request) throws URISyntaxException {
        log.debug("REST request to update Lead : {}", leadDTO);
        
        String baseUrl = request.getScheme() + // "http"
                "://" +                                // "://"
                request.getServerName() +              // "myhost"
                ":" +                                  // ":"
                request.getServerPort() +              // "80"
                request.getContextPath();              // "/myContextPath" or "" if deployed in root context
        
        if (leadDTO.getResponsavel() == null) {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert("lead-dto", "leadsemresponsavel", "Atribua um responsável para esta indicação.."))
                    .body(null);
        }
        
        Lead updatedLead = leadService.updateLead(leadDTO, baseUrl);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createAlert(String.format("Lead %s atualizado com sucesso", leadDTO.getNome())))
                .body(new LeadDTO(updatedLead));

    }    
    
    @RequestMapping(value = "/notificacoes/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    //@Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<List<NotificacoesDTO>> notificacao(@RequestParam Long userId) throws URISyntaxException {
        log.debug("REST request to get Notificacoes para o usuário : {}", userId);
    	 this.getAllLeadsEmAtrasoENotificar();
         List<NotificacoesDTO> notificacoes = notificacaoService.trazerNotificacao(userId) != null ? notificacaoService.trazerNotificacao(userId).stream().map(notificacao -> new NotificacoesDTO(notificacao)).collect(Collectors.toList()) : null;
        
         return ResponseEntity.ok()
                 .headers(HeaderUtil.createAlert(String.format("%s Pendências carregadas", notificacoes != null ? notificacoes.size() : null)))
                 .body(notificacoes != null ? notificacoes : new ArrayList<NotificacoesDTO>());
    }       
    
    @RequestMapping(value = "/notificacoes/pendencia",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        //@Secured(AuthoritiesConstants.ADMIN)
        public ResponseEntity<NotificacoesDTO> InserirPendencia(@RequestBody LeadDTO lead) throws URISyntaxException {
            log.debug("REST request to save Pendencia para : {}", lead.getResponsavel().getIdUser());
                Notificacao notificacao = notificacaoService.inserirNotificacao(lead, StatusPendencia.PENDENTE);
                //TODO -alimentar sistema de notificao de um user admin dizendo que chegou mais um lead. Manda um e-mail será opcional mailService.sendCreationEmail(newLead, baseUrl);
                return ResponseEntity.created(new URI("/api/lead/notificacoes/pendencia" + notificacao.getId()))
                    .headers(HeaderUtil.createAlert( String.format("Pendencia %s cadastrado com sucesso", notificacao.getId())))
                    .body(new NotificacoesDTO(notificacao));
        }   
    
    @RequestMapping(value = "/notificacoes/atualizar/pendencia",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        //@Secured(AuthoritiesConstants.ADMIN)
        public ResponseEntity<NotificacoesDTO> atualizarPendencia(@RequestBody NotificacoesDTO pendencia) throws URISyntaxException {
            log.debug("REST request to atualizar Pendencia para : {}", pendencia.getResponsavel().getFirstName());
                Notificacao notificacao = notificacaoService.atualizarPendencia(pendencia);
                //TODO -alimentar sistema de notificao de um user admin dizendo que chegou mais um lead. Manda um e-mail será opcional mailService.sendCreationEmail(newLead, baseUrl);
                return ResponseEntity.created(new URI("/api/lead/notificacoes/" + pendencia.getId()))
                    .headers(HeaderUtil.createAlert( String.format("Pendencia do Lead %s finalizada.", pendencia.getNumeroLead())))
                    .body(new NotificacoesDTO(notificacao));
        }    
    
    
    @RequestMapping(value = "/repescagem/lista",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        @Transactional
        //@Secured(AuthoritiesConstants.ADMIN)
        public ResponseEntity<List<LeadDTO>> listar(@RequestParam String inicio, @RequestParam String fim, @RequestParam Status status) throws URISyntaxException, ParseException {
            log.debug("REST request to get Repescagem list para o usuário");
            
            String periodoInicial = inicio.substring(0, inicio.indexOf("T"));
            String periodoFinal = fim.substring(0, fim.indexOf("T"));
            
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            
            ZonedDateTime start = ZonedDateTime.ofInstant(format.parse(periodoInicial).toInstant(), ZoneId.systemDefault());
            ZonedDateTime end = ZonedDateTime.ofInstant(format.parse(periodoFinal).toInstant(), ZoneId.systemDefault());
            
            List<LeadDTO> trazerLeadsParaRepescagem = repescagemService.trazerLeadsParaRepescagem(new RepescagemDTO(start, end, status)).stream().filter(lead -> !Status.DISPONIVEL.equals(lead.getStatus())).collect(Collectors.toList());
            
             return ResponseEntity.ok()
                     .headers(HeaderUtil.createAlert(String.format("%s Pendências carregadas", trazerLeadsParaRepescagem != null ? trazerLeadsParaRepescagem.size() : null)))
                     .body(trazerLeadsParaRepescagem != null ? trazerLeadsParaRepescagem : new ArrayList<LeadDTO>());
        }
    

    @RequestMapping(value = "/repescagem/criar",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        @Transactional
        //@Secured(AuthoritiesConstants.ADMIN)
        public ResponseEntity<RepescagemDTO> criar(@RequestBody RepescagemDTO repescagemDTO) throws URISyntaxException {
            log.debug("REST request to get Repescagem list para o usuário");
            Repescagem repescagem = repescagemService.criarCampanhaRepescagem(repescagemDTO);
            
            if (repescagem == null) {
                return ResponseEntity.badRequest()
                        .headers(HeaderUtil.createFailureAlert("repescagem-dto", "repescagemexistente", "Já foi efetuada uma campanha com estes leads.."))
                        .body(null);
            }
            
            return ResponseEntity.created(new URI("/api/lead/repescagem/lista"))
                    .headers(HeaderUtil.createAlert( String.format("Campanha de repescagem criada!")))
                    .body(new RepescagemDTO(repescagem));
        }           
    
    @RequestMapping(value = "/repescagem/aprovar",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        @Transactional
        //@Secured(AuthoritiesConstants.ADMIN)
        public ResponseEntity<?> aprovar(@RequestBody List<LeadDTO> leadsRepescadas) throws URISyntaxException {
            log.debug("REST request to get Repescagem list para o usuário");
            for (LeadDTO leadDTO : leadsRepescadas) {
            	leadDTO.setStatus(Status.REPESCAGEM);
            	leadService.updateLead(leadDTO, null);
			}
            return ResponseEntity.created(new URI("/api/lead/repescagem/lista"))
                    .headers(HeaderUtil.createAlert( String.format("Campanha de repescagem criada!"))).build();
        }           
        
    @RequestMapping(value = "/historico/indicacoes",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        @Transactional
        //@Secured(AuthoritiesConstants.ADMIN)
        public ResponseEntity<List<LeadDTO>> listar(@RequestParam String inicio, @RequestParam String fim, @RequestParam Long idUser) throws URISyntaxException, ParseException {
            log.debug("REST request to get Historico de Leads para o usuário");
            
            String periodoInicial = inicio.substring(0, inicio.indexOf("T"));
            String periodoFinal = fim.substring(0, fim.indexOf("T"));
            
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            
            ZonedDateTime start = ZonedDateTime.ofInstant(format.parse(periodoInicial).toInstant(), ZoneId.systemDefault());
            ZonedDateTime end = ZonedDateTime.ofInstant(format.parse(periodoFinal).toInstant(), ZoneId.systemDefault());
            
            List<LeadDTO> indicacoes = leadRepository.findAllByResponsavelOriginalAndDataEncaminhamentoBetweenOrderByDataEncaminhamentoDesc(idUser, start, end).stream().map(lead -> new LeadDTO(lead)).collect(Collectors.toList());
            
             return ResponseEntity.ok()
                     .headers(HeaderUtil.createAlert(String.format("%s Indicações carregadas", indicacoes != null ? indicacoes.size() : null)))
                     .body(indicacoes != null ? indicacoes : new ArrayList<LeadDTO>());
        }    
    
    @RequestMapping(value = "/pesquisa/user",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        @Transactional
        //@Secured(AuthoritiesConstants.ADMIN)
        public ResponseEntity<?> registrarQualidadeAtendimentoDoCorretor(@RequestBody PesquisaDTO pesquisaDTO) throws URISyntaxException {
            log.debug("REST request to post qualidade de atendimento do corretor");
            Lead lead = leadRepository.findOne(pesquisaDTO.getIdLead());
            if (lead != null && (Status.VENDIDO.equals(lead.getStatus()) || Status.SEM_INTERESSE.equals(lead.getStatus()))) {
                lead.setMotivo(pesquisaDTO.getMotivo());
                lead.setTipoAtendimento(pesquisaDTO.getTipoAtendimento());
                leadRepository.save(lead);
            } else {
            	 return ResponseEntity.ok()
                         .headers(HeaderUtil.createAlert(String.format("Este atendimento não foi encontrado ou ainda está em atendimento!"))).build();
            }
            
            return ResponseEntity.ok()
                    .headers(HeaderUtil.createAlert(String.format("Sua mensagem foi registrada! Obrigado!"))).build();
        }             
    

}
