package br.com.serracorretora.plataformaserra.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import br.com.serracorretora.plataformaserra.domain.Evento;
import br.com.serracorretora.plataformaserra.domain.GerenciamentoPlantoes;
import br.com.serracorretora.plataformaserra.domain.User;
import br.com.serracorretora.plataformaserra.repository.EventoRepository;
import br.com.serracorretora.plataformaserra.repository.GerenciamentoPlantaoRepository;
import br.com.serracorretora.plataformaserra.service.EventoService;
import br.com.serracorretora.plataformaserra.service.MailService;
import br.com.serracorretora.plataformaserra.service.UserService;
import br.com.serracorretora.plataformaserra.service.dto.ConfiguracaoPlantaoDTO;
import br.com.serracorretora.plataformaserra.service.dto.EventoDTO;
import br.com.serracorretora.plataformaserra.service.dto.GerenciamentoPlantoesDTO;
import br.com.serracorretora.plataformaserra.web.rest.util.HeaderUtil;

/**
 * REST controller for managing users.
 *
 * <p>This class accesses the User entity, and needs to fetch its collection of authorities.</p>
 * <p>
 * For a normal use-case, it would be better to have an eager relationship between User and Authority,
 * and send everything to the client side: there would be no DTO, a lot less code, and an outer-join
 * which would be good for performance.
 * </p>
 * <p>
 * We use a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities, because people will
 * quite often do relationships with the user, and we don't want them to get the authorities all
 * the time for nothing (for performance reasons). This is the #1 goal: we should not impact our users'
 * application because of this use-case.</li>
 * <li> Not having an outer join causes n+1 requests to the database. This is not a real issue as
 * we have by default a second-level cache. This means on the first HTTP call we do the n+1 requests,
 * but then all authorities come from the cache, so in fact it's much better than doing an outer join
 * (which will get lots of data from the database, for each HTTP call).</li>
 * <li> As this manages users, for security reasons, we'd rather have a DTO layer.</li>
 * </p>
 * <p>Another option would be to have a specific JPA entity graph to handle this case.</p>
 */
@RestController
@RequestMapping("/api/evento")
public class EventoResource {
	
	@Inject
	private EventoRepository eventoRepository;
	
	@Inject
	private GerenciamentoPlantaoRepository gerenciamentoPlantaoRepository;
	
    @Inject
    private MailService mailService;

    @Inject
    private UserService userService;
    
    @Inject
    private EventoService eventoService;    
	
	private final Logger log = LoggerFactory.getLogger(EventoResource.class);
 
    @RequestMapping(value = "/criar/evento",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        @Transactional
        public ResponseEntity<?> criarEvento(@RequestBody EventoDTO eventoDTO, HttpServletRequest request) throws URISyntaxException {
            log.debug("REST request to crite Evento : {}", eventoDTO.getTitle());
            User user = userService.getUserWithAuthorities(eventoDTO.getOwnerId());
            EventoDTO eventoCriado = new EventoDTO(eventoService.criarEvento(eventoDTO));
	            
	            String baseUrl = request.getScheme() + // "http"
	                    "://" +                                // "://"
	                    request.getServerName() +              // "myhost"
	                    ":" +                                  // ":"
	                    request.getServerPort() +              // "80"
	                    request.getContextPath();              // "/myContextPath" or "" if deployed in root context
	                    mailService.sendEventCreationEmail(user, baseUrl, eventoCriado);
	                        return ResponseEntity.created(new URI("#"))
	                            .headers(HeaderUtil.createAlert( String.format("Chegou", eventoCriado.getOwnerId())))
	                            .body(eventoCriado.getTaskId());

        }
    
	@RequestMapping(value = "/carregar/{userId}",
	        method = RequestMethod.GET,
	        produces = MediaType.APPLICATION_JSON_VALUE)
	    @Timed
	    @Transactional(readOnly = true)
	    public ResponseEntity<List<EventoDTO>> getAllEventos(@PathVariable Long userId) throws URISyntaxException {
			log.debug("REST request to search all available Eventos");
			List<EventoDTO> eventos = null;
			User user = userService.getUserWithAuthorities(userId);
			if ("GERENTE".equals(user.getCargo().getName()) || "DIRETOR".equals(user.getCargo().getName())) {
				eventos = eventoRepository.findAll().stream().map(evento -> new EventoDTO(evento)).collect(Collectors.toList());
			} else {
				eventos = eventoRepository.findAllByOwnerId(user.getId()).stream().map(evento -> new EventoDTO(evento)).collect(Collectors.toList());
			}
	        return new ResponseEntity<>(eventos, HttpStatus.OK);
	    }    
	
    @RequestMapping(value = "/atualizar/evento",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        @Transactional
        //@Secured(AuthoritiesConstants.ADMIN)
        public ResponseEntity<?> updateEvento(@RequestBody  EventoDTO eventoDTO, HttpServletRequest request) throws URISyntaxException {
            log.debug("REST request to update Evento : {}", eventoDTO);
            Evento updatedEvento = eventoService.updateEvento(eventoDTO);
            User user = userService.getUserWithAuthorities(eventoDTO.getOwnerId());
            
            String baseUrl = request.getScheme() + // "http"
                    "://" +                                // "://"
                    request.getServerName() +              // "myhost"
                    ":" +                                  // ":"
                    request.getServerPort() +              // "80"
                    request.getContextPath();              // "/myContextPath" or "" if deployed in root context
                    mailService.sendEventCreationEmail(user, baseUrl, new EventoDTO(updatedEvento));
                        
            return ResponseEntity.ok()
                    .headers(HeaderUtil.createAlert(String.format("Evento %s atualizado com sucesso", eventoDTO.getOwnerId())))
                    .body(new EventoDTO(updatedEvento).getTaskId());	
    }
    
    @RequestMapping(value = "/apagar/evento",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        @Transactional
        //@Secured(AuthoritiesConstants.ADMIN)
        public ResponseEntity<?> deleteEvento(@RequestBody  EventoDTO eventoDTO) {
            log.debug("REST request to delete Evento: {}", eventoDTO.getTaskId());
            	eventoService.deleteEvento(eventoDTO);
                return ResponseEntity.ok()
                		.headers(HeaderUtil.createAlert(String.format( "O evento foi removido com sucesso")))
                		.body(eventoDTO.getTaskId());
        }
    
    @RequestMapping(value = "/abrir-mes",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        @Transactional
        //@Secured(AuthoritiesConstants.ADMIN)
        public ResponseEntity<List<ConfiguracaoPlantaoDTO>> abrirMes(@RequestBody GerenciamentoPlantoesDTO aberturaMensal) throws URISyntaxException {
            log.debug("REST request para abrir um Mes : {}");
            List<ConfiguracaoPlantaoDTO> configuracoes;
			configuracoes = eventoService.abrirMes(aberturaMensal);
            return new ResponseEntity<>(configuracoes, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/iniciar-plantao-mensal",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        @Transactional
        //@Secured(AuthoritiesConstants.ADMIN)
        public ResponseEntity<?> iniciarPlantaoMensal(@RequestBody List<ConfiguracaoPlantaoDTO> configuracoes) throws URISyntaxException {
            log.debug("REST request para abrir um Mes : {}");
				 eventoService.iniciarPlantao(configuracoes);
			return ResponseEntity.ok()
            		.headers(HeaderUtil.createAlert(String.format( "Eventos criados."))).build();
    }
    
	@RequestMapping(value = "/plantao-mensal/notificar/corretores",
	        method = RequestMethod.GET,
	        produces = MediaType.APPLICATION_JSON_VALUE)
	    @Timed
	    @Transactional(readOnly = true)
	    public ResponseEntity<?> notificarCorretores(HttpServletRequest request) throws URISyntaxException {
			log.debug("REST request to send e-mails to all corretores chosen to participate in a month´s plantão");
			Optional<GerenciamentoPlantoes> plantaoMensal = gerenciamentoPlantaoRepository.findOneByPeriodoFechadoFalse();
			Set<User> usuariosDoPlantaoMensal = new HashSet<User>();
			if (plantaoMensal.isPresent()) {
				GerenciamentoPlantoes plantao = plantaoMensal.get();
				plantao.setPeriodoFechado(true);
				gerenciamentoPlantaoRepository.saveAndFlush(plantao);
				List<EventoDTO> eventos = eventoRepository.findAllByPlantaoId(plantaoMensal.get().getId()).stream().map(evento -> new EventoDTO(evento)).collect(Collectors.toList());
				User user = null;
				for (EventoDTO eventoDTO : eventos) {
					user = userService.getUserWithAuthorities(eventoDTO.getOwnerId());
					usuariosDoPlantaoMensal.add(user);
				}

				String baseUrl = request.getScheme() + // "http"
	                    "://" +                                // "://"
	                    request.getServerName() +              // "myhost"
	                    ":" +                                  // ":"
	                    request.getServerPort() +              // "80"
	                    request.getContextPath();              // "/myContextPath" or "" if deployed in root context
				
				for (User plantonista : usuariosDoPlantaoMensal) {
					mailService.sendPlantaoCreationEmail(plantonista, baseUrl, eventoRepository.findAllByPlantaoIdAndOwnerIdOrderByStart(plantaoMensal.get().getId(), plantonista.getId()));
				}
				
	        return ResponseEntity.ok()
                    .headers(HeaderUtil.createAlert("Corretores notificados")).build();
			} else {
	            return ResponseEntity.ok()
	                    .headers(HeaderUtil.createAlert("Já foram enviados e-mails para os corretores no período selecionado. Para quê enviar outro?")).build();
			}
	    }     

}
