package br.com.serracorretora.plataformaserra.web.rest.util;

import java.time.ZonedDateTime;
import java.util.List;

import br.com.serracorretora.plataformaserra.domain.Ranking;
import br.com.serracorretora.plataformaserra.service.dto.RankingDTO;

public class RankingUtil {
	
	public static RankingDTO getRankingAtual(List<Ranking> rankingsUser) {
		RankingDTO rankingDTO = null;
		for (Ranking ranking : rankingsUser) {
			if (ranking.getDataCompetencia().getYear() == ZonedDateTime.now().getYear() && ranking.getDataCompetencia().getDayOfYear() == ZonedDateTime.now().getDayOfYear()) {
				rankingDTO = new RankingDTO(ranking);
			}
		}
		
		return rankingDTO;
	}	
	
	public static Ranking getRankingAtualBanco(List<Ranking> rankingsUser) {
		Ranking rankingAtual = null;
		for (Ranking ranking : rankingsUser) {
			if (ranking.getDataCompetencia().getYear() == ZonedDateTime.now().getYear() && ranking.getDataCompetencia().getDayOfYear() == ZonedDateTime.now().getDayOfYear()) {
				rankingAtual = ranking;
			}
		}
		
		return rankingAtual;
	}		
	
	public static RankingDTO getRankingDTOAtual(List<RankingDTO> rankingsUser) {
		RankingDTO rankingDTO = null;
		for (RankingDTO ranking : rankingsUser) {
			if (ranking.getDataCompetencia().getYear() == ZonedDateTime.now().getYear() && ranking.getDataCompetencia().getDayOfYear() == ZonedDateTime.now().getDayOfYear()) {
				rankingDTO = ranking;
			}
		}
		
		return rankingDTO;
	}		

}
