package br.com.serracorretora.plataformaserra.repository;

import java.time.ZonedDateTime;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.serracorretora.plataformaserra.domain.Lead.Status;
import br.com.serracorretora.plataformaserra.domain.Repescagem;

public interface RepescagemRepository extends JpaRepository<Repescagem, Long> {
	
	Optional<Repescagem> findOneById(Long id);
	
	Optional<Repescagem> findByInicioAfterAndFimBeforeAndStatus(ZonedDateTime inicio, ZonedDateTime fim, Status status);

}
