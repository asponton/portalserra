package br.com.serracorretora.plataformaserra.repository;

import br.com.serracorretora.plataformaserra.domain.Cargo;
import br.com.serracorretora.plataformaserra.domain.User;

import java.time.ZonedDateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the User entity.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findOneByActivationKey(String activationKey);

    List<User> findAllByActivatedIsFalseAndCreatedDateBefore(ZonedDateTime dateTime);

    Optional<User> findOneByResetKey(String resetKey);

    Optional<User> findOneByEmail(String email);

    Optional<User> findOneByLogin(String login);

    Optional<User> findOneById(Long userId);
    
    Optional<User> findOneByCpf(String cpf);

    @Query(value = "select distinct user from User user left join fetch user.authorities",
        countQuery = "select count(user) from User user")
    Page<User> findAllWithAuthorities(Pageable pageable);
    
    @Query(value = "select distinct user from User user left join fetch user.authorities where user.time = ?1",
            countQuery = "select count(user) from User user")
        Page<User> findAllWithAuthoritiesByTime(Long idTime, Pageable pageable);    

    @Override
    void delete(User t);

	List<User> findAllByCargo(Cargo cargo);

	List<User> findAllByActivatedIsFalseAndDataBloqueioBefore(ZonedDateTime dateTime);


}
