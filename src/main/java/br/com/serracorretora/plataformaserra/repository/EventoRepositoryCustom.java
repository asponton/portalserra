package br.com.serracorretora.plataformaserra.repository;

public interface EventoRepositoryCustom {

    Long buscarUltimoTaskId();
}
