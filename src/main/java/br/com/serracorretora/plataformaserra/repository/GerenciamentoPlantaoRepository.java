package br.com.serracorretora.plataformaserra.repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.serracorretora.plataformaserra.domain.GerenciamentoPlantoes;


public interface GerenciamentoPlantaoRepository extends JpaRepository<GerenciamentoPlantoes, Long> {
	
	
	Optional<GerenciamentoPlantoes> findByPeriodoInicialBetween(ZonedDateTime inicio, ZonedDateTime fim);
	
	List<GerenciamentoPlantoes> findOneByPeriodoFechadoTrue();
	
	Optional<GerenciamentoPlantoes> findOneByPeriodoFechadoFalse();

}
