package br.com.serracorretora.plataformaserra.repository;

import java.util.List;

import br.com.serracorretora.plataformaserra.domain.User;

public interface UserRepositoryCustom {

    List<User> consultarPorCriteria();

    void atualizarResponsavel(User responsavelRetirado, User responsavelAdicionado);
}
