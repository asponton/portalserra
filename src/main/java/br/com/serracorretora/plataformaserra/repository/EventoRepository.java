package br.com.serracorretora.plataformaserra.repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.serracorretora.plataformaserra.domain.Evento;

/**
 * Spring Data JPA repository for the User entity.
 */
public interface EventoRepository extends JpaRepository<Evento, Long> , EventoRepositoryCustom{

    List<Evento> findAllByOwnerId(Long idUsuario);
    
    List<Evento> findAllByPlantaoId(Long idPlantao);
    
    List<Evento> findAllByPlantaoIdAndOwnerIdOrderByStart(Long idPlantao, Long idOwner);
    
    List<Evento> findAllByStartAndEnd(ZonedDateTime inicio, ZonedDateTime fim);
    
    Optional<Evento> findOneByTaskId(Long idEvento);
    
    List<Evento> findOneByStart(ZonedDateTime dia);
    
    Evento findByTaskId(Long idEvento);
    
    @Override
    void delete(Evento t);

}
