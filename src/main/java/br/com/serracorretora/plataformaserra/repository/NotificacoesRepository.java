package br.com.serracorretora.plataformaserra.repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.serracorretora.plataformaserra.domain.Notificacao;
import br.com.serracorretora.plataformaserra.domain.Notificacao.StatusPendencia;
import br.com.serracorretora.plataformaserra.domain.User;

/**
 * Spring Data JPA repository for the Notificacoes entity.
 */
public interface NotificacoesRepository extends JpaRepository<Notificacao, Long> {

    List<Notificacao> findAllByResponsavelAndStatusPendenciaAndDataEntrada(User user, StatusPendencia statusPendencia, ZonedDateTime data);
    
    List<Notificacao> findAllByStatusPendenciaAndDataEntrada(StatusPendencia statusPendencia, ZonedDateTime data);
    
    Notificacao findByResponsavelAndNumeroLeadAndStatusPendencia(User responsavel, Long idLead, StatusPendencia statusPendencia);
    
    Optional<Notificacao> findById(Long id);
    
    List<Notificacao> findAllByResponsavelAndStatusPendencia(User user, StatusPendencia statusPendencia);
    
}
