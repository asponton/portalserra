package br.com.serracorretora.plataformaserra.repository;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.serracorretora.plataformaserra.domain.Lead;
import br.com.serracorretora.plataformaserra.domain.Lead.Status;
import br.com.serracorretora.plataformaserra.domain.User;

/**
 * Spring Data JPA repository for the Lead entity.
 */
public interface LeadRepository extends JpaRepository<Lead, Long> {

    List<Lead> findAllByStatus(Status status);
    
    List<Lead> findAllByResponsavelAndStatus(User responsavel, Status status);
    
    List<Lead> findAllByResponsavelOrderByDataEncaminhamentoDesc(User responsavel);
    
    List<Lead> findAllByResponsavelOriginalAndDataEncaminhamentoBetweenOrderByDataEncaminhamentoDesc(Long idResponsavelOriginal, ZonedDateTime inicio, ZonedDateTime fim);
    
    List<Lead> findByStatusAndDataEntradaBetween(Status status, ZonedDateTime dataInicio, ZonedDateTime dataFim);

	Optional<Lead> findOneByEmail(String email);

	Optional<Lead> findOneByTelefone(String telefone);
	
	Optional<Lead> findOneById(Long leadId);

	Collection<Lead> findAllByResponsavelAndStatusOrderByDataEncaminhamentoDesc(User user, Status status);

    
}
