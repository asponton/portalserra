package br.com.serracorretora.plataformaserra.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.serracorretora.plataformaserra.domain.Time;

public interface TimeRepository extends JpaRepository<Time, Long> {}
