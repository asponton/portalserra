package br.com.serracorretora.plataformaserra.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.serracorretora.plataformaserra.domain.Ranking;
import br.com.serracorretora.plataformaserra.domain.User;

public interface RankingRepository extends JpaRepository<Ranking, Long> {
	
	List<Ranking> findAllByUsuarioAndDataCompetenciaBetween(User usuario, ZonedDateTime dataInicio, ZonedDateTime dataFim);
	
	Ranking findByUsuarioAndDataCompetenciaBetween(User usuario, ZonedDateTime dataInicio, ZonedDateTime dataFim);
	
}
