package br.com.serracorretora.plataformaserra.repository;

import br.com.serracorretora.plataformaserra.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
