package br.com.serracorretora.plataformaserra.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.serracorretora.plataformaserra.repository.EventoRepositoryCustom;

public class EventoRepositoryImpl implements EventoRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

	@Override
	public Long buscarUltimoTaskId() {
        String hql = "select t.taskId from Evento t order by t.id desc";
        Query query = entityManager.createQuery(hql);
        int resultado = Integer.valueOf(query.getResultList() == null || query.getResultList().isEmpty() ? "0" : query.getResultList().get(0).toString());
        return Long.valueOf(resultado) == 0 ? 1 : Long.valueOf(resultado) + 1; 
	}
    
}
