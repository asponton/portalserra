package br.com.serracorretora.plataformaserra.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.serracorretora.plataformaserra.domain.User;
import br.com.serracorretora.plataformaserra.repository.UserRepository;
import br.com.serracorretora.plataformaserra.repository.UserRepositoryCustom;

public class UserRepositoryImpl implements UserRepositoryCustom {

    @Autowired
    private UserRepository userRepository;
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public List<User> consultarPorCriteria() {
        return userRepository.findAll();
    }

    @Override
    public void atualizarResponsavel(User responsavelRetirado, User responsavelAdicionado) {
        String hql = "UPDATE User u SET u.responsavel = :responsavelAdicionado "
                + "WHERE u.responsavel = :responsavelRetirado";
        Query query = entityManager.createQuery(hql);
        
        query.setParameter("responsavelRetirado", responsavelRetirado);
        query.setParameter("responsavelAdicionado", responsavelAdicionado);
        
        query.executeUpdate();
    }
    
}
