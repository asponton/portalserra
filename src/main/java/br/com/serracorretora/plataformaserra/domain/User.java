package br.com.serracorretora.plataformaserra.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.serracorretora.plataformaserra.config.Constants;

/**
 * A user.
 */
@Entity
@Table(name = "jhi_user")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class User extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    @Column(length = 50, unique = true, nullable = false)
    private String login;

    @JsonIgnore
    @NotNull
    @Size(min = 60, max = 60)
    @Column(name = "password_hash",length = 60)
    private String password;

    @Size(max = 50)
    @Column(name = "first_name", length = 50)
    private String firstName;

    @Size(max = 50)
    @Column(name = "last_name", length = 50)
    private String lastName;
    
    @Size(max = 15)
    @Column(name = "cpf", length = 15)
    private String cpf;

    @Email
    @Size(max = 100)
    @Column(length = 100, unique = true)
    private String email;

    @NotNull
    @ManyToOne
    @JoinColumn(name="cargo_fk")
    private Cargo cargo;    
    
    @ManyToOne
    @JoinColumn(name="time_fk")
    private Time time;
    
    @ManyToOne
    @JoinColumn(name="usuario_responsavel_fk")
    private User responsavel;
    
    @OneToMany(mappedBy="usuario", cascade=CascadeType.ALL)
    private List<Contato> contatos;
    
    @OneToMany(mappedBy="ownerId", cascade=CascadeType.ALL)
    private List<Evento> evento;
    
    @Column(name="quantidade_leads_individual")
	private Long quantidadeLeadsIndividual;
	
	@Column(name="quantidade_leads_familiar")
	private Long quantidadeLeadsFamiliar;
	
	@Column(name="quantidade_leads_pme")
	private Long quantidadeLeadsPme;
	
	@Column(name="quantidade_leads_empresarial")
	private Long quantidadeLeadsEmpresarial;
	
	@Column(name="range_ddd_minimo")
	private Long rangeDddMinimo;
	
	@Column(name="range_ddd_maximo")
	private Long rangeDddMaximo;
	
	@Column(name="data_bloqueio", nullable = true)
	private ZonedDateTime dataBloqueio;
	
    @Column(name="meta", precision=16, scale=2)
    private BigDecimal meta;
    
    @Size(max = 255)
    @Column(name = "foto_url", length = 255)
    private String fotoUrl;
    
    @Size(max = 255)
    @Column(name = "foto_painel", length = 255)
    private String fotoPainel;
    
    @Column(name="limite_leads")
    private Long limiteLeads;	
    
    @NotNull
    @Column(nullable = false)
    private boolean activated = false;

    @Size(min = 2, max = 5)
    @Column(name = "lang_key", length = 5)
    private String langKey;

    @Size(max = 20)
    @Column(name = "activation_key", length = 20)
    @JsonIgnore
    private String activationKey;

    @Size(max = 20)
    @Column(name = "reset_key", length = 20)
    private String resetKey;

    @Column(name = "reset_date", nullable = true)
    private ZonedDateTime resetDate = null;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
        name = "jhi_user_authority",
        joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "authority_name", referencedColumnName = "name")})
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Authority> authorities = new HashSet<>();

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "user")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PersistentToken> persistentTokens = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    //Lowercase the login before saving it in database
    public void setLogin(String login) {
        this.login = login.toLowerCase(Locale.ENGLISH);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public User getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(User responsavel) {
		this.responsavel = responsavel;
	}

	public boolean getActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public String getResetKey() {
        return resetKey;
    }

    public void setResetKey(String resetKey) {
        this.resetKey = resetKey;
    }

    public ZonedDateTime getResetDate() {
       return resetDate;
    }

    public void setResetDate(ZonedDateTime resetDate) {
       this.resetDate = resetDate;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public Set<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public Set<PersistentToken> getPersistentTokens() {
        return persistentTokens;
    }

    public void setPersistentTokens(Set<PersistentToken> persistentTokens) {
        this.persistentTokens = persistentTokens;
    }

    public List<Contato> getContatos() {
		return contatos;
	}

	public void setContatos(List<Contato> contatos) {
		this.contatos = contatos;
	}

	public List<Evento> getEvento() {
		return evento;
	}

	public void setEvento(List<Evento> evento) {
		this.evento = evento;
	}

	public Long getQuantidadeLeadsIndividual() {
		return quantidadeLeadsIndividual;
	}

	public void setQuantidadeLeadsIndividual(Long quantidadeLeadsIndividual) {
		this.quantidadeLeadsIndividual = quantidadeLeadsIndividual;
	}

	public Long getQuantidadeLeadsFamiliar() {
		return quantidadeLeadsFamiliar;
	}

	public void setQuantidadeLeadsFamiliar(Long quantidadeLeadsFamiliar) {
		this.quantidadeLeadsFamiliar = quantidadeLeadsFamiliar;
	}

	public Long getQuantidadeLeadsPme() {
		return quantidadeLeadsPme;
	}

	public void setQuantidadeLeadsPme(Long quantidadeLeadsPme) {
		this.quantidadeLeadsPme = quantidadeLeadsPme;
	}

	public Long getQuantidadeLeadsEmpresarial() {
		return quantidadeLeadsEmpresarial;
	}

	public void setQuantidadeLeadsEmpresarial(Long quantidadeLeadsEmpresarial) {
		this.quantidadeLeadsEmpresarial = quantidadeLeadsEmpresarial;
	}

	public Long getRangeDddMinimo() {
		return rangeDddMinimo;
	}

	public void setRangeDddMinimo(Long rangeDddMinimo) {
		this.rangeDddMinimo = rangeDddMinimo;
	}

	public Long getRangeDddMaximo() {
		return rangeDddMaximo;
	}

	public void setRangeDddMaximo(Long rangeDddMaximo) {
		this.rangeDddMaximo = rangeDddMaximo;
	}

	public ZonedDateTime getDataBloqueio() {
		return dataBloqueio;
	}

	public void setDataBloqueio(ZonedDateTime dataBloqueio) {
		this.dataBloqueio = dataBloqueio;
	}

	public BigDecimal getMeta() {
		return meta;
	}

	public void setMeta(BigDecimal meta) {
		this.meta = meta;
	}

	public String getFotoUrl() {
		return fotoUrl;
	}

	public void setFotoUrl(String fotoUrl) {
		this.fotoUrl = fotoUrl;
	}

	public String getFotoPainel() {
		return fotoPainel;
	}

	public void setFotoPainel(String fotoPainel) {
		this.fotoPainel = fotoPainel;
	}

	public Long getLimiteLeads() {
		return limiteLeads;
	}

	public void setLimiteLeads(Long limiteLeads) {
		this.limiteLeads = limiteLeads;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        User user = (User) o;

        if (!login.equals(user.login)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return login.hashCode();
    }

    @Override
    public String toString() {
        return "User{" +
            "login='" + login + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", email='" + email + '\'' +
            ", activated='" + activated + '\'' +
            ", langKey='" + langKey + '\'' +
            ", activationKey='" + activationKey + '\'' +
            "}";
    }
}
