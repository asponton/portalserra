package br.com.serracorretora.plataformaserra.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "eventos")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Evento extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 137380984094912491L;
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @NotNull
    @Column(name = "task_id")
    private Long taskId;
    
    @NotNull
    @Column(name = "owner_id")
    private Long ownerId;
    
    @Column(name = "plantao_id")
    private Long plantaoId;    
    
    @Size(max = 255)
    @Column(name = "title", length = 255)
    private String title;
	
    @Size(max = 255)
    @Column(name = "description", length = 255)
	private String description;
	
    @Column(name = "start_time_zone")
	private ZonedDateTime startTimeZone;
	
    @Column(name = "start")
	private ZonedDateTime start;
	
    @Column(name = "end")
	private ZonedDateTime end;
	
    @Column(name = "end_time_zone")
	private ZonedDateTime endTimeZone;
	
    @Size(max = 255)
    @Column(name = "recurrence_rule", length = 255)
	private String recurrenceRule;
	
    @Column(name = "recurrence_cancel_id")
	private Long recurrenceCancelId;
	
    @Column(name = "recurrence_id")
	private Long recurrenceId;
	
    @Size(max = 255)
    @Column(name = "recurrence_exception", length = 255)
	private String recurrenceException;
    
    @Column(name = "is_all_day")
	private Boolean isAllDay;
    
    @Column(name="quantidade_indicacoes")
    private Long quantidadeIndicacoes;
    
    @Column(name="quantidade_leads_individual")
    private Long quantidadeLeadsIndividual;
    
    @Column(name="quantidade_leads_familiar")
    private Long quantidadeLeadsFamiliar;
    
    @Column(name="quantidade_leads_pme")
    private Long quantidadeLeadsPme;
    
    @Column(name="quantidade_leads_corporativo")
    private Long quantidadeLeadsCorporativo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}

	public Long getPlantaoId() {
		return plantaoId;
	}

	public void setPlantaoId(Long plantaoId) {
		this.plantaoId = plantaoId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ZonedDateTime getStartTimeZone() {
		return startTimeZone;
	}

	public void setStartTimeZone(ZonedDateTime startTimeZone) {
		this.startTimeZone = startTimeZone;
	}

	public ZonedDateTime getStart() {
		return start;
	}

	public void setStart(ZonedDateTime start) {
		this.start = start;
	}

	public ZonedDateTime getEnd() {
		return end;
	}

	public void setEnd(ZonedDateTime end) {
		this.end = end;
	}

	public ZonedDateTime getEndTimeZone() {
		return endTimeZone;
	}

	public void setEndTimeZone(ZonedDateTime endTimeZone) {
		this.endTimeZone = endTimeZone;
	}

	public String getRecurrenceRule() {
		return recurrenceRule;
	}

	public void setRecurrenceRule(String recurrenceRule) {
		this.recurrenceRule = recurrenceRule;
	}

	public Long getRecurrenceCancelId() {
		return recurrenceCancelId;
	}

	public void setRecurrenceCancelId(Long recurrenceCancelId) {
		this.recurrenceCancelId = recurrenceCancelId;
	}

	public Long getRecurrenceId() {
		return recurrenceId;
	}

	public void setRecurrenceId(Long recurrenceId) {
		this.recurrenceId = recurrenceId;
	}

	public String getRecurrenceException() {
		return recurrenceException;
	}

	public void setRecurrenceException(String recurrenceException) {
		this.recurrenceException = recurrenceException;
	}

	public Boolean getIsAllDay() {
		return isAllDay;
	}

	public void setIsAllDay(Boolean isAllDay) {
		this.isAllDay = isAllDay;
	}

	public Long getQuantidadeIndicacoes() {
		return quantidadeIndicacoes;
	}

	public void setQuantidadeIndicacoes(Long quantidadeIndicacoes) {
		this.quantidadeIndicacoes = quantidadeIndicacoes;
	}

	public Long getQuantidadeLeadsIndividual() {
		return quantidadeLeadsIndividual;
	}

	public void setQuantidadeLeadsIndividual(Long quantidadeLeadsIndividual) {
		this.quantidadeLeadsIndividual = quantidadeLeadsIndividual;
	}

	public Long getQuantidadeLeadsFamiliar() {
		return quantidadeLeadsFamiliar;
	}

	public void setQuantidadeLeadsFamiliar(Long quantidadeLeadsFamiliar) {
		this.quantidadeLeadsFamiliar = quantidadeLeadsFamiliar;
	}

	public Long getQuantidadeLeadsPme() {
		return quantidadeLeadsPme;
	}

	public void setQuantidadeLeadsPme(Long quantidadeLeadsPme) {
		this.quantidadeLeadsPme = quantidadeLeadsPme;
	}

	public Long getQuantidadeLeadsCorporativo() {
		return quantidadeLeadsCorporativo;
	}

	public void setQuantidadeLeadsCorporativo(Long quantidadeLeadsCorporativo) {
		this.quantidadeLeadsCorporativo = quantidadeLeadsCorporativo;
	}

	@Override
	public String toString() {
		return "Evento [id=" + id + ", taskId=" + taskId + ", ownerId=" + ownerId + ", plantaoId=" + plantaoId
				+ ", title=" + title + ", description=" + description + ", startTimeZone=" + startTimeZone + ", start="
				+ start + ", end=" + end + ", endTimeZone=" + endTimeZone + ", recurrenceRule=" + recurrenceRule
				+ ", recurrenceCancelId=" + recurrenceCancelId + ", recurrenceId=" + recurrenceId
				+ ", recurrenceException=" + recurrenceException + ", isAllDay=" + isAllDay + ", quantidadeIndicacoes="
				+ quantidadeIndicacoes + ", quantidadeLeadsIndividual=" + quantidadeLeadsIndividual
				+ ", quantidadeLeadsFamiliar=" + quantidadeLeadsFamiliar + ", quantidadeLeadsPme=" + quantidadeLeadsPme
				+ ", quantidadeLeadsCorporativo=" + quantidadeLeadsCorporativo + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((end == null) ? 0 : end.hashCode());
		result = prime * result + ((endTimeZone == null) ? 0 : endTimeZone.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((isAllDay == null) ? 0 : isAllDay.hashCode());
		result = prime * result + ((ownerId == null) ? 0 : ownerId.hashCode());
		result = prime * result + ((plantaoId == null) ? 0 : plantaoId.hashCode());
		result = prime * result + ((quantidadeIndicacoes == null) ? 0 : quantidadeIndicacoes.hashCode());
		result = prime * result + ((quantidadeLeadsCorporativo == null) ? 0 : quantidadeLeadsCorporativo.hashCode());
		result = prime * result + ((quantidadeLeadsFamiliar == null) ? 0 : quantidadeLeadsFamiliar.hashCode());
		result = prime * result + ((quantidadeLeadsIndividual == null) ? 0 : quantidadeLeadsIndividual.hashCode());
		result = prime * result + ((quantidadeLeadsPme == null) ? 0 : quantidadeLeadsPme.hashCode());
		result = prime * result + ((recurrenceCancelId == null) ? 0 : recurrenceCancelId.hashCode());
		result = prime * result + ((recurrenceException == null) ? 0 : recurrenceException.hashCode());
		result = prime * result + ((recurrenceId == null) ? 0 : recurrenceId.hashCode());
		result = prime * result + ((recurrenceRule == null) ? 0 : recurrenceRule.hashCode());
		result = prime * result + ((start == null) ? 0 : start.hashCode());
		result = prime * result + ((startTimeZone == null) ? 0 : startTimeZone.hashCode());
		result = prime * result + ((taskId == null) ? 0 : taskId.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Evento other = (Evento) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (end == null) {
			if (other.end != null)
				return false;
		} else if (!end.equals(other.end))
			return false;
		if (endTimeZone == null) {
			if (other.endTimeZone != null)
				return false;
		} else if (!endTimeZone.equals(other.endTimeZone))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isAllDay == null) {
			if (other.isAllDay != null)
				return false;
		} else if (!isAllDay.equals(other.isAllDay))
			return false;
		if (ownerId == null) {
			if (other.ownerId != null)
				return false;
		} else if (!ownerId.equals(other.ownerId))
			return false;
		if (plantaoId == null) {
			if (other.plantaoId != null)
				return false;
		} else if (!plantaoId.equals(other.plantaoId))
			return false;
		if (quantidadeIndicacoes == null) {
			if (other.quantidadeIndicacoes != null)
				return false;
		} else if (!quantidadeIndicacoes.equals(other.quantidadeIndicacoes))
			return false;
		if (quantidadeLeadsCorporativo == null) {
			if (other.quantidadeLeadsCorporativo != null)
				return false;
		} else if (!quantidadeLeadsCorporativo.equals(other.quantidadeLeadsCorporativo))
			return false;
		if (quantidadeLeadsFamiliar == null) {
			if (other.quantidadeLeadsFamiliar != null)
				return false;
		} else if (!quantidadeLeadsFamiliar.equals(other.quantidadeLeadsFamiliar))
			return false;
		if (quantidadeLeadsIndividual == null) {
			if (other.quantidadeLeadsIndividual != null)
				return false;
		} else if (!quantidadeLeadsIndividual.equals(other.quantidadeLeadsIndividual))
			return false;
		if (quantidadeLeadsPme == null) {
			if (other.quantidadeLeadsPme != null)
				return false;
		} else if (!quantidadeLeadsPme.equals(other.quantidadeLeadsPme))
			return false;
		if (recurrenceCancelId == null) {
			if (other.recurrenceCancelId != null)
				return false;
		} else if (!recurrenceCancelId.equals(other.recurrenceCancelId))
			return false;
		if (recurrenceException == null) {
			if (other.recurrenceException != null)
				return false;
		} else if (!recurrenceException.equals(other.recurrenceException))
			return false;
		if (recurrenceId == null) {
			if (other.recurrenceId != null)
				return false;
		} else if (!recurrenceId.equals(other.recurrenceId))
			return false;
		if (recurrenceRule == null) {
			if (other.recurrenceRule != null)
				return false;
		} else if (!recurrenceRule.equals(other.recurrenceRule))
			return false;
		if (start == null) {
			if (other.start != null)
				return false;
		} else if (!start.equals(other.start))
			return false;
		if (startTimeZone == null) {
			if (other.startTimeZone != null)
				return false;
		} else if (!startTimeZone.equals(other.startTimeZone))
			return false;
		if (taskId == null) {
			if (other.taskId != null)
				return false;
		} else if (!taskId.equals(other.taskId))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
    
    
    

}
