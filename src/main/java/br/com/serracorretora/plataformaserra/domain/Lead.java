package br.com.serracorretora.plataformaserra.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.Email;


/**
 * A lead.
 */
@Entity
@Table(name = "lead")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Lead extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Size(max = 255)
    @Column(name = "nome", length = 50)
    private String nome;
    
    @Email
    @NotNull
    @Size(max = 100)
    @Column(name="email", length = 100, unique = true)
    private String email;
    
    @Column(name = "ddd")
    private Long ddd;    
    
    @Size(max = 20)
    @Column(name = "telefone", length = 20)
    private String telefone;
    
    @Size(max = 255)
    @Column(name = "motivo_status", length = 50)
    private String motivoStatus;
    
    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(name="tipo_plano", length = 20)
    private TipoPlano tipoPlano;
    
    @Size(max = 255)
    @Column(name = "operadora", length = 50)
    private String operadora;
    
    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(length = 20)
    private Status status;
    
    @Column(name = "data_entrada", nullable = false)
    private ZonedDateTime dataEntrada;
    
    @Column(name = "data_alteracao_status")
    private ZonedDateTime dataAlteracaoStatus;
    
    @Column(name = "data_encaminhamento")
    private ZonedDateTime dataEncaminhamento;
    
    @Column(name = "data_retorno")
    private ZonedDateTime dataRetorno;    
    
    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="faixa_fk")
    private Faixa faixaEtaria;
    
    @Size(max = 255)
    @Column(name="observacoes")
    private String observacoes;
    
    @ManyToOne
    @JoinColumn(name="usuario_responsavel_fk")
    private User responsavel;
    
    @Column(name="responsavel_original")
    private Long responsavelOriginal;
    
    @Column(name="valor", precision=16, scale = 2)
    private BigDecimal valor;
    
    @Column(nullable = false)
    private boolean repescado = false;
    
    @Size(max = 255)
    @Column(name = "motivo", length = 255)
    private String motivo;
    
    @Enumerated(EnumType.STRING)
    @Column(name="tipo_atendimento", length = 20)
    private TipoAtendimento tipoAtendimento;
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getMotivoStatus() {
		return motivoStatus;
	}

	public void setMotivoStatus(String motivoStatus) {
		this.motivoStatus = motivoStatus;
	}

	public TipoPlano getTipoPlano() {
		return tipoPlano;
	}

	public void setTipoPlano(TipoPlano tipoPlano) {
		this.tipoPlano = tipoPlano;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public ZonedDateTime getDataEntrada() {
		return dataEntrada;
	}
	
	public User getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(User responsavel) {
		this.responsavel = responsavel;
	}
	

	public Long getResponsavelOriginal() {
		return responsavelOriginal;
	}

	public void setResponsavelOriginal(Long responsavelOriginal) {
		this.responsavelOriginal = responsavelOriginal;
	}

	public void setDataEntrada(ZonedDateTime dataEntrada) {
		this.dataEntrada = dataEntrada;
	}
	
	public Long getDdd() {
		return ddd;
	}

	public void setDdd(Long ddd) {
		this.ddd = ddd;
	}	

	public ZonedDateTime getDataAlteracaoStatus() {
		return dataAlteracaoStatus;
	}

	public void setDataAlteracaoStatus(ZonedDateTime dataAlteracaoStatus) {
		this.dataAlteracaoStatus = dataAlteracaoStatus;
	}

	public ZonedDateTime getDataEncaminhamento() {
		return dataEncaminhamento;
	}

	public void setDataEncaminhamento(ZonedDateTime dataEncaminhamento) {
		this.dataEncaminhamento = dataEncaminhamento;
	}

	public ZonedDateTime getDataRetorno() {
		return dataRetorno;
	}

	public void setDataRetorno(ZonedDateTime dataRetorno) {
		this.dataRetorno = dataRetorno;
	}

	public Faixa getFaixaEtaria() {
		return faixaEtaria;
	}

	public void setFaixaEtaria(Faixa faixaEtaria) {
		this.faixaEtaria = faixaEtaria;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public String getOperadora() {
		return operadora;
	}

	public void setOperadora(String operadora) {
		this.operadora = operadora;
	}



	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}



	public boolean isRepescado() {
		return repescado;
	}

	public void setRepescado(boolean repescado) {
		this.repescado = repescado;
	}



	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public TipoAtendimento getTipoAtendimento() {
		return tipoAtendimento;
	}

	public void setTipoAtendimento(TipoAtendimento tipoAtendimento) {
		this.tipoAtendimento = tipoAtendimento;
	}



	public enum TipoPlano {
        INDIVIDUAL, FAMILIAR, PME, EMPRESARIAL
    }	
	
	public enum TipoAtendimento {
        PESSIMO, NORMAL, OTIMO
    }
    
    public enum Status {
        DISPONIVEL("DISPONÍVEL"), VENDIDO("VENDIDO"), EM_ANDAMENTO("EM NEGOCIAÇÃO"), SEM_INTERESSE("PERDIDO"), EM_ATRASO("ATRASADO"), REPESCAGEM("REPESCAGEM");
    	
       private String descricao;
    	
 	   Status(String descricao) {
	        this.descricao = descricao;
	   }
 	   
    	public String getDescricao() {
    		return descricao;
    	}
    	
    }

	@Override
	public String toString() {
		return "Lead [id=" + id + ", nome=" + nome + ", email=" + email + ", ddd=" + ddd + ", telefone=" + telefone
				+ ", motivoStatus=" + motivoStatus + ", tipoPlano=" + tipoPlano + ", operadora=" + operadora
				+ ", status=" + status + ", dataEntrada=" + dataEntrada + ", dataAlteracaoStatus=" + dataAlteracaoStatus
				+ ", dataEncaminhamento=" + dataEncaminhamento + ", dataRetorno=" + dataRetorno + ", faixaEtaria="
				+ faixaEtaria + ", observacoes=" + observacoes + ", responsavelOriginal=" + responsavelOriginal
				+ ", valor=" + valor + ", repescado=" + repescado + ", motivo=" + motivo + ", tipoAtendimento="
				+ tipoAtendimento + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataAlteracaoStatus == null) ? 0 : dataAlteracaoStatus.hashCode());
		result = prime * result + ((dataEncaminhamento == null) ? 0 : dataEncaminhamento.hashCode());
		result = prime * result + ((dataEntrada == null) ? 0 : dataEntrada.hashCode());
		result = prime * result + ((dataRetorno == null) ? 0 : dataRetorno.hashCode());
		result = prime * result + ((ddd == null) ? 0 : ddd.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((faixaEtaria == null) ? 0 : faixaEtaria.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((motivo == null) ? 0 : motivo.hashCode());
		result = prime * result + ((motivoStatus == null) ? 0 : motivoStatus.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((observacoes == null) ? 0 : observacoes.hashCode());
		result = prime * result + ((operadora == null) ? 0 : operadora.hashCode());
		result = prime * result + (repescado ? 1231 : 1237);
		result = prime * result + ((responsavelOriginal == null) ? 0 : responsavelOriginal.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((telefone == null) ? 0 : telefone.hashCode());
		result = prime * result + ((tipoAtendimento == null) ? 0 : tipoAtendimento.hashCode());
		result = prime * result + ((tipoPlano == null) ? 0 : tipoPlano.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lead other = (Lead) obj;
		if (dataAlteracaoStatus == null) {
			if (other.dataAlteracaoStatus != null)
				return false;
		} else if (!dataAlteracaoStatus.equals(other.dataAlteracaoStatus))
			return false;
		if (dataEncaminhamento == null) {
			if (other.dataEncaminhamento != null)
				return false;
		} else if (!dataEncaminhamento.equals(other.dataEncaminhamento))
			return false;
		if (dataEntrada == null) {
			if (other.dataEntrada != null)
				return false;
		} else if (!dataEntrada.equals(other.dataEntrada))
			return false;
		if (dataRetorno == null) {
			if (other.dataRetorno != null)
				return false;
		} else if (!dataRetorno.equals(other.dataRetorno))
			return false;
		if (ddd == null) {
			if (other.ddd != null)
				return false;
		} else if (!ddd.equals(other.ddd))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (faixaEtaria == null) {
			if (other.faixaEtaria != null)
				return false;
		} else if (!faixaEtaria.equals(other.faixaEtaria))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (motivo == null) {
			if (other.motivo != null)
				return false;
		} else if (!motivo.equals(other.motivo))
			return false;
		if (motivoStatus == null) {
			if (other.motivoStatus != null)
				return false;
		} else if (!motivoStatus.equals(other.motivoStatus))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (observacoes == null) {
			if (other.observacoes != null)
				return false;
		} else if (!observacoes.equals(other.observacoes))
			return false;
		if (operadora == null) {
			if (other.operadora != null)
				return false;
		} else if (!operadora.equals(other.operadora))
			return false;
		if (repescado != other.repescado)
			return false;
		if (responsavelOriginal == null) {
			if (other.responsavelOriginal != null)
				return false;
		} else if (!responsavelOriginal.equals(other.responsavelOriginal))
			return false;
		if (status != other.status)
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		if (tipoAtendimento != other.tipoAtendimento)
			return false;
		if (tipoPlano != other.tipoPlano)
			return false;
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		return true;
	}
    
}
