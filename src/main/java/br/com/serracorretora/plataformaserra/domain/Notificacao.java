package br.com.serracorretora.plataformaserra.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;


/**
 Pendências
 */
@Entity
@Table(name = "notificacoes")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Notificacao extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "data_entrada", nullable = false)
    private ZonedDateTime dataEntrada;    
    
    @ManyToOne
    @JoinColumn(name="usuario_responsavel_fk")
    private User responsavel;
    
    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(length = 20)
    private StatusPendencia statusPendencia;
    
    @NotNull
    @Size(max = 100)
    @Column(name="mensagem_cobranca", length = 255)
    private String cobranca;
    
    @Column(name="numero_lead")
    private Long numeroLead;
    
    @NotNull
    @Size(max = 100)
    @Column(name="mensagem_justificativa", length = 255)
    private String justificativa;    
    
    
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public User getResponsavel() {
		return responsavel;
	}


	public void setResponsavel(User responsavel) {
		this.responsavel = responsavel;
	}


	public ZonedDateTime getDataEntrada() {
		return dataEntrada;
	}


	public void setDataEntrada(ZonedDateTime dataEntrada) {
		this.dataEntrada = dataEntrada;
	}


	public StatusPendencia getStatusPendencia() {
		return statusPendencia;
	}


	public void setStatusPendencia(StatusPendencia statusPendencia) {
		this.statusPendencia = statusPendencia;
	}


	public String getCobranca() {
		return cobranca;
	}


	public void setCobranca(String cobranca) {
		this.cobranca = cobranca;
	}


	public String getJustificativa() {
		return justificativa;
	}


	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}


	public Long getNumeroLead() {
		return numeroLead;
	}


	public void setNumeroLead(Long numeroLead) {
		this.numeroLead = numeroLead;
	}


	public enum StatusPendencia {
        SOLUCIONADO, PENDENTE, INFO
    }


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cobranca == null) ? 0 : cobranca.hashCode());
		result = prime * result + ((dataEntrada == null) ? 0 : dataEntrada.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((justificativa == null) ? 0 : justificativa.hashCode());
		result = prime * result + ((numeroLead == null) ? 0 : numeroLead.hashCode());
		result = prime * result + ((responsavel == null) ? 0 : responsavel.hashCode());
		result = prime * result + ((statusPendencia == null) ? 0 : statusPendencia.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Notificacao other = (Notificacao) obj;
		if (cobranca == null) {
			if (other.cobranca != null)
				return false;
		} else if (!cobranca.equals(other.cobranca))
			return false;
		if (dataEntrada == null) {
			if (other.dataEntrada != null)
				return false;
		} else if (!dataEntrada.equals(other.dataEntrada))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (justificativa == null) {
			if (other.justificativa != null)
				return false;
		} else if (!justificativa.equals(other.justificativa))
			return false;
		if (numeroLead == null) {
			if (other.numeroLead != null)
				return false;
		} else if (!numeroLead.equals(other.numeroLead))
			return false;
		if (responsavel == null) {
			if (other.responsavel != null)
				return false;
		} else if (!responsavel.equals(other.responsavel))
			return false;
		if (statusPendencia != other.statusPendencia)
			return false;
		return true;
	}

}


