package br.com.serracorretora.plataformaserra.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 Ranking stats of user.
 */
@Entity
@Table(name = "ranking")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Ranking extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @JsonIgnore
    @NotNull
    @ManyToOne
    @JoinColumn(name="usuario_fk")
    private User usuario;    
    
    @Column(name = "data_competencia")
    private ZonedDateTime dataCompetencia;    
    
    @Column(name="valor_base", precision=16, scale = 2)
    private BigDecimal valorBase;
    
    @Column(name="pontos")
    private Long pontos;
    
    @Column(name="valor_vendas", precision=16, scale = 2)
    private BigDecimal valorVendas;
    
    @Column(name="quantidade_vendas")
    private Long quantidadeVendas;

    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getDataCompetencia() {
		return dataCompetencia;
	}

	public void setDataCompetencia(ZonedDateTime dataCompetencia) {
		this.dataCompetencia = dataCompetencia;
	}

	public BigDecimal getValorBase() {
		return valorBase;
	}

	public void setValorBase(BigDecimal valorBase) {
		this.valorBase = valorBase;
	}

	public Long getPontos() {
		return pontos;
	}

	public void setPontos(Long pontos) {
		this.pontos = pontos;
	}

	public BigDecimal getValorVendas() {
		return valorVendas;
	}

	public void setValorVendas(BigDecimal valorVendas) {
		this.valorVendas = valorVendas;
	}

	public Long getQuantidadeVendas() {
		return quantidadeVendas;
	}

	public void setQuantidadeVendas(Long quantidadeVendas) {
		this.quantidadeVendas = quantidadeVendas;
	}

	public User getUsuario() {
		return usuario;
	}

	public void setUsuario(User usuario) {
		this.usuario = usuario;
	}

	@Override
	public String toString() {
		return "Ranking [id=" + id + ", dataCompetencia=" + dataCompetencia + ", valorBase=" + valorBase + ", pontos="
				+ pontos + ", valorVendas=" + valorVendas + ", quantidadeVendas=" + quantidadeVendas + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataCompetencia == null) ? 0 : dataCompetencia.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((pontos == null) ? 0 : pontos.hashCode());
		result = prime * result + ((quantidadeVendas == null) ? 0 : quantidadeVendas.hashCode());
		result = prime * result + ((valorBase == null) ? 0 : valorBase.hashCode());
		result = prime * result + ((valorVendas == null) ? 0 : valorVendas.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ranking other = (Ranking) obj;
		if (dataCompetencia == null) {
			if (other.dataCompetencia != null)
				return false;
		} else if (!dataCompetencia.equals(other.dataCompetencia))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (pontos == null) {
			if (other.pontos != null)
				return false;
		} else if (!pontos.equals(other.pontos))
			return false;
		if (quantidadeVendas == null) {
			if (other.quantidadeVendas != null)
				return false;
		} else if (!quantidadeVendas.equals(other.quantidadeVendas))
			return false;
		if (valorBase == null) {
			if (other.valorBase != null)
				return false;
		} else if (!valorBase.equals(other.valorBase))
			return false;
		if (valorVendas == null) {
			if (other.valorVendas != null)
				return false;
		} else if (!valorVendas.equals(other.valorVendas))
			return false;
		return true;
	}	

    
}
