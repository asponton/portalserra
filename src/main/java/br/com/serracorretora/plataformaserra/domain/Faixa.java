package br.com.serracorretora.plataformaserra.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name="faixa")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Faixa implements Serializable {

	private static final long serialVersionUID = 1L;
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;	
	
	@Column(name="_0a18")
	private int _0a18;
	
	@Column(name="_19a23")
	private int _19a23;
	
	@Column(name="_24a28")
	private int _24a28;
	
	@Column(name="_29a33")
	private int _29a33;
	
	@Column(name="_34a38")
	private int _34a38;
	
	@Column(name="_39a43")
	private int _39a43;
	
	@Column(name="_44a48")
	private int _44a48;
	
	@Column(name="_49a53")
	private int _49a53;
	
	@Column(name="_54_plus")
	private int _54_plus;
	
	
	public int get_0a18() {
		return _0a18;
	}

	public void set_0a18(int _0a18) {
		this._0a18 = _0a18;
	}

	public int get_19a23() {
		return _19a23;
	}

	public void set_19a23(int _19a23) {
		this._19a23 = _19a23;
	}

	public int get_24a28() {
		return _24a28;
	}

	public void set_24a28(int _24a28) {
		this._24a28 = _24a28;
	}

	public int get_29a33() {
		return _29a33;
	}

	public void set_29a33(int _29a33) {
		this._29a33 = _29a33;
	}

	public int get_34a38() {
		return _34a38;
	}

	public void set_34a38(int _34a38) {
		this._34a38 = _34a38;
	}

	public int get_39a43() {
		return _39a43;
	}

	public void set_39a43(int _39a43) {
		this._39a43 = _39a43;
	}

	public int get_44a48() {
		return _44a48;
	}

	public void set_44a48(int _44a48) {
		this._44a48 = _44a48;
	}

	public int get_49a53() {
		return _49a53;
	}

	public void set_49a53(int _49a53) {
		this._49a53 = _49a53;
	}

	public int get_54_plus() {
		return _54_plus;
	}

	public void set_54_plus(int _54_plus) {
		this._54_plus = _54_plus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + _0a18;
		result = prime * result + _19a23;
		result = prime * result + _24a28;
		result = prime * result + _29a33;
		result = prime * result + _34a38;
		result = prime * result + _39a43;
		result = prime * result + _44a48;
		result = prime * result + _49a53;
		result = prime * result + _54_plus;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Faixa other = (Faixa) obj;
		if (_0a18 != other._0a18)
			return false;
		if (_19a23 != other._19a23)
			return false;
		if (_24a28 != other._24a28)
			return false;
		if (_29a33 != other._29a33)
			return false;
		if (_34a38 != other._34a38)
			return false;
		if (_39a43 != other._39a43)
			return false;
		if (_44a48 != other._44a48)
			return false;
		if (_49a53 != other._49a53)
			return false;
		if (_54_plus != other._54_plus)
			return false;
		return true;
	}

}
