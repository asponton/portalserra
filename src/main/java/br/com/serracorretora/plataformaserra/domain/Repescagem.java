package br.com.serracorretora.plataformaserra.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.serracorretora.plataformaserra.domain.Lead.Status;


@Entity
@Table(name = "repescagem")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Repescagem extends AbstractAuditingEntity implements Serializable {

	
	private static final long serialVersionUID = -1542552914508443315L;

	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
    @Column(name = "inicio", nullable = true)
	private ZonedDateTime inicio;
	
    @Column(name = "fim", nullable = true)
	private ZonedDateTime fim;
	
    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(length = 20)
	private Status status;
	
    @Column(nullable = false)
	private boolean aprovada;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getInicio() {
		return inicio;
	}

	public void setInicio(ZonedDateTime inicio) {
		this.inicio = inicio;
	}

	public ZonedDateTime getFim() {
		return fim;
	}

	public void setFim(ZonedDateTime fim) {
		this.fim = fim;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public boolean isAprovada() {
		return aprovada;
	}

	public void setAprovada(boolean aprovada) {
		this.aprovada = aprovada;
	}
	
	

}
