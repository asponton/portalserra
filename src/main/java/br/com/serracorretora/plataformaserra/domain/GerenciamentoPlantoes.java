package br.com.serracorretora.plataformaserra.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A lead.
 */
@Entity
@Table(name = "gerenciamento")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class GerenciamentoPlantoes extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 5723554534129634725L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "periodo_inicial")
	private ZonedDateTime periodoInicial;
	
	@Column(name = "periodo_final")
	private ZonedDateTime periodoFinal;
	
	 @Column(name = "periodo_fechado")
	private Boolean periodoFechado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getPeriodoInicial() {
		return periodoInicial;
	}

	public void setPeriodoInicial(ZonedDateTime periodoInicial) {
		this.periodoInicial = periodoInicial;
	}

	public ZonedDateTime getPeriodoFinal() {
		return periodoFinal;
	}

	public void setPeriodoFinal(ZonedDateTime periodoFinal) {
		this.periodoFinal = periodoFinal;
	}

	public Boolean getPeriodoFechado() {
		return periodoFechado;
	}

	public void setPeriodoFechado(Boolean periodoFechado) {
		this.periodoFechado = periodoFechado;
	}

	@Override
	public String toString() {
		return "GerenciamentoPlantoes [id=" + id + ", periodoInicial=" + periodoInicial + ", periodoFinal="
				+ periodoFinal + ", periodoFechado=" + periodoFechado + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((periodoFechado == null) ? 0 : periodoFechado.hashCode());
		result = prime * result + ((periodoFinal == null) ? 0 : periodoFinal.hashCode());
		result = prime * result + ((periodoInicial == null) ? 0 : periodoInicial.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GerenciamentoPlantoes other = (GerenciamentoPlantoes) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (periodoFechado == null) {
			if (other.periodoFechado != null)
				return false;
		} else if (!periodoFechado.equals(other.periodoFechado))
			return false;
		if (periodoFinal == null) {
			if (other.periodoFinal != null)
				return false;
		} else if (!periodoFinal.equals(other.periodoFinal))
			return false;
		if (periodoInicial == null) {
			if (other.periodoInicial != null)
				return false;
		} else if (!periodoInicial.equals(other.periodoInicial))
			return false;
		return true;
	}
	

}
