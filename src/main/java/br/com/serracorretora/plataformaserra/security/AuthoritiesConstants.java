package br.com.serracorretora.plataformaserra.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";
    
    public static final String GERENCIA_EXTERNA = "GERENCIA_EXTERNA";
    
    public static final String GERENCIA_INTERNA = "GERENCIA_INTERNA";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    private AuthoritiesConstants() {
    }
}
