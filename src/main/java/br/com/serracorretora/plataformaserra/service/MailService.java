package br.com.serracorretora.plataformaserra.service;

import br.com.serracorretora.plataformaserra.config.JHipsterProperties;
import br.com.serracorretora.plataformaserra.domain.Evento;
import br.com.serracorretora.plataformaserra.domain.User;
import br.com.serracorretora.plataformaserra.service.dto.EventoDTO;
import br.com.serracorretora.plataformaserra.service.dto.LeadDTO;

import org.apache.commons.lang3.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;


import javax.inject.Inject;
import javax.mail.internet.MimeMessage;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Service for sending e-mails.
 * <p>
 * We use the @Async annotation to send e-mails asynchronously.
 * </p>
 */
@Service
public class MailService {

    private final Logger log = LoggerFactory.getLogger(MailService.class);

    private static final String USER = "user";
    private static final String BASE_URL = "baseUrl";

    @Inject
    private JHipsterProperties jHipsterProperties;

    @Inject
    private JavaMailSenderImpl javaMailSender;

    @Inject
    private MessageSource messageSource;

    @Inject
    private SpringTemplateEngine templateEngine;

    @Async
    public void sendEmail(String to, String bcc, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send e-mail[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart, isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
            message.setTo(to);
            if (bcc != null) {
            	message.setBcc(bcc);
        	}
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent e-mail to User '{}'", to);
        } catch (Exception e) {
            log.warn("E-mail could not be sent to user '{}'", to, e);
        }
    }

    @Async
    public void sendActivationEmail(User user, String baseUrl) {
        log.debug("Sending activation e-mail to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, baseUrl);
        String content = templateEngine.process("activationEmail", context);
        String subject = messageSource.getMessage("email.activation.title", null, locale);
        sendEmail(user.getEmail(), null, subject, content, false, true);
    }

    @Async
    public void sendCreationEmail(User user, String baseUrl) {
        log.debug("Sending creation e-mail to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, baseUrl);
        String content = templateEngine.process("creationEmail", context);
        String subject = messageSource.getMessage("email.activation.title", null, locale);
        sendEmail(user.getEmail(), null, subject, content, false, true);
    }

    @Async
    public void sendPasswordResetMail(User user, String baseUrl) {
        log.debug("Sending password reset e-mail to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, baseUrl);
        String content = templateEngine.process("passwordResetEmail", context);
        String subject = messageSource.getMessage("email.reset.title", null, locale);
        sendEmail(user.getEmail(), null, subject, content, false, true);
    }
    
    @Async
    public void sendEventCreationEmail(User user, String baseUrl, EventoDTO eventoDTO) {
        log.debug("Sending event creation e-mail to '{}'", user.getEmail());
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        
        Context context = new Context();
        context.setVariable("user", user);
        context.setVariable("baseUrl", baseUrl);
        context.setVariable("data", eventoDTO.getStart().format(format));
        String content = templateEngine.process("eventCreationEmail", context);
        String subject = "Serra - Novo Agendamento de Plantão";
        sendEmail(user.getEmail(), user.getResponsavel().getEmail(), subject, content, false, true);
    }    
    
    @Async
    public void sendPlantaoCreationEmail(User user, String baseUrl, List<Evento> eventosDoPlantonista) {
        log.debug("Sending event creation e-mail to '{}'", user.getEmail());
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        List<String> datasFormatadas = new ArrayList<String>();
        
        for (Evento evento : eventosDoPlantonista) {
        	datasFormatadas.add(evento.getStart().format(format));
        }
        
        Context context = new Context();
        context.setVariable("user", user);
        context.setVariable("baseUrl", baseUrl);
        context.setVariable("plantoes", datasFormatadas);
        String content = templateEngine.process(user.getRangeDddMaximo() <= 11L ? "plantaoCreationEmail" : "plantaoInteriorCreationEmail", context);
        String subject = "Serra - Novo Agendamento de Plantão";
        sendEmail(user.getEmail(), user.getResponsavel().getEmail(), subject, content, false, true);
    }        


    
    @Async
    public void sendPesquisaQualidadeDoAtendimento(LeadDTO leadDTO, String baseUrl) {
        log.debug("Sending pesquisa de atendimento to '{}'", leadDTO.getEmail());
        Context context = new Context();
        context.setVariable("lead", leadDTO);
        context.setVariable("baseUrl", baseUrl);
        String content = templateEngine.process("pesquisaAtendimento", context);
        String subject = "Serra - Pesquisa da qualidade de atendimento do Corretor";
        sendEmail(leadDTO.getEmail(), null, subject, content, false, true);
    }
    
}
