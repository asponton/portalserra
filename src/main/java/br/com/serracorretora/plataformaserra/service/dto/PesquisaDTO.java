package br.com.serracorretora.plataformaserra.service.dto;

import br.com.serracorretora.plataformaserra.domain.Lead.TipoAtendimento;

public class PesquisaDTO {
	
	private String motivo;
	
	private TipoAtendimento tipoAtendimento;
	
	private Long idLead;
	
	public PesquisaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public TipoAtendimento getTipoAtendimento() {
		return tipoAtendimento;
	}

	public void setTipoAtendimento(TipoAtendimento tipoAtendimento) {
		this.tipoAtendimento = tipoAtendimento;
	}

	public Long getIdLead() {
		return idLead;
	}

	public void setIdLead(Long idLead) {
		this.idLead = idLead;
	}	
	

}
