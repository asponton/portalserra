package br.com.serracorretora.plataformaserra.service.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ResultadoPorPeriodo {
	
	private List<String> mes;
	
	private List<Integer> valor;
	
	private String nomeTime;
	
	private Integer valorTimeSemestral;
	
	private Integer valorTimeMensal;
	
	private List<UserDTO> integrantes;
	
	private BigDecimal percentualAtingido;
	
	private List<UserDTO> campeoesDoMes;
	
	private String statusMetaTime;
	
    private static BigDecimal ABAIXO_DA_META = new BigDecimal("35");
    
    private static BigDecimal REGULAR_DA_META = new BigDecimal("80");
    
	public ResultadoPorPeriodo(List<String> mes, List<Integer> valor) {
		this.mes = mes;
		this.valor = valor;
	}

	
	public ResultadoPorPeriodo(List<String> mes, List<Integer> valor, List<UserDTO> campeoes) {
		this.mes = mes;
		this.valor = valor;
		this.campeoesDoMes = campeoes;
	}
	
	public ResultadoPorPeriodo() {
		this.mes = new ArrayList<String>();
		this.valor = new ArrayList<Integer>();
		this.valorTimeSemestral = 0;
		this.valorTimeMensal = 0;
		this.nomeTime = "";
		this.integrantes = new ArrayList<UserDTO>();
		this.percentualAtingido =BigDecimal.ZERO;
		this.statusMetaTime = "";
	}

	public List<String> getMes() {
		return mes;
	}

	public void setMes(List<String> mes) {
		this.mes = mes;
	}

	public List<Integer> getValor() {
		return valor;
	}

	public void setValor(List<Integer> valor) {
		this.valor = valor;
	}

	public Integer getValorTimeSemestral() {
		return valorTimeSemestral;
	}

	public void setValorTimeSemestral(Integer valorTimeSemestral) {
		this.valorTimeSemestral = valorTimeSemestral;
	}

	public String getNomeTime() {
		return nomeTime;
	}

	public void setNomeTime(String nomeTime) {
		this.nomeTime = nomeTime;
	}

	public Integer getValorTimeMensal() {
		return valorTimeMensal;
	}

	public void setValorTimeMensal(Integer valorTimeMensal) {
		this.valorTimeMensal = valorTimeMensal;
	}

	public List<UserDTO> getIntegrantes() {
		return integrantes;
	}

	public void setIntegrantes(List<UserDTO> integrantes) {
		this.integrantes = integrantes;
	}
	
	public BigDecimal getPercentualAtingido() {
		return this.percentualAtingido;
	}

	public void setPercentualAtingido(BigDecimal percentualAtingido) {
		this.percentualAtingido = percentualAtingido;
	}	


	private BigDecimal getMeta(){
		BigDecimal meta = BigDecimal.ZERO;
		for (UserDTO userDTO : this.getIntegrantes()) {
			meta = meta.add(userDTO.getMeta() != null ? userDTO.getMeta() : BigDecimal.ZERO);
		}
		return meta;
	}
	
	private BigDecimal getValorVendas() {
		BigDecimal valor = BigDecimal.ZERO;
		for (UserDTO userDTO : this.getIntegrantes()) {
			valor = valor.add(userDTO.getRanking().getValorVendas());
		}
		return valor;
	}
	
	public BigDecimal buscaPercentualMetaConcluidaTime() {
		if (this.getValorVendas().equals(BigDecimal.ZERO) && this.getMeta().equals(BigDecimal.ZERO)) {
			return BigDecimal.ZERO;
		}
		return this.getValorVendas().multiply(new BigDecimal(100)).divide(this.getMeta());
	}
	
	public String buscaStatus() {
		return this.buscaPercentualMetaConcluidaTime().compareTo(ABAIXO_DA_META) ==  -1 ? "#ff3535" : this.buscaPercentualMetaConcluidaTime().compareTo(REGULAR_DA_META) == -1 ? "#ffdb00" : "#00FF38" ; 
	}

	public List<UserDTO> getCampeoesDoMes() {
		return campeoesDoMes;
	}

	public void setCampeoesDoMes(List<UserDTO> campeoesDoMes) {
		this.campeoesDoMes = campeoesDoMes;
	}


	public String getStatusMetaTime() {
		return statusMetaTime;
	}

	public void setStatusMetaTime(String statusMetaTime) {
		this.statusMetaTime = statusMetaTime;
	}

	
}
