package br.com.serracorretora.plataformaserra.service.dto;

import br.com.serracorretora.plataformaserra.config.Constants;

import br.com.serracorretora.plataformaserra.domain.Authority;
import br.com.serracorretora.plataformaserra.domain.Cargo;
import br.com.serracorretora.plataformaserra.domain.Contato;
import br.com.serracorretora.plataformaserra.domain.Evento;
import br.com.serracorretora.plataformaserra.domain.Ranking;
import br.com.serracorretora.plataformaserra.domain.Time;
import br.com.serracorretora.plataformaserra.domain.User;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A DTO representing a user, with his authorities.
 */
public class UserDTO {
	
	private Long idUser;

    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    private String login;

    @Size(max = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;

    @Email
    @Size(min = 5, max = 100)
    private String email;
    
    private Time time;
    
    private String cpf;
    
    private Cargo cargo;
    
	private List<Contato> contatos;
    
    private User responsavel;
    
    private BigDecimal meta;
    
    private RankingDTO ranking;
    
    private List<EventoDTO> plantoes;
    
    private BigDecimal percentualAtingido;
    
    private String urlFoto;
    
    private String urlFotoPainel;
    
    private Long limiteLeads;
    
	private Long quantidadeLeadsIndividual;
	
	private Long quantidadeLeadsFamiliar;
	
	private Long quantidadeLeadsPme;
	
	private Long quantidadeLeadsEmpresarial;
	
	private Long rangeDddMinimo;
	
	private Long rangeDddMaximo;      
    
    private static BigDecimal ABAIXO_DA_META = new BigDecimal("35");
    
    private static BigDecimal REGULAR_DA_META = new BigDecimal("80");    

    private boolean activated = false;

    @Size(min = 2, max = 5)
    private String langKey;

    private Set<String> authorities;

    public UserDTO() {
    }

    public UserDTO(User user) {
        this(user.getId(), user.getLogin(), user.getFirstName(), user.getLastName(),
            user.getEmail(), user.getCpf(), user.getTime(), user.getActivated(), user.getLangKey(),
            user.getAuthorities().stream().map(Authority::getName).collect(Collectors.toSet()), user.getCargo(), user.getContatos(), Optional.ofNullable(user.getResponsavel()), user.getMeta(), null, null, 
            user.getFotoUrl(), user.getFotoPainel(), user.getLimiteLeads(),
            user.getQuantidadeLeadsIndividual(), user.getQuantidadeLeadsFamiliar(), user.getQuantidadeLeadsPme(), user.getQuantidadeLeadsEmpresarial(),
            user.getRangeDddMinimo(), user.getRangeDddMaximo());
    }
    
    
    public UserDTO(Long id, String login, String firstName, String lastName,
        String email, String cpf, Time time, boolean activated, String langKey, Set<String> authorities, Cargo cargo, List<Contato> contatos, Optional<User> responsavel, 
        BigDecimal meta, Ranking ranking, List<Evento> plantoes, String urlFoto, String fotoPainel, Long limiteLeads, 
        Long quantidadeLeadsIndividual, Long quantidadeLeadsFamiliar, Long quantidadeLeadsPme, Long quantidadeLeadsEmpresarial, Long rangeMinimo, Long rangeMaximo) {
    	
    	this.idUser = id;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.cpf = cpf;
        this.activated = activated;
        this.langKey = langKey;
        this.authorities = authorities;
        this.cargo = cargo;
        this.time = time;
        this.contatos = contatos;
        this.responsavel = responsavel.isPresent() ? responsavel.get() : null;
        this.meta = meta;
        this.ranking = ranking != null ? new RankingDTO(ranking) : new RankingDTO();
        this.plantoes = plantoes == null || plantoes.isEmpty() ? null : plantoes.stream().map(plantao -> new EventoDTO(plantao)).collect(Collectors.toList());
        this.percentualAtingido = BigDecimal.ZERO;
        this.urlFoto = urlFoto;
        this.urlFotoPainel = fotoPainel;
        this.limiteLeads = limiteLeads;
        this.quantidadeLeadsIndividual = quantidadeLeadsIndividual;
        this.quantidadeLeadsFamiliar = quantidadeLeadsFamiliar;
        this.quantidadeLeadsPme = quantidadeLeadsPme;
        this.quantidadeLeadsEmpresarial = quantidadeLeadsEmpresarial;
        this.rangeDddMinimo = rangeMinimo;
        this.rangeDddMaximo = rangeMaximo;
    }

    public Long getIdUser() {
		return idUser;
	}

	public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public boolean isActivated() {
        return activated;
    }

    public String getLangKey() {
        return langKey;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

    public List<Contato> getContatos() {
		return contatos;
	}
    
    public Cargo getCargo() {
		return cargo;
	}


	public User getResponsavel() {
		return responsavel;
	}

	public BigDecimal getMeta() {
		return meta;
	}

	public RankingDTO getRanking() {
		return ranking;
	}

	public void setRanking(RankingDTO ranking) {
		this.ranking = ranking;
	}

	public List<EventoDTO> getPlantoes() {
		return plantoes;
	}

	public BigDecimal getPercentualAtingido() {
		return percentualAtingido;
	}

	public void setPercentualAtingido(BigDecimal percentualAtingido) {
		this.percentualAtingido = percentualAtingido;
	}

	public String getUrlFoto() {
		return urlFoto;
	}

	public String getUrlFotoPainel() {
		return urlFotoPainel;
	}

	public Long getLimiteLeads() {
		return limiteLeads;
	}

	public Long getQuantidadeLeadsIndividual() {
		return quantidadeLeadsIndividual;
	}

	public Long getQuantidadeLeadsFamiliar() {
		return quantidadeLeadsFamiliar;
	}

	public Long getQuantidadeLeadsPme() {
		return quantidadeLeadsPme;
	}

	public Long getQuantidadeLeadsEmpresarial() {
		return quantidadeLeadsEmpresarial;
	}

	public Long getRangeDddMinimo() {
		return rangeDddMinimo;
	}

	public Long getRangeDddMaximo() {
		return rangeDddMaximo;
	}

	@Override
    public String toString() {
        return "UserDTO{" +
            "login='" + login + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", email='" + email + '\'' +
            ", activated=" + activated +
            ", langKey='" + langKey + '\'' +
            ", authorities=" + authorities +
            "}";
    }
    
	public BigDecimal buscaPorcentagemConcluidaMetaCorretor() {
		if (this.getRanking().getValorVendas() != null) {
			return this.getRanking().getValorVendas().multiply(new BigDecimal(100)).divide(this.meta);
		} else {
			return BigDecimal.ZERO;
		}
	}
	
	public String buscaStatusMeta() {
		return this.buscaPorcentagemConcluidaMetaCorretor().compareTo(ABAIXO_DA_META) ==  -1 ? "#ff3535" : this.buscaPorcentagemConcluidaMetaCorretor().compareTo(REGULAR_DA_META) == -1 ? "#ffdb00" : "#00FF38" ; 
	}
	
	public boolean isHabilitadoParaPlantonista() {
		return this.getPlantoes() != null && !this.getPlantoes().isEmpty();
	}
	
	public boolean isExisteFoto() {
		return this.getUrlFoto() != null;
	}	
    
}
