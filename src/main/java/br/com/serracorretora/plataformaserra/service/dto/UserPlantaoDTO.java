package br.com.serracorretora.plataformaserra.service.dto;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import br.com.serracorretora.plataformaserra.domain.User;

public class UserPlantaoDTO {

    private List<ResourceDTO> resourceDTO;

    private List<FilterDTO> filterDTO;
        
    public UserPlantaoDTO() {
        super();
    }

    public UserPlantaoDTO(List<User> usuarios) {
        super();
        this.resourceDTO = this.filtrarUsuarioForResourceDTO(usuarios);
        this.filterDTO = this.filtrarUsuariosForFilterDTO(usuarios);
    }

	private List<FilterDTO> filtrarUsuariosForFilterDTO(List<User> usuarios) {
		List<FilterDTO> filters = new ArrayList<FilterDTO>();
		for (User usuario : usuarios) {
			filters.add(new FilterDTO(usuario.getId()));
		}
		filters.sort(new Comparator<FilterDTO>() {

			@Override
			public int compare(FilterDTO o1, FilterDTO o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
			
		});
		return filters;
	}

	private List<ResourceDTO> filtrarUsuarioForResourceDTO(List<User> usuarios) {
		List<ResourceDTO> resources = new ArrayList<ResourceDTO>();
		for (User usuario : usuarios) {
			if ("VENDEDOR".equals(usuario.getCargo().getName().toUpperCase()) || "SUPERVISOR".equals(usuario.getCargo().getName().toUpperCase())) {
				resources.add(new ResourceDTO(usuario.getFirstName(), usuario.getId(), usuario.getTime().getCor()));
			}
		}
		resources.sort(new Comparator<ResourceDTO>() {

			@Override
			public int compare(ResourceDTO o1, ResourceDTO o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
			
		});
		
		return resources;
	}

	public List<ResourceDTO> getResourceDTO() {
		return resourceDTO;
	}

	public void setResourceDTO(List<ResourceDTO> resourceDTO) {
		this.resourceDTO = resourceDTO;
	}

	public List<FilterDTO> getFilterDTO() {
		return filterDTO;
	}

	public void setFilterDTO(List<FilterDTO> filterDTO) {
		this.filterDTO = filterDTO;
	}


    
}
