package br.com.serracorretora.plataformaserra.service.dto;

import java.time.ZonedDateTime;

import br.com.serracorretora.plataformaserra.domain.Evento;

public class EventoDTO {
	
	private Long id;

	private Long taskId;
	
	private Long ownerId;
	
	private Long plantaoId;
	
	private String title;
	
	private String description;
	
	private ZonedDateTime startTimeZone;
	
	private ZonedDateTime start;
	
	private ZonedDateTime end;
	
	private ZonedDateTime endTimeZone;
	
	private String recurrenceRule;
	
	private Long recurrenceCancelId;
	
	private Long recurrenceId;
	
	private String recurrenceException;

	private Boolean isAllDay;
	
	public EventoDTO() {
		super();
	}
	
	public EventoDTO(Evento evento) {
		this(evento.getId(), evento.getTaskId(), evento.getOwnerId(), evento.getPlantaoId(), evento.getTitle(),evento.getDescription(), evento.getStartTimeZone(), evento.getStart(), evento.getEnd(), evento.getEndTimeZone(), 
				evento.getRecurrenceRule(), evento.getRecurrenceCancelId(), evento.getRecurrenceId(), evento.getRecurrenceException(), evento.getIsAllDay());
	}

	public EventoDTO(Long id, Long idEvento, Long idUser, Long plantaoId, String title, String description, ZonedDateTime startTimeZone, ZonedDateTime start, ZonedDateTime end, ZonedDateTime endTimeZone,
			String recurrenceRule, Long recurrenceCancelId, Long recurrenceId, String recurrenceException, Boolean isAllDay) {
		this.id = id;
		this.taskId = idEvento;
		this.ownerId = idUser;
		this.plantaoId = plantaoId;
		this.title = title;
		this.description = description;
		this.startTimeZone = startTimeZone;
		this.start = start;
		this.end = end;
		this.endTimeZone = endTimeZone;
		this.recurrenceRule = recurrenceRule;
		this.recurrenceCancelId = recurrenceCancelId;
		this.recurrenceId = recurrenceId;
		this.recurrenceException = recurrenceException;
		this.isAllDay = isAllDay;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}

	public Long getPlantaoId() {
		return plantaoId;
	}

	public void setPlantaoId(Long plantaoId) {
		this.plantaoId = plantaoId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ZonedDateTime getStartTimeZone() {
		return startTimeZone;
	}

	public void setStartTimeZone(ZonedDateTime startTimeZone) {
		this.startTimeZone = startTimeZone;
	}

	public ZonedDateTime getStart() {
		return start;
	}

	public void setStart(ZonedDateTime start) {
		this.start = start;
	}

	public ZonedDateTime getEnd() {
		return end;
	}

	public void setEnd(ZonedDateTime end) {
		this.end = end;
	}

	public ZonedDateTime getEndTimeZone() {
		return endTimeZone;
	}

	public void setEndTimeZone(ZonedDateTime endTimeZone) {
		this.endTimeZone = endTimeZone;
	}

	public String getRecurrenceRule() {
		return recurrenceRule;
	}

	public void setRecurrenceRule(String recurrenceRule) {
		this.recurrenceRule = recurrenceRule;
	}

	public Long getRecurrenceCancelId() {
		return recurrenceCancelId;
	}

	public void setRecurrenceCancelId(Long recurrenceCancelId) {
		this.recurrenceCancelId = recurrenceCancelId;
	}

	public Long getRecurrenceId() {
		return recurrenceId;
	}

	public void setRecurrenceId(Long recurrenceId) {
		this.recurrenceId = recurrenceId;
	}

	public String getRecurrenceException() {
		return recurrenceException;
	}

	public void setRecurrenceException(String recurrenceException) {
		this.recurrenceException = recurrenceException;
	}

	public Boolean getIsAllDay() {
		return isAllDay;
	}

	public void setIsAllDay(Boolean isAllDay) {
		this.isAllDay = isAllDay;
	}
    
}
