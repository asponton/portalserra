package br.com.serracorretora.plataformaserra.service.dto;

import java.util.List;
import java.util.stream.Collectors;

import br.com.serracorretora.plataformaserra.domain.Time;

public class TimeDTO {
	
	private Long id;
	
	private String nome;
	
	private String cor;
	
	private List<UserDTO> integrantes;
	
	public TimeDTO() {
		super();
	}
	
	public TimeDTO(Time time) {
		this(time.getId(), time.getNome(), time.getCor(), time.getIntegrantes().stream().map(integrante -> new UserDTO(integrante)).collect(Collectors.toList()));
	}
	

	public TimeDTO(Long id, String nome, String cor, List<UserDTO> integrantes) {
		this.id = id;
		this.nome = nome;
		this.cor = cor;
		this.integrantes = integrantes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<UserDTO> getIntegrantes() {
		return integrantes;
	}

	public void setIntegrantes(List<UserDTO> integrantes) {
		this.integrantes = integrantes;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}
	
}
