package br.com.serracorretora.plataformaserra.service;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.serracorretora.plataformaserra.domain.Notificacao;
import br.com.serracorretora.plataformaserra.domain.Notificacao.StatusPendencia;
import br.com.serracorretora.plataformaserra.domain.User;
import br.com.serracorretora.plataformaserra.repository.NotificacoesRepository;
import br.com.serracorretora.plataformaserra.repository.UserRepository;
import br.com.serracorretora.plataformaserra.service.dto.LeadDTO;
import br.com.serracorretora.plataformaserra.service.dto.NotificacoesDTO;

/**
 * Service class for managing notificacoes.
 */
@Service
@Transactional
public class NotificacaoService {

    private final Logger log = LoggerFactory.getLogger(NotificacaoService.class);
    
    @Inject
    private UserRepository userRepository;
    
    @Inject
    private NotificacoesRepository notificacaoRepository;
    
    @Inject
    private LeadService leadService;
    
    
    
    private static int ano = ZonedDateTime.now().getYear();
    
	public List<Notificacao> trazerNotificacao(Long userId) {
		log.info("Efetuando busca.");
		List<Notificacao> pendencias;
		Optional<User> user = userRepository.findOneById(userId);
		if (!user.get().getCargo().getName().equals("VENDEDOR") && !user.get().getCargo().getName().equals("SUPERVISOR")) {
			pendencias = notificacaoRepository.findAllByStatusPendenciaAndDataEntrada(StatusPendencia.SOLUCIONADO, ZonedDateTime.of(ano, ZonedDateTime.now().getMonthValue(), ZonedDateTime.now().getDayOfMonth(), 0, 0, 0, 0, ZoneId.of("America/Sao_Paulo")));
		}  else {
			pendencias = notificacaoRepository.findAllByResponsavelAndStatusPendenciaAndDataEntrada(user.get(), StatusPendencia.PENDENTE, ZonedDateTime.of(ano, ZonedDateTime.now().getMonthValue(), ZonedDateTime.now().getDayOfMonth(), 0, 0, 0, 0, ZoneId.of("America/Sao_Paulo")));
		}
		List<Notificacao> info = notificacaoRepository.findAllByResponsavelAndStatusPendenciaAndDataEntrada(user.get(), StatusPendencia.INFO, ZonedDateTime.of(ano, ZonedDateTime.now().getMonthValue(), ZonedDateTime.now().getDayOfMonth(), 0, 0, 0, 0, ZoneId.of("America/Sao_Paulo")));
		if (pendencias != null && !pendencias.isEmpty()) {
			pendencias.sort(new Comparator<Notificacao>() {
				@Override
				public int compare(Notificacao o1, Notificacao o2) {
					// TODO Auto-generated method stub
					return o1.getDataEntrada().compareTo(o2.getDataEntrada());
				}
			});
			return pendencias;
		}
		
		if (info != null  && !info.isEmpty()) {
			info.sort(new Comparator<Notificacao>() {
				@Override
				public int compare(Notificacao o1, Notificacao o2) {
					// TODO Auto-generated method stub
					return o1.getDataEntrada().compareTo(o2.getDataEntrada());
				}
			});
			return info;
		}

		return null;
	}
	
	public Notificacao inserirNotificacao(LeadDTO lead, StatusPendencia statusPendencia) {
		User usuario = userRepository.getOne(lead.getResponsavel().getIdUser());
		if (notificacaoRepository.findByResponsavelAndNumeroLeadAndStatusPendencia(usuario, lead.getId(), statusPendencia) == null) {
			Notificacao notificacao = new Notificacao();
			notificacao.setStatusPendencia(statusPendencia);
			notificacao.setCobranca(StatusPendencia.PENDENTE.equals(statusPendencia) ? lead.getResponsavel().getFirstName() +  ", porquê esta indicação atrasou?" : lead.getResponsavel().getFirstName() + ", você recebeu um novo Lead no valor de R$ " + lead.getValor().toString());
			notificacao.setJustificativa("");
			notificacao.setDataEntrada(ZonedDateTime.of(ano, ZonedDateTime.now().getMonthValue(), ZonedDateTime.now().getDayOfMonth(), 0, 0, 0, 0, ZoneId.of("America/Sao_Paulo")));
			notificacao.setResponsavel(usuario);
			notificacao.setNumeroLead(lead.getId());
			
			Notificacao notificacaoResponsavel = new Notificacao();
			notificacaoResponsavel.setStatusPendencia(statusPendencia);
			notificacaoResponsavel.setCobranca(StatusPendencia.PENDENTE.equals(statusPendencia) ?  "O vendedor " + usuario.getFirstName() + " atrasou Lead(s). Por favor verifique o que aconteceu." : "O vendedor " + usuario.getFirstName() + " recebeu um novo Lead. Faça com que ele realize o atendimento sem atrasar!");
			notificacaoResponsavel.setJustificativa("");
			notificacaoResponsavel.setDataEntrada(ZonedDateTime.of(ano, ZonedDateTime.now().getMonthValue(), ZonedDateTime.now().getDayOfMonth(), 0, 0, 0, 0, ZoneId.of("America/Sao_Paulo")));
			notificacaoResponsavel.setResponsavel(usuario.getResponsavel());
			notificacaoResponsavel.setNumeroLead(lead.getId());
			Notificacao notificacaoSalva = notificacaoRepository.saveAndFlush(notificacao);
			notificacaoRepository.saveAndFlush(notificacaoResponsavel);
			if (StatusPendencia.PENDENTE.equals(statusPendencia)) {
				leadService.updateLead(lead, null);
			}
			return notificacaoSalva;
		}
		return null;
	}
	
	public Notificacao atualizarPendencia(NotificacoesDTO pendencia) {
		return notificacaoRepository.findById(pendencia.getId()).map(notificacao -> {
			notificacao.setStatusPendencia(StatusPendencia.SOLUCIONADO);
			notificacao.setJustificativa(pendencia.getMensagemJustificativa());
			notificacaoRepository.save(notificacao);
	        log.debug("Updated Information for Notificacao: {}", notificacao);
	        return notificacao;
		}).orElseGet(null);		
	}

	public void inserirNotificacaoInfo(LeadDTO leadDTO) {
		
	}
	
//	public Notificacao inserirNotificacaoInfo(UserDTO user) {
//		Notificacao notificacao = new Notificacao();
//		User usuario = userRepository.getOne(user.getId());
//		Integer quantidadeLeadsNovos = 0;
//		List<Lead> leads = leadRepository.findAllByResponsavelAndStatus(usuario, Status.DISPONIVEL);
//		
//		for (Lead lead : leads) {
//			if (lead.getResponsavel().getAuthority().getName().equals("VENDEDOR") 
//					&& lead.getDataEncaminhamento().getDayOfYear() == ZonedDateTime.now(ZoneId.of("America/Sao_Paulo")).getDayOfYear()) {
//				quantidadeLeadsNovos = quantidadeLeadsNovos + 1;
//			}
//		}
//		
//		Notificacao notificacaoExitente = notificacaoRepository.findAllByResponsavelAndStatusPendenciaAndDataEntrada(usuario, StatusPendencia.INFO, ZonedDateTime.of(ano, ZonedDateTime.now().getMonthValue(), ZonedDateTime.now().getDayOfMonth(), 0, 0, 0, 0, ZoneId.of("America/Sao_Paulo")));
//		
//		if (notificacaoExitente == null) {
//			notificacao.setStatusPendencia(StatusPendencia.INFO);
//			notificacao.setCobranca("Você tem " + quantidadeLeadsNovos + "lead(s) novo(s)!" );
//			notificacao.setJustificativa("");
//			notificacao.setDataEntrada(ZonedDateTime.now(ZoneId.of("America/Sao_Paulo")));
//			notificacao.setResponsavel(usuario);
//		} else {
//			notificacao = notificacaoExitente;
//		}
//		
//		return notificacao;
//	}


}
