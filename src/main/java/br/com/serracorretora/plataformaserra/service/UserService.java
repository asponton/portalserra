package br.com.serracorretora.plataformaserra.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.serracorretora.plataformaserra.domain.Authority;
import br.com.serracorretora.plataformaserra.domain.Cargo;
import br.com.serracorretora.plataformaserra.domain.Contato;
import br.com.serracorretora.plataformaserra.domain.Time;
import br.com.serracorretora.plataformaserra.domain.User;
import br.com.serracorretora.plataformaserra.repository.AuthorityRepository;
import br.com.serracorretora.plataformaserra.repository.PersistentTokenRepository;
import br.com.serracorretora.plataformaserra.repository.UserRepository;
import br.com.serracorretora.plataformaserra.security.AuthoritiesConstants;
import br.com.serracorretora.plataformaserra.security.SecurityUtils;
import br.com.serracorretora.plataformaserra.service.util.RandomUtil;
import br.com.serracorretora.plataformaserra.web.rest.vm.ManagedUserVM;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    @Inject
    private PasswordEncoder passwordEncoder;

    @Inject
    private UserRepository userRepository;

    @Inject
    private PersistentTokenRepository persistentTokenRepository;

    @Inject
    private AuthorityRepository authorityRepository;

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                userRepository.save(user);
                log.debug("Activated user: {}", user);
                return user;
            });
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
       log.debug("Reset user password for reset key {}", key);

       return userRepository.findOneByResetKey(key)
            .filter(user -> {
                ZonedDateTime oneDayAgo = ZonedDateTime.now().minusHours(24);
                return user.getResetDate().isAfter(oneDayAgo);
           })
           .map(user -> {
                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                userRepository.save(user);
                return user;
           });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmail(mail)
            .filter(User::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(ZonedDateTime.now());
                userRepository.save(user);
                return user;
            });
    }

    public User createUser(String login, String password, String firstName, String lastName, String email, String langKey) {

        User newUser = new User();
        Authority authority = authorityRepository.findOne(AuthoritiesConstants.USER);
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(login);
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(email);
        newUser.setLangKey(langKey);
        // new user is not active
        newUser.setActivated(false);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    public User createUser(ManagedUserVM managedUserVM) {
    	Long idResponsavel = null;
    	if (!"DIRETOR".equals(managedUserVM.getCargo().getName().toUpperCase()) || !"GERENTE".equals(managedUserVM.getCargo().getName().toUpperCase())) {
    		idResponsavel = userRepository.findOneByLogin(managedUserVM.getResponsavel().getLogin()).get().getId();	
    	}
    	
        User user = new User();
        user.setLogin(managedUserVM.getLogin());
        user.setFirstName(managedUserVM.getFirstName());
        user.setLastName(managedUserVM.getLastName());
        user.setEmail(managedUserVM.getEmail());
        user.setTime(managedUserVM.getTime());
        user.setCpf(managedUserVM.getCpf());
        user.setMeta(managedUserVM.getMeta());
        user.setLimiteLeads(managedUserVM.getLimiteLeads());
        managedUserVM.getContatos().stream().forEach(c -> c.setUsuario(user));
        user.setContatos(managedUserVM.getContatos());
        user.setCargo(managedUserVM.getCargo());
        user.setQuantidadeLeadsIndividual(managedUserVM.getQuantidadeLeadsIndividual());
        user.setQuantidadeLeadsFamiliar(managedUserVM.getQuantidadeLeadsFamiliar());
        user.setQuantidadeLeadsPme(managedUserVM.getQuantidadeLeadsPme());
        user.setQuantidadeLeadsEmpresarial(managedUserVM.getQuantidadeLeadsEmpresarial());
        user.setRangeDddMinimo(managedUserVM.getRangeDddMinimo());
        user.setRangeDddMaximo(managedUserVM.getRangeDddMaximo());        
        if(managedUserVM.getResponsavel() != null){
            User responsavel = new User();
            responsavel.setId(idResponsavel);
            user.setResponsavel(responsavel);
        }
        if (managedUserVM.getLangKey() == null) {
            user.setLangKey("en"); // default language
        } else {
            user.setLangKey(managedUserVM.getLangKey());
        }
        if (managedUserVM.getAuthorities() != null) {
            Set<Authority> authorities = new HashSet<>();
            managedUserVM.getAuthorities().stream().forEach(
                authority -> authorities.add(authorityRepository.findOne(authority))
            );
            user.setAuthorities(authorities);
        }
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(ZonedDateTime.now());
        user.setActivated(true);
        userRepository.save(user);
        log.debug("Created Information for User: {}", user);
        return user;
    }

    public void updateUser(String firstName, String lastName, String email, Time time, String langKey) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(u -> {
            u.setFirstName(firstName);
            u.setLastName(lastName);
            u.setEmail(email);
            u.setTime(time);
            u.setLangKey(langKey);
            userRepository.save(u);
            log.debug("Changed Information for User: {}", u);
        });
    }

    public void updateUser(Long id, String login, String firstName, String lastName, String email,
        boolean activated, String langKey, Set<String> authorities, Cargo cargo, Time time, String cpf, BigDecimal meta, Long limiteLeads, List<Contato> contatos,
		Long quantidadeLeadsIndividual, Long quantidadeLeadsFamiliar, Long quantidadeLeadsPme, Long quantidadeLeadsEmpresarial, Long rangeDddMinimo, Long rangeDddMaximo, User userDTO, String fotoUrl, String fotoPainel) {
        userRepository
            .findOneById(id)
            .ifPresent(u -> {
                u.setLogin(login);
                u.setFirstName(firstName);
                u.setLastName(lastName);
                u.setEmail(email);
                u.setActivated(activated);
                u.setLangKey(langKey);
                Set<Authority> managedAuthorities = u.getAuthorities();
                managedAuthorities.clear();
                authorities.stream().forEach(
                    authority -> managedAuthorities.add(authorityRepository.findOne(authority))
                );
                u.setCargo(cargo);
                u.setTime(time);
                u.setCpf(cpf);
                u.setMeta(meta);
                u.setLimiteLeads(limiteLeads);
                if (contatos != null) {
                	contatos.stream().forEach(c -> { 
    	                c.setUsuario(u);
    	                c.setCreatedBy(SecurityUtils.getCurrentUserLogin());
    	            });
                }
                u.setContatos(contatos);
                u.setQuantidadeLeadsIndividual(quantidadeLeadsIndividual);
                u.setQuantidadeLeadsFamiliar(quantidadeLeadsFamiliar);
                u.setQuantidadeLeadsPme(quantidadeLeadsPme);
                u.setQuantidadeLeadsEmpresarial(quantidadeLeadsEmpresarial);
                u.setRangeDddMinimo(rangeDddMinimo);
                u.setRangeDddMaximo(rangeDddMaximo);
                if(userDTO != null){
                	Optional<User> chefe = userRepository.findOneByLogin(userDTO.getLogin());
                    u.setResponsavel(chefe.get());
                }
                u.setFotoUrl(fotoUrl);
                u.setFotoPainel(fotoPainel);
                userRepository.saveAndFlush(u);
                log.debug("Changed Information for User: {}", u);
            });
    }

    public void deleteUser(String login) {
        userRepository.findOneByLogin(login).ifPresent(u -> {
            userRepository.delete(u);
            log.debug("Deleted User: {}", u);
        });
    }

    public void changePassword(String password) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(u -> {
            String encryptedPassword = passwordEncoder.encode(password);
            u.setPassword(encryptedPassword);
            userRepository.save(u);
            log.debug("Changed password for User: {}", u);
        });
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneByLogin(login).map(u -> {
            u.getAuthorities().size();
            u.getContatos().size();
            if (u.getResponsavel() != null) {
                User chefe = u.getResponsavel();
                chefe.getAuthorities();
            }
            return u;
        });
    }

    @Transactional(readOnly = true)
    public User getUserWithAuthorities(Long id) {
        User user = userRepository.findOne(id);
        user.getAuthorities().size(); // eagerly load the association
        if (user.getContatos() != null) {
        	user.getContatos().size();	
        }
        if (user.getResponsavel() != null) {
            User chefe = user.getResponsavel();
            chefe.getAuthorities();
        }
        return user;
    }

    @Transactional(readOnly = true)
    public User getUserWithAuthorities() {
        User user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).get();
        user.getAuthorities().size(); // eagerly load the association
        user.getContatos().size();
        if (user.getResponsavel() != null) {
            User chefe = user.getResponsavel();
            chefe.getAuthorities();
        }
        return user;
    }

    /**
     * Persistent Token are used for providing automatic authentication, they should be automatically deleted after
     * 30 days.
     * <p>
     * This is scheduled to get fired everyday, at midnight.
     * </p>
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void removeOldPersistentTokens() {
        LocalDate now = LocalDate.now();
        persistentTokenRepository.findByTokenDateBefore(now.minusMonths(1)).stream().forEach(token -> {
            log.debug("Deleting token {}", token.getSeries());
            User user = token.getUser();
            user.getPersistentTokens().remove(token);
            persistentTokenRepository.delete(token);
        });
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     * </p>
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void reActivateNotActivatedUsers() {
        ZonedDateTime now = ZonedDateTime.now();
        List<User> users = userRepository.findAllByActivatedIsFalseAndDataBloqueioBefore(now.minusDays(1));
        for (User user : users) {
            log.debug("Reativando usu�rio inativo {}", user.getLogin());
            user.setActivated(true);
            user.setDataBloqueio(null);
            userRepository.saveAndFlush(user);
        }
    }

	public void inativarUser(User responsavel) {
		User vendedor = userRepository.findOne(responsavel.getId());
		vendedor.setActivated(false);
		vendedor.setDataBloqueio(ZonedDateTime.now());
		userRepository.saveAndFlush(vendedor);
	}
	
	public void inativarUserPorTempoIndeterminado(User responsavel) {
		User vendedor = userRepository.findOne(responsavel.getId());
		vendedor.setActivated(false);
		vendedor.setDataBloqueio(ZonedDateTime.now().plusYears(1));
		userRepository.saveAndFlush(vendedor);	
	}
	
	
}
