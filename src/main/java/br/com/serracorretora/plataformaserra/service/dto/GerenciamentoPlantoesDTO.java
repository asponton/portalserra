package br.com.serracorretora.plataformaserra.service.dto;

import java.time.ZonedDateTime;
import java.util.List;

import br.com.serracorretora.plataformaserra.domain.GerenciamentoPlantoes;

public class GerenciamentoPlantoesDTO {
	
	private Long id;
	
	private List<Long> vendedores;
	
	private List<Long> times;
	
	private ZonedDateTime periodoInicial;
	
	private ZonedDateTime  periodoFinal;
	
	private boolean periodoFechado;
	
	public GerenciamentoPlantoesDTO() {
		super();
	}
	
	public GerenciamentoPlantoesDTO(GerenciamentoPlantoes plantaoMensal) {
		this(plantaoMensal.getId(), null, null, plantaoMensal.getPeriodoInicial(), plantaoMensal.getPeriodoFinal(), plantaoMensal.getPeriodoFechado());
	}
	
	public GerenciamentoPlantoesDTO(Long id, List<Long> vendedores, List<Long> times, ZonedDateTime periodoInicial, ZonedDateTime periodoFinal, boolean periodoFechado) {
		super();
		this.id = id;
		this.vendedores = vendedores;
		this.times = times;
		this.periodoInicial = periodoInicial;
		this.periodoFinal = periodoFinal;
		this.periodoFechado = periodoFechado;
	}



	public ZonedDateTime getPeriodoInicial() {
		return periodoInicial;
	}

	public void setPeriodoInicial(ZonedDateTime periodoInicial) {
		this.periodoInicial = periodoInicial;
	}

	public ZonedDateTime getPeriodoFinal() {
		return periodoFinal;
	}

	public void setPeriodoFinal(ZonedDateTime periodoFinal) {
		this.periodoFinal = periodoFinal;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isPeriodoFechado() {
		return periodoFechado;
	}

	public void setPeriodoFechado(boolean periodoFechado) {
		this.periodoFechado = periodoFechado;
	}

	public List<Long> getVendedores() {
		return vendedores;
	}

	public void setVendedores(List<Long> vendedores) {
		this.vendedores = vendedores;
	}

	public List<Long> getTimes() {
		return times;
	}

	public void setTimes(List<Long> times) {
		this.times = times;
	}

}
