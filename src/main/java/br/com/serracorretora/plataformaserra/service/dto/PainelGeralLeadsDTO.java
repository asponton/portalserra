package br.com.serracorretora.plataformaserra.service.dto;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import br.com.serracorretora.plataformaserra.domain.Lead;
import br.com.serracorretora.plataformaserra.domain.Lead.TipoPlano;


public class PainelGeralLeadsDTO {
	
	private ResultadoPorPeriodo vendidosPorPeriodo;
	
	private ResultadoPorPeriodo perdidosPorPeriodo;
	
	private ResultadoPorPeriodo emNegociacaoPorPeriodo;
	
	private List<ResultadoPorPeriodo> vendasTimePorPeriodo = new ArrayList<ResultadoPorPeriodo>();
	
	private List<ResultadoPorPeriodo> perdasTimePorPeriodo = new ArrayList<ResultadoPorPeriodo>();
	
	private List<ResultadoPorPeriodo> emNegociacaoTimePorPeriodo = new ArrayList<ResultadoPorPeriodo>();
	
	private ResultadoPorTipoPlano resultadoPlanoIndividual;
	
	private ResultadoPorTipoPlano resultadoPlanoFamiliar;
	
	private ResultadoPorTipoPlano resultadoPlanoPme;
	
	private ResultadoPorTipoPlano resultadoPlanoEmpresarial;
	
	List<DadosGraficoMultilevel> dadosGraficoMultiLevelTime1;
	
	List<DadosGraficoMultilevel> dadosGraficoMultiLevelTime2;
	
	List<DadosGraficoMultilevel> dadosGraficoMultiLevelTime3;
	
	List<DadosGraficoMultilevel> dadosGraficoMultiLevelTime4;
	
	List<DadosGraficoMultilevel> dadosGraficoMultiLevelTime5;
	
	List<DadosGraficoGauge> dadosGraficoGaugeTime1;
	
	List<DadosGraficoGauge> dadosGraficoGaugeTime2;
	
	List<DadosGraficoGauge> dadosGraficoGaugeTime3;
	
	List<DadosGraficoGauge> dadosGraficoGaugeTime4;
	
	List<DadosGraficoGauge> dadosGraficoGaugeTime5;
	
	public PainelGeralLeadsDTO(List<LeadDTO> vendidos, List<LeadDTO> emNegociacao, List<LeadDTO> perdidos, List<TimeDTO> times) {
			this.filtrar(vendidos, Lead.Status.VENDIDO, times);
			this.filtrar(emNegociacao, Lead.Status.EM_ANDAMENTO, times);
			this.filtrar(perdidos, Lead.Status.SEM_INTERESSE, times);
			
			this.dadosGraficoMultiLevelTime1 = this.gerarDadosGraficoMultiLevel(this.vendasTimePorPeriodo.get(0));
			this.dadosGraficoMultiLevelTime2 = this.gerarDadosGraficoMultiLevel(this.vendasTimePorPeriodo.get(1));
			this.dadosGraficoMultiLevelTime3 = this.gerarDadosGraficoMultiLevel(this.vendasTimePorPeriodo.get(2));
			this.dadosGraficoMultiLevelTime4 = this.gerarDadosGraficoMultiLevel(this.vendasTimePorPeriodo.get(3));
			this.dadosGraficoMultiLevelTime5 = this.gerarDadosGraficoMultiLevel(this.vendasTimePorPeriodo.get(4));
			
			this.dadosGraficoGaugeTime1 = this.gerarDadosGraficoGauge(this.vendasTimePorPeriodo.get(0));
			this.dadosGraficoGaugeTime2 = this.gerarDadosGraficoGauge(this.vendasTimePorPeriodo.get(1));
			this.dadosGraficoGaugeTime3 = this.gerarDadosGraficoGauge(this.vendasTimePorPeriodo.get(2));
			this.dadosGraficoGaugeTime4 = this.gerarDadosGraficoGauge(this.vendasTimePorPeriodo.get(3));
			this.dadosGraficoGaugeTime5 = this.gerarDadosGraficoGauge(this.vendasTimePorPeriodo.get(4));
	}

	private void filtrar(List<LeadDTO> leads, Lead.Status status, List<TimeDTO> times) {
		List<LeadDTO> leadsJaneiro = new ArrayList<LeadDTO>();
		List<LeadDTO> leadsFevereiro = new ArrayList<LeadDTO>();
		List<LeadDTO> leadsMarco = new ArrayList<LeadDTO>();
		List<LeadDTO> leadsAbril = new ArrayList<LeadDTO>();
		List<LeadDTO> leadsMaio = new ArrayList<LeadDTO>();
		List<LeadDTO> leadsJunho = new ArrayList<LeadDTO>();
		List<LeadDTO> leadsJulho = new ArrayList<LeadDTO>();
		List<LeadDTO> leadsAgosto = new ArrayList<LeadDTO>();
		List<LeadDTO> leadsSetembro = new ArrayList<LeadDTO>();
		List<LeadDTO> leadsOutubro = new ArrayList<LeadDTO>();
		List<LeadDTO> leadsNovembro = new ArrayList<LeadDTO>();
		List<LeadDTO> leadsDezembro = new ArrayList<LeadDTO>();
		
		List<String> mesesVendidos = new ArrayList<String>();
		List<String> mesesPerdidos = new ArrayList<String>();
		List<String> mesesNegociacao = new ArrayList<String>();
		
		List<Integer> quantidadeVendidos = new ArrayList<Integer>();
		List<Integer> quantidadePerdidos = new ArrayList<Integer>();
		List<Integer> quantidadeNegociacao = new ArrayList<Integer>();
		
		List<UserDTO> competidoresJaneiro = new ArrayList<UserDTO>();
		List<UserDTO> competidoresFevereiro = new ArrayList<UserDTO>();
		List<UserDTO> competidoresMarco = new ArrayList<UserDTO>();
		List<UserDTO> competidoresAbril = new ArrayList<UserDTO>();
		List<UserDTO> competidoresMaio = new ArrayList<UserDTO>();
		List<UserDTO> competidoresJunho = new ArrayList<UserDTO>();
		List<UserDTO> competidoresJulho = new ArrayList<UserDTO>();
		List<UserDTO> competidoresAgosto = new ArrayList<UserDTO>();
		List<UserDTO> competidoresSetembro = new ArrayList<UserDTO>();
		List<UserDTO> competidoresOutubro = new ArrayList<UserDTO>();
		List<UserDTO> competidoresNovembro = new ArrayList<UserDTO>();
		List<UserDTO> competidoresDezembro = new ArrayList<UserDTO>();
		List<UserDTO> campeoes = new ArrayList<UserDTO>();
		
		
		for (LeadDTO lead : leads) {
			switch (lead.getDataEntrada().getMonth()) {
			case JANUARY:
				leadsJaneiro.add(lead);
				break;
			case FEBRUARY:
				leadsFevereiro.add(lead);
				break;
			case MARCH:
				leadsMarco.add(lead);
				break;
			case APRIL:
				leadsAbril.add(lead);
				break;
			case MAY:
				leadsMaio.add(lead);
				break;
			case JUNE:
				leadsJunho.add(lead);
				break;
			case JULY:
				leadsJulho.add(lead);
				break;
			case AUGUST:
				leadsAgosto.add(lead);
				break;
			case SEPTEMBER:
				leadsSetembro.add(lead);
				break;
			case OCTOBER:
				leadsOutubro.add(lead);
				break;
			case NOVEMBER:
				leadsNovembro.add(lead);
				break;
			case DECEMBER:
				leadsDezembro.add(lead);
				break;
			}
			
		}
		
		int ano = ZonedDateTime.now().getYear();
		ZonedDateTime dataAtual = ZonedDateTime.of(ano, ZonedDateTime.now().getMonthValue(), ZonedDateTime.now().getDayOfMonth(), 23, 59, 59, 0, ZoneId.of("America/Sao_Paulo"));
		ZonedDateTime julho = ZonedDateTime.of(ano, 7, 1, 0, 0, 0, 0, ZoneId.of("America/Sao_Paulo"));
		
		
		if (julho.isAfter(dataAtual)) {
			switch (status) {
			case VENDIDO:
				mesesVendidos.add("JANEIRO");
				mesesVendidos.add("FEVEREIRO");
				mesesVendidos.add("MARÇO");
				mesesVendidos.add("ABRIL");
				mesesVendidos.add("MAIO");
				mesesVendidos.add("JUNHO");
				quantidadeVendidos.add(leadsJaneiro.size());
				quantidadeVendidos.add(leadsFevereiro.size());
				quantidadeVendidos.add(leadsMarco.size());
				quantidadeVendidos.add(leadsAbril.size());
				quantidadeVendidos.add(leadsMaio.size());
				quantidadeVendidos.add(leadsJunho.size());
				
				switch (dataAtual.getMonth()) {
				case JANUARY:
					resultadoPlanoEmpresarial = new ResultadoPorTipoPlano("Janeiro", leadsJaneiro.stream().filter(lead -> TipoPlano.EMPRESARIAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size()); 
					resultadoPlanoFamiliar = new ResultadoPorTipoPlano("Janeiro", leadsJaneiro.stream().filter(lead -> TipoPlano.FAMILIAR.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoIndividual = new ResultadoPorTipoPlano("Janeiro", leadsJaneiro.stream().filter(lead -> TipoPlano.INDIVIDUAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoPme = new ResultadoPorTipoPlano("Janeiro", leadsJaneiro.stream().filter(lead -> TipoPlano.PME.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					break;
				case FEBRUARY:
					resultadoPlanoEmpresarial = new ResultadoPorTipoPlano("Fevereiro", leadsFevereiro.stream().filter(lead -> TipoPlano.EMPRESARIAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size()); 
					resultadoPlanoFamiliar = new ResultadoPorTipoPlano("Fevereiro", leadsFevereiro.stream().filter(lead -> TipoPlano.FAMILIAR.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoIndividual = new ResultadoPorTipoPlano("Fevereiro", leadsFevereiro.stream().filter(lead -> TipoPlano.INDIVIDUAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoPme = new ResultadoPorTipoPlano("Fevereiro", leadsFevereiro.stream().filter(lead -> TipoPlano.PME.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					break;
				case MARCH:
					resultadoPlanoEmpresarial = new ResultadoPorTipoPlano("Março", leadsMarco.stream().filter(lead -> TipoPlano.EMPRESARIAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size()); 
					resultadoPlanoFamiliar = new ResultadoPorTipoPlano("Março", leadsMarco.stream().filter(lead -> TipoPlano.FAMILIAR.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoIndividual = new ResultadoPorTipoPlano("Março", leadsMarco.stream().filter(lead -> TipoPlano.INDIVIDUAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoPme = new ResultadoPorTipoPlano("Março", leadsMarco.stream().filter(lead -> TipoPlano.PME.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());					
					break;
				case APRIL:
					resultadoPlanoEmpresarial = new ResultadoPorTipoPlano("Abril", leadsAbril.stream().filter(lead -> TipoPlano.EMPRESARIAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size()); 
					resultadoPlanoFamiliar = new ResultadoPorTipoPlano("Abril", leadsAbril.stream().filter(lead -> TipoPlano.FAMILIAR.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoIndividual = new ResultadoPorTipoPlano("Abril", leadsAbril.stream().filter(lead -> TipoPlano.INDIVIDUAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoPme = new ResultadoPorTipoPlano("Abril", leadsAbril.stream().filter(lead -> TipoPlano.PME.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					break;
				case MAY:
					resultadoPlanoEmpresarial = new ResultadoPorTipoPlano("Maio", leadsMaio.stream().filter(lead -> TipoPlano.EMPRESARIAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size()); 
					resultadoPlanoFamiliar = new ResultadoPorTipoPlano("Maio", leadsMaio.stream().filter(lead -> TipoPlano.FAMILIAR.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoIndividual = new ResultadoPorTipoPlano("Maio", leadsMaio.stream().filter(lead -> TipoPlano.INDIVIDUAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoPme = new ResultadoPorTipoPlano("Maio", leadsMaio.stream().filter(lead -> TipoPlano.PME.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					break;
				case JUNE:
					resultadoPlanoEmpresarial = new ResultadoPorTipoPlano("Junho", leadsJunho.stream().filter(lead -> TipoPlano.EMPRESARIAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size()); 
					resultadoPlanoFamiliar = new ResultadoPorTipoPlano("Junho", leadsJunho.stream().filter(lead -> TipoPlano.FAMILIAR.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoIndividual = new ResultadoPorTipoPlano("Junho", leadsJunho.stream().filter(lead -> TipoPlano.INDIVIDUAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoPme = new ResultadoPorTipoPlano("Junho", leadsJunho.stream().filter(lead -> TipoPlano.PME.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					break;
				default:
					break;
				}								
				
				for (TimeDTO time : times) {
					Map<Long, UserDTO> performanceCorretoresJaneiro = new HashMap<Long, UserDTO>();
					Map<Long, UserDTO> performanceCorretoresFevereiro = new HashMap<Long, UserDTO>();
					Map<Long, UserDTO> performanceCorretoresMarco = new HashMap<Long, UserDTO>();
					Map<Long, UserDTO> performanceCorretoresAbril = new HashMap<Long, UserDTO>();
					Map<Long, UserDTO> performanceCorretoresMaio = new HashMap<Long, UserDTO>();
					Map<Long, UserDTO> performanceCorretoresJunho = new HashMap<Long, UserDTO>();
					
					int quantidadeVendidosTimeJaneiro = 0;
					int quantidadeVendidosTimeFevereiro = 0;
					int quantidadeVendidosTimeMarco = 0;
					int quantidadeVendidosTimeAbril = 0;
					int quantidadeVendidosTimeMaio = 0;
					int quantidadeVendidosTimeJunho = 0;
					
					for (LeadDTO lead : leadsJaneiro) {
						for (UserDTO vendedor : time.getIntegrantes().stream().filter(integrante -> integrante.getCargo().getName().equals("VENDEDOR")).collect(Collectors.toList())) {
							if (vendedor.getIdUser().equals(lead.getResponsavel().getIdUser())) {
							quantidadeVendidosTimeJaneiro += 1;
								if (performanceCorretoresJaneiro.containsKey(vendedor.getIdUser())) {
									performanceCorretoresJaneiro.get(vendedor.getIdUser()).getRanking().setQuantidadeVendas(performanceCorretoresJaneiro.get(vendedor.getIdUser()).getRanking().getQuantidadeVendas() + 1L);
									performanceCorretoresJaneiro.get(vendedor.getIdUser()).getRanking().setValorVendas(performanceCorretoresJaneiro.get(vendedor.getIdUser()).getRanking().getValorVendas().add(lead.getValor()));
								} else {
									if (vendedor.getRanking().getQuantidadeVendas() == null) {
										RankingDTO rankingVendedor = new RankingDTO();
										rankingVendedor.setQuantidadeVendas(1L);
										rankingVendedor.setValorVendas(lead.getValor());
										vendedor.setRanking(rankingVendedor);
									} else {
										vendedor.getRanking().setQuantidadeVendas(vendedor.getRanking().getQuantidadeVendas() + 1L);
										vendedor.getRanking().setValorVendas(vendedor.getRanking().getValorVendas().add(lead.getValor()));
									}
									performanceCorretoresJaneiro.put(vendedor.getIdUser(), vendedor);
								}
								if(competidoresJaneiro.contains(vendedor)) {
									competidoresJaneiro.remove(vendedor);
								}
								vendedor.setPercentualAtingido(vendedor.getPercentualAtingido().add(vendedor.buscaPorcentagemConcluidaMetaCorretor()));
								competidoresJaneiro.add(vendedor);
							}
						}
					}
					
					for (LeadDTO lead : leadsFevereiro) {
						for (UserDTO vendedor : time.getIntegrantes().stream().filter(integrante -> integrante.getCargo().getName().equals("VENDEDOR")).collect(Collectors.toList())) {
							if (vendedor.getIdUser().equals(lead.getResponsavel().getIdUser())) {
							quantidadeVendidosTimeFevereiro += 1;
								if (performanceCorretoresFevereiro.containsKey(vendedor.getIdUser())) {
									performanceCorretoresFevereiro.get(vendedor.getIdUser()).getRanking().setQuantidadeVendas(performanceCorretoresFevereiro.get(vendedor.getIdUser()).getRanking().getQuantidadeVendas() + 1L);
									performanceCorretoresFevereiro.get(vendedor.getIdUser()).getRanking().setValorVendas(performanceCorretoresFevereiro.get(vendedor.getIdUser()).getRanking().getValorVendas().add(lead.getValor()));
								} else {
									if (vendedor.getRanking().getQuantidadeVendas() == null) {
										RankingDTO rankingVendedor = new RankingDTO();
										rankingVendedor.setQuantidadeVendas(1L);
										rankingVendedor.setValorVendas(lead.getValor());
										vendedor.setRanking(rankingVendedor);
									} else {
										vendedor.getRanking().setQuantidadeVendas(vendedor.getRanking().getQuantidadeVendas() + 1L);
										vendedor.getRanking().setValorVendas(vendedor.getRanking().getValorVendas().add(lead.getValor()));
									}
									performanceCorretoresFevereiro.put(vendedor.getIdUser(), vendedor);
								}
								if(competidoresFevereiro.contains(vendedor)) {
									competidoresFevereiro.remove(vendedor);
								}
								vendedor.setPercentualAtingido(vendedor.getPercentualAtingido().add(vendedor.buscaPorcentagemConcluidaMetaCorretor()));
								competidoresFevereiro.add(vendedor);
							}
						}
					}
					
					for (LeadDTO lead : leadsMarco) {
						for (UserDTO vendedor : time.getIntegrantes().stream().filter(integrante -> integrante.getCargo().getName().equals("VENDEDOR")).collect(Collectors.toList())) {
							if (vendedor.getIdUser().equals(lead.getResponsavel().getIdUser())) {
							quantidadeVendidosTimeMarco += 1;
								if (performanceCorretoresMarco.containsKey(vendedor.getIdUser())) {
									performanceCorretoresMarco.get(vendedor.getIdUser()).getRanking().setQuantidadeVendas(performanceCorretoresMarco.get(vendedor.getIdUser()).getRanking().getQuantidadeVendas() + 1L);
									performanceCorretoresMarco.get(vendedor.getIdUser()).getRanking().setValorVendas(performanceCorretoresMarco.get(vendedor.getIdUser()).getRanking().getValorVendas().add(lead.getValor()));
								} else {
									if (vendedor.getRanking().getQuantidadeVendas() == null) {
										RankingDTO rankingVendedor = new RankingDTO();
										rankingVendedor.setQuantidadeVendas(1L);
										rankingVendedor.setValorVendas(lead.getValor());
										vendedor.setRanking(rankingVendedor);
									} else {
										vendedor.getRanking().setQuantidadeVendas(vendedor.getRanking().getQuantidadeVendas() + 1L);
										vendedor.getRanking().setValorVendas(vendedor.getRanking().getValorVendas().add(lead.getValor()));
									}
									performanceCorretoresMarco.put(vendedor.getIdUser(), vendedor);
								}
								if(competidoresMarco.contains(vendedor)) {
									competidoresMarco.remove(vendedor);
								}
								vendedor.setPercentualAtingido(vendedor.getPercentualAtingido().add(vendedor.buscaPorcentagemConcluidaMetaCorretor()));
								competidoresMarco.add(vendedor);
							}
						}
					}
					
					for (LeadDTO lead : leadsAbril) {
						for (UserDTO vendedor : time.getIntegrantes().stream().filter(integrante -> integrante.getCargo().getName().equals("VENDEDOR")).collect(Collectors.toList())) {
							if (vendedor.getIdUser().equals(lead.getResponsavel().getIdUser())) {
							quantidadeVendidosTimeAbril += 1;
								if (performanceCorretoresAbril.containsKey(vendedor.getIdUser())) {
									performanceCorretoresAbril.get(vendedor.getIdUser()).getRanking().setQuantidadeVendas(performanceCorretoresAbril.get(vendedor.getIdUser()).getRanking().getQuantidadeVendas() + 1L);
									performanceCorretoresAbril.get(vendedor.getIdUser()).getRanking().setValorVendas(performanceCorretoresAbril.get(vendedor.getIdUser()).getRanking().getValorVendas().add(lead.getValor()));
								} else {
									if (vendedor.getRanking().getQuantidadeVendas() == null) {
										RankingDTO rankingVendedor = new RankingDTO();
										rankingVendedor.setQuantidadeVendas(1L);
										rankingVendedor.setValorVendas(lead.getValor());
										vendedor.setRanking(rankingVendedor);
									} else {
										vendedor.getRanking().setQuantidadeVendas(vendedor.getRanking().getQuantidadeVendas() + 1L);
										vendedor.getRanking().setValorVendas(vendedor.getRanking().getValorVendas().add(lead.getValor()));
									}
									performanceCorretoresAbril.put(vendedor.getIdUser(), vendedor);
								}
								if(competidoresAbril.contains(vendedor)) {
									competidoresAbril.remove(vendedor);
								}
								vendedor.setPercentualAtingido(vendedor.getPercentualAtingido().add(vendedor.buscaPorcentagemConcluidaMetaCorretor()));
								competidoresAbril.add(vendedor);
							}
						}
					}
					
					for (LeadDTO lead : leadsMaio) {
						for (UserDTO vendedor : time.getIntegrantes().stream().filter(integrante -> integrante.getCargo().getName().equals("VENDEDOR")).collect(Collectors.toList())) {
							if (vendedor.getIdUser().equals(lead.getResponsavel().getIdUser())) {
							quantidadeVendidosTimeMaio += 1;
								if (performanceCorretoresMaio.containsKey(vendedor.getIdUser())) {
									performanceCorretoresMaio.get(vendedor.getIdUser()).getRanking().setQuantidadeVendas(performanceCorretoresMaio.get(vendedor.getIdUser()).getRanking().getQuantidadeVendas() + 1L);
									performanceCorretoresMaio.get(vendedor.getIdUser()).getRanking().setValorVendas(performanceCorretoresMaio.get(vendedor.getIdUser()).getRanking().getValorVendas().add(lead.getValor()));
								} else {
									if (vendedor.getRanking().getQuantidadeVendas() == null) {
										RankingDTO rankingVendedor = new RankingDTO();
										rankingVendedor.setQuantidadeVendas(1L);
										rankingVendedor.setValorVendas(lead.getValor());
										vendedor.setRanking(rankingVendedor);
									} else {
										vendedor.getRanking().setQuantidadeVendas(vendedor.getRanking().getQuantidadeVendas() + 1L);
										vendedor.getRanking().setValorVendas(vendedor.getRanking().getValorVendas().add(lead.getValor()));
									}
									performanceCorretoresMaio.put(vendedor.getIdUser(), vendedor);
								}
								if(competidoresMaio.contains(vendedor)) {
									competidoresMaio.remove(vendedor);
								}
								vendedor.setPercentualAtingido(vendedor.getPercentualAtingido().add(vendedor.buscaPorcentagemConcluidaMetaCorretor()));
								competidoresMaio.add(vendedor);
							}
						}
					}
					
					for (LeadDTO lead : leadsJunho) {
						for (UserDTO vendedor : time.getIntegrantes().stream().filter(integrante -> integrante.getCargo().getName().equals("VENDEDOR")).collect(Collectors.toList())) {
							if (vendedor.getIdUser().equals(lead.getResponsavel().getIdUser())) {
							quantidadeVendidosTimeJunho += 1;
								if (performanceCorretoresJunho.containsKey(vendedor.getIdUser())) {
									performanceCorretoresJunho.get(vendedor.getIdUser()).getRanking().setQuantidadeVendas(performanceCorretoresJunho.get(vendedor.getIdUser()).getRanking().getQuantidadeVendas() + 1L);
									performanceCorretoresJunho.get(vendedor.getIdUser()).getRanking().setValorVendas(performanceCorretoresJunho.get(vendedor.getIdUser()).getRanking().getValorVendas().add(lead.getValor()));
								} else {
									if (vendedor.getRanking().getQuantidadeVendas() == null) {
										RankingDTO rankingVendedor = new RankingDTO();
										rankingVendedor.setQuantidadeVendas(1L);
										rankingVendedor.setValorVendas(lead.getValor());
										vendedor.setRanking(rankingVendedor);
									} else {
										vendedor.getRanking().setQuantidadeVendas(vendedor.getRanking().getQuantidadeVendas() + 1L);
										vendedor.getRanking().setValorVendas(vendedor.getRanking().getValorVendas().add(lead.getValor()));
									}
									performanceCorretoresJunho.put(vendedor.getIdUser(), vendedor);
								}
								if(competidoresJunho.contains(vendedor)) {
									competidoresJunho.remove(vendedor);
								}
								vendedor.setPercentualAtingido(vendedor.getPercentualAtingido().add(vendedor.buscaPorcentagemConcluidaMetaCorretor()));
								competidoresJunho.add(vendedor);
							}
						}
					}
					int valorTimeMensal = 0;
					ResultadoPorPeriodo resultadoTimePorPeriodo = new ResultadoPorPeriodo();
					resultadoTimePorPeriodo.setNomeTime(time.getNome());
					resultadoTimePorPeriodo.setValorTimeSemestral(quantidadeVendidosTimeJaneiro+quantidadeVendidosTimeFevereiro+quantidadeVendidosTimeMarco+quantidadeVendidosTimeAbril+quantidadeVendidosTimeMaio+quantidadeVendidosTimeJunho);
					campeoes.add(this.obterCampeaoMes(competidoresJaneiro));
					campeoes.add(this.obterCampeaoMes(competidoresFevereiro));
					campeoes.add(this.obterCampeaoMes(competidoresMarco));
					campeoes.add(this.obterCampeaoMes(competidoresAbril));
					campeoes.add(this.obterCampeaoMes(competidoresMaio));
					campeoes.add(this.obterCampeaoMes(competidoresJunho));
					switch (dataAtual.getMonth()) {
					case JANUARY:
						valorTimeMensal = quantidadeVendidosTimeJaneiro;
						resultadoTimePorPeriodo.getIntegrantes().addAll(performanceCorretoresJaneiro.values());
						resultadoTimePorPeriodo.setPercentualAtingido(resultadoTimePorPeriodo.buscaPercentualMetaConcluidaTime());
						break;
					case FEBRUARY:
						valorTimeMensal = quantidadeVendidosTimeFevereiro;
						resultadoTimePorPeriodo.getIntegrantes().addAll(performanceCorretoresFevereiro.values());
						resultadoTimePorPeriodo.setPercentualAtingido(resultadoTimePorPeriodo.buscaPercentualMetaConcluidaTime());
						break;
					case MARCH:
						valorTimeMensal = quantidadeVendidosTimeMarco;
						resultadoTimePorPeriodo.getIntegrantes().addAll(performanceCorretoresMarco.values());
						resultadoTimePorPeriodo.setPercentualAtingido(resultadoTimePorPeriodo.buscaPercentualMetaConcluidaTime());
						break;
					case APRIL:
						valorTimeMensal = quantidadeVendidosTimeAbril;
						resultadoTimePorPeriodo.getIntegrantes().addAll(performanceCorretoresAbril.values());
						resultadoTimePorPeriodo.setPercentualAtingido(resultadoTimePorPeriodo.buscaPercentualMetaConcluidaTime());
						break;
					case MAY:
						valorTimeMensal = quantidadeVendidosTimeMaio;
						resultadoTimePorPeriodo.getIntegrantes().addAll(performanceCorretoresMaio.values());
						resultadoTimePorPeriodo.setPercentualAtingido(resultadoTimePorPeriodo.buscaPercentualMetaConcluidaTime());
						break;
					case JUNE:
						valorTimeMensal = quantidadeVendidosTimeJunho;
						resultadoTimePorPeriodo.getIntegrantes().addAll(performanceCorretoresJunho.values());
						resultadoTimePorPeriodo.setPercentualAtingido(resultadoTimePorPeriodo.buscaPercentualMetaConcluidaTime());
						break;
					default:
						break;
					}
					resultadoTimePorPeriodo.setValorTimeMensal(valorTimeMensal);					
					vendasTimePorPeriodo.add(resultadoTimePorPeriodo);
				}
				vendidosPorPeriodo =  new ResultadoPorPeriodo(mesesVendidos, quantidadeVendidos, campeoes);
				break;
			case SEM_INTERESSE:
				mesesPerdidos.add("JANEIRO");
				mesesPerdidos.add("FEVEREIRO");
				mesesPerdidos.add("MARÇO");
				mesesPerdidos.add("ABRIL");
				mesesPerdidos.add("MAIO");
				mesesPerdidos.add("JUNHO");
				quantidadePerdidos.add(leadsJaneiro.size());
				quantidadePerdidos.add(leadsFevereiro.size());
				quantidadePerdidos.add(leadsMarco.size());
				quantidadePerdidos.add(leadsAbril.size());
				quantidadePerdidos.add(leadsMaio.size());
				quantidadePerdidos.add(leadsJunho.size());
				
				perdidosPorPeriodo = new ResultadoPorPeriodo(mesesPerdidos, quantidadePerdidos);
				break;
			case EM_ANDAMENTO:
				mesesNegociacao.add("JANEIRO");
				mesesNegociacao.add("FEVEREIRO");
				mesesNegociacao.add("MARÇO");
				mesesNegociacao.add("ABRIL");
				mesesNegociacao.add("MAIO");
				mesesNegociacao.add("JUNHO");
				quantidadeNegociacao.add(leadsJaneiro.size());
				quantidadeNegociacao.add(leadsFevereiro.size());
				quantidadeNegociacao.add(leadsMarco.size());
				quantidadeNegociacao.add(leadsAbril.size());
				quantidadeNegociacao.add(leadsMaio.size());
				quantidadeNegociacao.add(leadsJunho.size());
				
				emNegociacaoPorPeriodo = new ResultadoPorPeriodo(mesesNegociacao, quantidadeNegociacao);
				break;
			default:
				break;
			}
		} else {
			switch (status) {
			case VENDIDO:
				mesesVendidos.add("JULHO");
				mesesVendidos.add("AGOSTO");
				mesesVendidos.add("SETEMBRO");
				mesesVendidos.add("OUTUBRO");
				mesesVendidos.add("NOVEMBRO");
				mesesVendidos.add("DEZEMBRO");
				quantidadeVendidos.add(leadsJulho.size());
				quantidadeVendidos.add(leadsAgosto.size());
				quantidadeVendidos.add(leadsSetembro.size());
				quantidadeVendidos.add(leadsOutubro.size());
				quantidadeVendidos.add(leadsNovembro.size());
				quantidadeVendidos.add(leadsDezembro.size());
				
				switch (dataAtual.getMonth()) {
				case JULY:
					resultadoPlanoEmpresarial = new ResultadoPorTipoPlano("Julho", leadsJulho.stream().filter(lead -> TipoPlano.EMPRESARIAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size()); 
					resultadoPlanoFamiliar = new ResultadoPorTipoPlano("Julho", leadsJulho.stream().filter(lead -> TipoPlano.FAMILIAR.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoIndividual = new ResultadoPorTipoPlano("Julho", leadsJulho.stream().filter(lead -> TipoPlano.INDIVIDUAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoPme = new ResultadoPorTipoPlano("Julho", leadsJulho.stream().filter(lead -> TipoPlano.PME.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					break;
				case AUGUST:
					resultadoPlanoEmpresarial = new ResultadoPorTipoPlano("Agosto", leadsAgosto.stream().filter(lead -> TipoPlano.EMPRESARIAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size()); 
					resultadoPlanoFamiliar = new ResultadoPorTipoPlano("Agosto", leadsAgosto.stream().filter(lead -> TipoPlano.FAMILIAR.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoIndividual = new ResultadoPorTipoPlano("Agosto", leadsAgosto.stream().filter(lead -> TipoPlano.INDIVIDUAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoPme = new ResultadoPorTipoPlano("Agosto", leadsAgosto.stream().filter(lead -> TipoPlano.PME.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					break;
				case SEPTEMBER:
					resultadoPlanoEmpresarial = new ResultadoPorTipoPlano("Setembro", leadsSetembro.stream().filter(lead -> TipoPlano.EMPRESARIAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size()); 
					resultadoPlanoFamiliar = new ResultadoPorTipoPlano("Setembro", leadsSetembro.stream().filter(lead -> TipoPlano.FAMILIAR.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoIndividual = new ResultadoPorTipoPlano("Setembro", leadsSetembro.stream().filter(lead -> TipoPlano.INDIVIDUAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoPme = new ResultadoPorTipoPlano("Setembro", leadsSetembro.stream().filter(lead -> TipoPlano.PME.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					break;
				case OCTOBER:
					resultadoPlanoEmpresarial = new ResultadoPorTipoPlano("Outubro", leadsOutubro.stream().filter(lead -> TipoPlano.EMPRESARIAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size()); 
					resultadoPlanoFamiliar = new ResultadoPorTipoPlano("Outubro", leadsOutubro.stream().filter(lead -> TipoPlano.FAMILIAR.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoIndividual = new ResultadoPorTipoPlano("Outubro", leadsOutubro.stream().filter(lead -> TipoPlano.INDIVIDUAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoPme = new ResultadoPorTipoPlano("Outubro", leadsOutubro.stream().filter(lead -> TipoPlano.PME.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					break;
				case NOVEMBER:
					resultadoPlanoEmpresarial = new ResultadoPorTipoPlano("Novembro", leadsNovembro.stream().filter(lead -> TipoPlano.EMPRESARIAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size()); 
					resultadoPlanoFamiliar = new ResultadoPorTipoPlano("Novembro", leadsNovembro.stream().filter(lead -> TipoPlano.FAMILIAR.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoIndividual = new ResultadoPorTipoPlano("Novembro", leadsNovembro.stream().filter(lead -> TipoPlano.INDIVIDUAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoPme = new ResultadoPorTipoPlano("Novembro", leadsNovembro.stream().filter(lead -> TipoPlano.PME.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					break;
				case DECEMBER:
					resultadoPlanoEmpresarial = new ResultadoPorTipoPlano("Dezembro", leadsDezembro.stream().filter(lead -> TipoPlano.EMPRESARIAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size()); 
					resultadoPlanoFamiliar = new ResultadoPorTipoPlano("Dezembro", leadsDezembro.stream().filter(lead -> TipoPlano.FAMILIAR.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoIndividual = new ResultadoPorTipoPlano("Dezembro", leadsDezembro.stream().filter(lead -> TipoPlano.INDIVIDUAL.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					resultadoPlanoPme = new ResultadoPorTipoPlano("Dezembro", leadsDezembro.stream().filter(lead -> TipoPlano.PME.equals(lead.getTipoPlano())).collect(Collectors.toList()).size());
					break;
				default:
					break;
				}				
				
				for (TimeDTO time : times) {
					Map<Long, UserDTO> performanceCorretoresJulho = new HashMap<Long, UserDTO>();
					Map<Long, UserDTO> performanceCorretoresAgosto = new HashMap<Long, UserDTO>();
					Map<Long, UserDTO> performanceCorretoresSetembro = new HashMap<Long, UserDTO>();
					Map<Long, UserDTO> performanceCorretoresOutubro = new HashMap<Long, UserDTO>();
					Map<Long, UserDTO> performanceCorretoresNovembro = new HashMap<Long, UserDTO>();
					Map<Long, UserDTO> performanceCorretoresDezembro = new HashMap<Long, UserDTO>();
					int quantidadeVendidosTimeJulho = 0;
					int quantidadeVendidosTimeAgosto = 0;
					int quantidadeVendidosTimeSetembro = 0;
					int quantidadeVendidosTimeOutubro = 0;
					int quantidadeVendidosTimeNovembro = 0;
					int quantidadeVendidosTimeDezembro = 0;
					
					for (LeadDTO lead : leadsJulho) {
						for (UserDTO vendedor : time.getIntegrantes().stream().filter(integrante -> integrante.getCargo().getName().equals("VENDEDOR")).collect(Collectors.toList())) {
							if (vendedor.getIdUser().equals(lead.getResponsavel().getIdUser())) {
								quantidadeVendidosTimeJulho += 1;
								if (performanceCorretoresJulho.containsKey(vendedor.getIdUser())) {
									performanceCorretoresJulho.get(vendedor.getIdUser()).getRanking().setQuantidadeVendas(performanceCorretoresJulho.get(vendedor.getIdUser()).getRanking().getQuantidadeVendas() + 1L);
									performanceCorretoresJulho.get(vendedor.getIdUser()).getRanking().setValorVendas(performanceCorretoresJulho.get(vendedor.getIdUser()).getRanking().getValorVendas().add(lead.getValor()));
								} else {
									if (vendedor.getRanking().getQuantidadeVendas() == null) {
										RankingDTO rankingVendedor = new RankingDTO();
										rankingVendedor.setQuantidadeVendas(1L);
										rankingVendedor.setValorVendas(lead.getValor());
										vendedor.setRanking(rankingVendedor);
									} else {
										vendedor.getRanking().setQuantidadeVendas(vendedor.getRanking().getQuantidadeVendas() + 1L);
										vendedor.getRanking().setValorVendas(vendedor.getRanking().getValorVendas().add(lead.getValor()));
									}
									performanceCorretoresJulho.put(vendedor.getIdUser(), vendedor);
								}
								if(competidoresJulho.contains(vendedor)) {
									competidoresJulho.remove(vendedor);
								}
								vendedor.setPercentualAtingido(vendedor.buscaPorcentagemConcluidaMetaCorretor());
								competidoresJulho.add(vendedor);
							}
						}
					}
					
					for (LeadDTO lead : leadsAgosto) {
						for (UserDTO vendedor : time.getIntegrantes().stream().filter(integrante -> integrante.getCargo().getName().equals("VENDEDOR")).collect(Collectors.toList())) {
							if (vendedor.getIdUser().equals(lead.getResponsavel().getIdUser())) {
							quantidadeVendidosTimeAgosto += 1;
								if (performanceCorretoresAgosto.containsKey(vendedor.getIdUser())) {
									performanceCorretoresAgosto.get(vendedor.getIdUser()).getRanking().setQuantidadeVendas(performanceCorretoresAgosto.get(vendedor.getIdUser()).getRanking().getQuantidadeVendas() + 1L);
									performanceCorretoresAgosto.get(vendedor.getIdUser()).getRanking().setValorVendas(performanceCorretoresAgosto.get(vendedor.getIdUser()).getRanking().getValorVendas().add(lead.getValor()));
								} else {
									if (vendedor.getRanking().getQuantidadeVendas() == null) {
										RankingDTO rankingVendedor = new RankingDTO();
										rankingVendedor.setQuantidadeVendas(1L);
										rankingVendedor.setValorVendas(lead.getValor());
										vendedor.setRanking(rankingVendedor);
									} else {
										vendedor.getRanking().setQuantidadeVendas(vendedor.getRanking().getQuantidadeVendas() + 1L);
										vendedor.getRanking().setValorVendas(vendedor.getRanking().getValorVendas().add(lead.getValor()));
									}
									performanceCorretoresAgosto.put(vendedor.getIdUser(), vendedor);
								}
								if(competidoresAgosto.contains(vendedor)) {
									competidoresAgosto.remove(vendedor);
								}
								vendedor.setPercentualAtingido(vendedor.getPercentualAtingido().add(vendedor.buscaPorcentagemConcluidaMetaCorretor()));
								competidoresAgosto.add(vendedor);
							}
						}
					}
					
					for (LeadDTO lead : leadsSetembro) {
						for (UserDTO vendedor : time.getIntegrantes().stream().filter(integrante -> integrante.getCargo().getName().equals("VENDEDOR")).collect(Collectors.toList())) {
							if (vendedor.getIdUser().equals(lead.getResponsavel().getIdUser())) {
							quantidadeVendidosTimeSetembro += 1;
								if (performanceCorretoresSetembro.containsKey(vendedor.getIdUser())) {
									performanceCorretoresSetembro.get(vendedor.getIdUser()).getRanking().setQuantidadeVendas(performanceCorretoresSetembro.get(vendedor.getIdUser()).getRanking().getQuantidadeVendas() + 1L);
									performanceCorretoresSetembro.get(vendedor.getIdUser()).getRanking().setValorVendas(performanceCorretoresSetembro.get(vendedor.getIdUser()).getRanking().getValorVendas().add(lead.getValor()));
								} else {
									if (vendedor.getRanking().getQuantidadeVendas() == null) {
										RankingDTO rankingVendedor = new RankingDTO();
										rankingVendedor.setQuantidadeVendas(1L);
										rankingVendedor.setValorVendas(lead.getValor());
										vendedor.setRanking(rankingVendedor);
									} else {
										vendedor.getRanking().setQuantidadeVendas(vendedor.getRanking().getQuantidadeVendas() + 1L);
										vendedor.getRanking().setValorVendas(vendedor.getRanking().getValorVendas().add(lead.getValor()));
									}
									performanceCorretoresSetembro.put(vendedor.getIdUser(), vendedor);
								}
								if(competidoresSetembro.contains(vendedor)) {
									competidoresSetembro.remove(vendedor);
								}
								vendedor.setPercentualAtingido(vendedor.getPercentualAtingido().add(vendedor.buscaPorcentagemConcluidaMetaCorretor()));
								competidoresSetembro.add(vendedor);
							}
						}
					}
					
					for (LeadDTO lead : leadsOutubro) {
						for (UserDTO vendedor : time.getIntegrantes().stream().filter(integrante -> integrante.getCargo().getName().equals("VENDEDOR")).collect(Collectors.toList())) {
							if (vendedor.getIdUser().equals(lead.getResponsavel().getIdUser())) {
							quantidadeVendidosTimeOutubro += 1;
								if (performanceCorretoresOutubro.containsKey(vendedor.getIdUser())) {
									performanceCorretoresOutubro.get(vendedor.getIdUser()).getRanking().setQuantidadeVendas(performanceCorretoresOutubro.get(vendedor.getIdUser()).getRanking().getQuantidadeVendas() + 1L);
									performanceCorretoresOutubro.get(vendedor.getIdUser()).getRanking().setValorVendas(performanceCorretoresOutubro.get(vendedor.getIdUser()).getRanking().getValorVendas().add(lead.getValor()));
								} else {
									if (vendedor.getRanking().getQuantidadeVendas() == null) {
										RankingDTO rankingVendedor = new RankingDTO();
										rankingVendedor.setQuantidadeVendas(1L);
										rankingVendedor.setValorVendas(lead.getValor());
										vendedor.setRanking(rankingVendedor);
									} else {
										vendedor.getRanking().setQuantidadeVendas(vendedor.getRanking().getQuantidadeVendas() + 1L);
										vendedor.getRanking().setValorVendas(vendedor.getRanking().getValorVendas().add(lead.getValor()));
									}
									performanceCorretoresOutubro.put(vendedor.getIdUser(), vendedor);
								}
								if(competidoresOutubro.contains(vendedor)) {
									competidoresOutubro.remove(vendedor);
								}
								vendedor.setPercentualAtingido(vendedor.getPercentualAtingido().add(vendedor.buscaPorcentagemConcluidaMetaCorretor()));
								competidoresOutubro.add(vendedor);
							}
						}
					}
					
					for (LeadDTO lead : leadsNovembro) {
						for (UserDTO vendedor : time.getIntegrantes().stream().filter(integrante -> integrante.getCargo().getName().equals("VENDEDOR")).collect(Collectors.toList())) {
							if (vendedor.getIdUser().equals(lead.getResponsavel().getIdUser())) {
							quantidadeVendidosTimeNovembro += 1;
								if (performanceCorretoresNovembro.containsKey(vendedor.getIdUser())) {
									performanceCorretoresNovembro.get(vendedor.getIdUser()).getRanking().setQuantidadeVendas(performanceCorretoresNovembro.get(vendedor.getIdUser()).getRanking().getQuantidadeVendas() + 1L);
									performanceCorretoresNovembro.get(vendedor.getIdUser()).getRanking().setValorVendas(performanceCorretoresNovembro.get(vendedor.getIdUser()).getRanking().getValorVendas().add(lead.getValor()));
								} else {
									if (vendedor.getRanking().getQuantidadeVendas() == null) {
										RankingDTO rankingVendedor = new RankingDTO();
										rankingVendedor.setQuantidadeVendas(1L);
										rankingVendedor.setValorVendas(lead.getValor());
										vendedor.setRanking(rankingVendedor);
									} else {
										vendedor.getRanking().setQuantidadeVendas(vendedor.getRanking().getQuantidadeVendas() + 1L);
										vendedor.getRanking().setValorVendas(vendedor.getRanking().getValorVendas().add(lead.getValor()));
									}
									performanceCorretoresNovembro.put(vendedor.getIdUser(), vendedor);
								}
								if(competidoresNovembro.contains(vendedor)) {
									competidoresNovembro.remove(vendedor);
								}
								vendedor.setPercentualAtingido(vendedor.getPercentualAtingido().add(vendedor.buscaPorcentagemConcluidaMetaCorretor()));
								competidoresNovembro.add(vendedor);
							}
						}
					}
					
					for (LeadDTO lead : leadsDezembro) {
						for (UserDTO vendedor : time.getIntegrantes().stream().filter(integrante -> integrante.getCargo().getName().equals("VENDEDOR")).collect(Collectors.toList())) {
							if (vendedor.getIdUser().equals(lead.getResponsavel().getIdUser())) {
							quantidadeVendidosTimeDezembro += 1;
								if (performanceCorretoresDezembro.containsKey(vendedor.getIdUser())) {
									performanceCorretoresDezembro.get(vendedor.getIdUser()).getRanking().setQuantidadeVendas(performanceCorretoresDezembro.get(vendedor.getIdUser()).getRanking().getQuantidadeVendas() + 1L);
									performanceCorretoresDezembro.get(vendedor.getIdUser()).getRanking().setValorVendas(performanceCorretoresDezembro.get(vendedor.getIdUser()).getRanking().getValorVendas().add(lead.getValor()));
								} else {
									if (vendedor.getRanking().getQuantidadeVendas() == null) {
										RankingDTO rankingVendedor = new RankingDTO();
										rankingVendedor.setQuantidadeVendas(1L);
										rankingVendedor.setValorVendas(lead.getValor());
										vendedor.setRanking(rankingVendedor);
									} else {
										vendedor.getRanking().setQuantidadeVendas(vendedor.getRanking().getQuantidadeVendas() + 1L);
										vendedor.getRanking().setValorVendas(vendedor.getRanking().getValorVendas().add(lead.getValor()));
									}
									performanceCorretoresDezembro.put(vendedor.getIdUser(), vendedor);
								}
								if(competidoresDezembro.contains(vendedor)) {
									competidoresDezembro.remove(vendedor);
								}
								vendedor.setPercentualAtingido(vendedor.getPercentualAtingido().add(vendedor.buscaPorcentagemConcluidaMetaCorretor()));
								competidoresDezembro.add(vendedor);
							}
						}
					}
					int valorTimeMensal = 0;
					ResultadoPorPeriodo resultadoTimePorPeriodo = new ResultadoPorPeriodo();
					resultadoTimePorPeriodo.setNomeTime(time.getNome());
					resultadoTimePorPeriodo.setValorTimeSemestral(quantidadeVendidosTimeJulho+quantidadeVendidosTimeAgosto+quantidadeVendidosTimeSetembro+quantidadeVendidosTimeOutubro+quantidadeVendidosTimeNovembro+quantidadeVendidosTimeDezembro);
					campeoes.add(this.obterCampeaoMes(competidoresJulho));
					campeoes.add(this.obterCampeaoMes(competidoresAgosto));
					campeoes.add(this.obterCampeaoMes(competidoresSetembro));
					campeoes.add(this.obterCampeaoMes(competidoresOutubro));
					campeoes.add(this.obterCampeaoMes(competidoresNovembro));
					campeoes.add(this.obterCampeaoMes(competidoresDezembro));
					switch (dataAtual.getMonth()) {
					case JULY:
						valorTimeMensal = quantidadeVendidosTimeJulho;
						resultadoTimePorPeriodo.getIntegrantes().addAll(performanceCorretoresJulho.values());
						resultadoTimePorPeriodo.setPercentualAtingido(resultadoTimePorPeriodo.buscaPercentualMetaConcluidaTime());
						break;
					case AUGUST:
						valorTimeMensal = quantidadeVendidosTimeAgosto;
						resultadoTimePorPeriodo.getIntegrantes().addAll(performanceCorretoresAgosto.values());
						resultadoTimePorPeriodo.setPercentualAtingido(resultadoTimePorPeriodo.buscaPercentualMetaConcluidaTime());
						break;
					case SEPTEMBER:
						valorTimeMensal = quantidadeVendidosTimeSetembro;
						resultadoTimePorPeriodo.getIntegrantes().addAll(performanceCorretoresSetembro.values());
						resultadoTimePorPeriodo.setPercentualAtingido(resultadoTimePorPeriodo.buscaPercentualMetaConcluidaTime());
						break;
					case OCTOBER:
						valorTimeMensal = quantidadeVendidosTimeOutubro;
						resultadoTimePorPeriodo.getIntegrantes().addAll(performanceCorretoresOutubro.values());
						resultadoTimePorPeriodo.setPercentualAtingido(resultadoTimePorPeriodo.buscaPercentualMetaConcluidaTime());
						break;
					case NOVEMBER:
						valorTimeMensal = quantidadeVendidosTimeNovembro;
						resultadoTimePorPeriodo.getIntegrantes().addAll(performanceCorretoresNovembro.values());
						resultadoTimePorPeriodo.setPercentualAtingido(resultadoTimePorPeriodo.buscaPercentualMetaConcluidaTime());
						break;
					case DECEMBER:
						valorTimeMensal = quantidadeVendidosTimeDezembro;
						resultadoTimePorPeriodo.getIntegrantes().addAll(performanceCorretoresDezembro.values());
						resultadoTimePorPeriodo.setPercentualAtingido(resultadoTimePorPeriodo.buscaPercentualMetaConcluidaTime());
						break;
					default:
						break;
					}
					resultadoTimePorPeriodo.setValorTimeMensal(valorTimeMensal);
					vendasTimePorPeriodo.add(resultadoTimePorPeriodo);
				}				
				vendidosPorPeriodo = new ResultadoPorPeriodo(mesesVendidos, quantidadeVendidos, campeoes);
				break;
			case SEM_INTERESSE:
				mesesPerdidos.add("JULHO");
				mesesPerdidos.add("AGOSTO");
				mesesPerdidos.add("SETEMBRO");
				mesesPerdidos.add("OUTUBRO");
				mesesPerdidos.add("NOVEMBRO");
				mesesPerdidos.add("DEZEMBRO");
				quantidadePerdidos.add(leadsJulho.size());
				quantidadePerdidos.add(leadsAgosto.size());
				quantidadePerdidos.add(leadsSetembro.size());
				quantidadePerdidos.add(leadsOutubro.size());
				quantidadePerdidos.add(leadsNovembro.size());
				quantidadePerdidos.add(leadsDezembro.size());
				
				perdidosPorPeriodo = new ResultadoPorPeriodo(mesesPerdidos, quantidadePerdidos);
				break;
			case EM_ANDAMENTO:
				mesesNegociacao.add("JULHO");
				mesesNegociacao.add("AGOSTO");
				mesesNegociacao.add("SETEMBRO");
				mesesNegociacao.add("OUTUBRO");
				mesesNegociacao.add("NOVEMBRO");
				mesesNegociacao.add("DEZEMBRO");
				quantidadeNegociacao.add(leadsJulho.size());
				quantidadeNegociacao.add(leadsAgosto.size());
				quantidadeNegociacao.add(leadsSetembro.size());
				quantidadeNegociacao.add(leadsOutubro.size());
				quantidadeNegociacao.add(leadsNovembro.size());
				quantidadeNegociacao.add(leadsDezembro.size());
				
				emNegociacaoPorPeriodo = new ResultadoPorPeriodo(mesesNegociacao, quantidadeNegociacao);
				break;
			default:
				break;
			}
		}
	}

	private List<DadosGraficoMultilevel> gerarDadosGraficoMultiLevel(ResultadoPorPeriodo time) {
		List<DadosGraficoMultilevel> listaGrafico = new ArrayList<DadosGraficoMultilevel>();
			for (UserDTO vendedor : time.getIntegrantes()) {
				listaGrafico.add(new DadosGraficoMultilevel(vendedor.getFirstName(), vendedor.buscaStatusMeta(), vendedor.getRanking().getQuantidadeVendas()));
			}
			time.setStatusMetaTime(time.buscaStatus());
		return listaGrafico;
	}
	
	private List<DadosGraficoGauge> gerarDadosGraficoGauge(ResultadoPorPeriodo time) {
		List<DadosGraficoGauge> listaGrafico = new ArrayList<DadosGraficoGauge>();
		for (UserDTO vendedor : time.getIntegrantes()) {
			listaGrafico.add(new DadosGraficoGauge(vendedor.getPercentualAtingido(), "#0077CC", "80", vendedor.getFirstName()));
		}
	return listaGrafico;
	}	

	
	private UserDTO obterCampeaoMes(List<UserDTO> competidores) {
		UserDTO campeao = null;
		for (UserDTO userDTO : competidores) {
			if (campeao == null) {
				campeao = userDTO;
			} else {
				if (campeao.getRanking().getValorVendas().compareTo(userDTO.getRanking().getValorVendas()) == -1) {
					campeao = userDTO;
				}
			}
		}
		return campeao;
	}

	public ResultadoPorPeriodo getVendidosPorPeriodo() {
		return vendidosPorPeriodo;
	}

	public void setVendidosPorPeriodo(ResultadoPorPeriodo vendidosPorPeriodo) {
		this.vendidosPorPeriodo = vendidosPorPeriodo;
	}

	public ResultadoPorPeriodo getPerdidosPorPeriodo() {
		return perdidosPorPeriodo;
	}

	public void setPerdidosPorPeriodo(ResultadoPorPeriodo perdidosPorPeriodo) {
		this.perdidosPorPeriodo = perdidosPorPeriodo;
	}

	public ResultadoPorPeriodo getEmNegociacaoPorPeriodo() {
		return emNegociacaoPorPeriodo;
	}

	public void setEmNegociacaoPorPeriodo(ResultadoPorPeriodo emNegociacaoPorPeriodo) {
		this.emNegociacaoPorPeriodo = emNegociacaoPorPeriodo;
	}

	public List<ResultadoPorPeriodo> getVendasTimePorPeriodo() {
		return vendasTimePorPeriodo;
	}

	public void setVendasTimePorPeriodo(List<ResultadoPorPeriodo> vendasTimePorPeriodo) {
		this.vendasTimePorPeriodo = vendasTimePorPeriodo;
	}

	public List<ResultadoPorPeriodo> getPerdasTimePorPeriodo() {
		return perdasTimePorPeriodo;
	}

	public void setPerdasTimePorPeriodo(List<ResultadoPorPeriodo> perdasTimePorPeriodo) {
		this.perdasTimePorPeriodo = perdasTimePorPeriodo;
	}

	public List<ResultadoPorPeriodo> getEmNegociacaoTimePorPeriodo() {
		return emNegociacaoTimePorPeriodo;
	}

	public void setEmNegociacaoTimePorPeriodo(List<ResultadoPorPeriodo> emNegociacaoTimePorPeriodo) {
		this.emNegociacaoTimePorPeriodo = emNegociacaoTimePorPeriodo;
	}

	public ResultadoPorTipoPlano getResultadoPlanoIndividual() {
		return resultadoPlanoIndividual;
	}

	public void setResultadoPlanoIndividual(ResultadoPorTipoPlano resultadoPlanoIndividual) {
		this.resultadoPlanoIndividual = resultadoPlanoIndividual;
	}

	public ResultadoPorTipoPlano getResultadoPlanoFamiliar() {
		return resultadoPlanoFamiliar;
	}

	public void setResultadoPlanoFamiliar(ResultadoPorTipoPlano resultadoPlanoFamiliar) {
		this.resultadoPlanoFamiliar = resultadoPlanoFamiliar;
	}

	public ResultadoPorTipoPlano getResultadoPlanoEmpresarial() {
		return resultadoPlanoEmpresarial;
	}

	public void setResultadoPlanoEmpresarial(ResultadoPorTipoPlano resultadoPlanoEmpresarial) {
		this.resultadoPlanoEmpresarial = resultadoPlanoEmpresarial;
	}

	public ResultadoPorTipoPlano getResultadoPlanoPme() {
		return resultadoPlanoPme;
	}

	public void setResultadoPlanoPme(ResultadoPorTipoPlano resultadoPlanoPme) {
		this.resultadoPlanoPme = resultadoPlanoPme;
	}

	public List<DadosGraficoMultilevel> getDadosGraficoMultiLevelTime1() {
		return dadosGraficoMultiLevelTime1;
	}

	public List<DadosGraficoMultilevel> getDadosGraficoMultiLevelTime2() {
		return dadosGraficoMultiLevelTime2;
	}

	public List<DadosGraficoMultilevel> getDadosGraficoMultiLevelTime3() {
		return dadosGraficoMultiLevelTime3;
	}

	public List<DadosGraficoMultilevel> getDadosGraficoMultiLevelTime4() {
		return dadosGraficoMultiLevelTime4;
	}

	public List<DadosGraficoMultilevel> getDadosGraficoMultiLevelTime5() {
		return dadosGraficoMultiLevelTime5;
	}

	public List<DadosGraficoGauge> getDadosGraficoGaugeTime1() {
		return dadosGraficoGaugeTime1;
	}

	public List<DadosGraficoGauge> getDadosGraficoGaugeTime2() {
		return dadosGraficoGaugeTime2;
	}

	public List<DadosGraficoGauge> getDadosGraficoGaugeTime3() {
		return dadosGraficoGaugeTime3;
	}

	public List<DadosGraficoGauge> getDadosGraficoGaugeTime4() {
		return dadosGraficoGaugeTime4;
	}

	public List<DadosGraficoGauge> getDadosGraficoGaugeTime5() {
		return dadosGraficoGaugeTime5;
	}

	@Override
	public String toString() {
		return "PainelGeralLeadsDTO [vendidosPorPeriodo=" + vendidosPorPeriodo + ", perdidosPorPeriodo="
				+ perdidosPorPeriodo + ", emNegociacaoPorPeriodo=" + emNegociacaoPorPeriodo + ", vendasTimePorPeriodo="
				+ vendasTimePorPeriodo + ", perdasTimePorPeriodo=" + perdasTimePorPeriodo
				+ ", emNegociacaoTimePorPeriodo=" + emNegociacaoTimePorPeriodo + ", resultadoPlanoIndividual="
				+ resultadoPlanoIndividual + ", resultadoPlanoFamiliar=" + resultadoPlanoFamiliar
				+ ", resultadoPlanoPme=" + resultadoPlanoPme + ", resultadoPlanoEmpresarial="
				+ resultadoPlanoEmpresarial + "]";
	}

	public enum Meses {
		JANEIRO, FEVEREIRO, MARCO, ABRIL, MAIO, JUNHO, JULHO, AGOSTO, SETEMBRO, OUTUBRO, NOVEMBRO, DEZEMBRO
	}

	
}
