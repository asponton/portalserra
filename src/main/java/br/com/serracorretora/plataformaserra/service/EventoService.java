package br.com.serracorretora.plataformaserra.service;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.time.DayOfWeek;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import br.com.serracorretora.plataformaserra.domain.Evento;
import br.com.serracorretora.plataformaserra.domain.GerenciamentoPlantoes;
import br.com.serracorretora.plataformaserra.domain.User;
import br.com.serracorretora.plataformaserra.repository.EventoRepository;
import br.com.serracorretora.plataformaserra.repository.GerenciamentoPlantaoRepository;
import br.com.serracorretora.plataformaserra.repository.TimeRepository;
import br.com.serracorretora.plataformaserra.repository.UserRepository;
import br.com.serracorretora.plataformaserra.service.dto.ConfiguracaoPlantaoDTO;
import br.com.serracorretora.plataformaserra.service.dto.EventoDTO;
import br.com.serracorretora.plataformaserra.service.dto.Feriado;
import br.com.serracorretora.plataformaserra.service.dto.GerenciamentoPlantoesDTO;
import br.com.serracorretora.plataformaserra.service.dto.UserDTO;
import br.com.serracorretora.plataformaserra.web.rest.util.RespostaApiFeriado;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class EventoService {

    private final Logger log = LoggerFactory.getLogger(EventoService.class);

    @Inject
    private EventoRepository eventoRepository;
    
    @Inject
    private GerenciamentoPlantaoRepository gerenciamentoPlantaoRepository;
    
    @Inject
    private UserRepository userRepository;
    
    @Inject
    private TimeRepository timeRepository;
    
    public Evento updateEvento(EventoDTO eventoDTO) {
        return eventoRepository.findOneByTaskId(eventoDTO.getTaskId()).map(evento -> {
            
            evento.setTaskId(eventoDTO.getTaskId());
            evento.setOwnerId(eventoDTO.getOwnerId());
            evento.setTitle(eventoDTO.getTitle());
            evento.setDescription(eventoDTO.getDescription());
            evento.setStartTimeZone(eventoDTO.getStartTimeZone());
            evento.setStart(eventoDTO.getStart());
            evento.setEnd(eventoDTO.getEnd());
            evento.setEndTimeZone(eventoDTO.getEndTimeZone());
            evento.setRecurrenceRule(eventoDTO.getRecurrenceRule());
            evento.setRecurrenceCancelId(eventoDTO.getRecurrenceCancelId());
            evento.setRecurrenceId(eventoDTO.getRecurrenceId());
            evento.setRecurrenceException(eventoDTO.getRecurrenceException());
            evento.setIsAllDay(eventoDTO.getIsAllDay());
            
            eventoRepository.save(evento);
            log.debug("Updated Information for Evento: {}", evento);
            
            return evento;
            
        }).orElseGet(null);
    }
    
    public Evento criarEvento(EventoDTO eventoDTO) {
    	Evento evento = new Evento();
        evento.setTaskId(eventoRepository.buscarUltimoTaskId());
        evento.setCreatedDate(ZonedDateTime.now());
        evento.setDescription(eventoDTO.getDescription());
        evento.setEnd(eventoDTO.getEnd());
        evento.setEndTimeZone(eventoDTO.getEndTimeZone());
        evento.setIsAllDay(eventoDTO.getIsAllDay());
        evento.setRecurrenceCancelId(eventoDTO.getRecurrenceCancelId());
        evento.setRecurrenceException(eventoDTO.getRecurrenceException());
        evento.setRecurrenceId(eventoDTO.getRecurrenceId());
        evento.setRecurrenceRule(eventoDTO.getRecurrenceRule());
        evento.setStart(eventoDTO.getStart());
        evento.setStartTimeZone(eventoDTO.getStartTimeZone());
        evento.setTitle(eventoDTO.getTitle());
        evento.setOwnerId(eventoDTO.getOwnerId());
        evento.setPlantaoId(eventoDTO.getPlantaoId());
        evento.setQuantidadeIndicacoes(0L);
        evento.setQuantidadeLeadsCorporativo(0L);
        evento.setQuantidadeLeadsFamiliar(0L);
        evento.setQuantidadeLeadsPme(0L);
        evento.setQuantidadeLeadsIndividual(0L);
    	return eventoRepository.save(evento);
    }

	public void deleteEvento(EventoDTO eventoDTO) {
		  eventoRepository.findOneByTaskId(eventoDTO.getTaskId()).ifPresent(evento -> {
			  eventoRepository.delete(evento);
	            log.debug("Deleted Evento: {}", evento);
	        });
	}

	public List<ConfiguracaoPlantaoDTO> abrirMes(GerenciamentoPlantoesDTO aberturaMensal) {
		
		Optional<GerenciamentoPlantoes> periodoJaAberto = gerenciamentoPlantaoRepository.findOneByPeriodoFechadoFalse();
		
		if (!periodoJaAberto.isPresent()) {
			GerenciamentoPlantoes gerenciamentoPlantoes = new GerenciamentoPlantoes();
			gerenciamentoPlantoes.setPeriodoInicial(aberturaMensal.getPeriodoInicial());
			gerenciamentoPlantoes.setPeriodoFinal(aberturaMensal.getPeriodoFinal());
			gerenciamentoPlantoes.setPeriodoFechado(false);
			gerenciamentoPlantaoRepository.save(gerenciamentoPlantoes);
		}
		
		List<UserDTO> vendedores = this.getVendedores(aberturaMensal);
		
		List<ConfiguracaoPlantaoDTO> selecaoVendedoresDTO = new ArrayList<>();
			for (UserDTO vendedor : vendedores) {
				ConfiguracaoPlantaoDTO vendedorSelecionado = new ConfiguracaoPlantaoDTO();
				vendedorSelecionado.setVendedorSelecionado(vendedor);
				vendedorSelecionado.setPeriodoInicial(aberturaMensal.getPeriodoInicial());
				vendedorSelecionado.setPeriodoFinal(aberturaMensal.getPeriodoFinal());
				selecaoVendedoresDTO.add(vendedorSelecionado);
			}
				
		return selecaoVendedoresDTO;
	}

	private List<UserDTO> getVendedores(GerenciamentoPlantoesDTO aberturaMensal) {
		List<UserDTO> vendedores = new ArrayList<UserDTO>();
		if (aberturaMensal.getVendedores() != null && !aberturaMensal.getVendedores().isEmpty()) {
			for (Long idUser : aberturaMensal.getVendedores()) {
				vendedores.add(new UserDTO(userRepository.findOne(idUser)));
			}
			return vendedores;
		} else {
			for (Long idTime : aberturaMensal.getTimes()) {
				for (User integrante : timeRepository.findOne(idTime).getIntegrantes()) {
					vendedores.add(new UserDTO(integrante));
				}
			}
			return vendedores.stream().filter(vendedor -> "VENDEDOR".equals(vendedor.getCargo().getName())).collect(Collectors.toList());
		}
	}

	public void iniciarPlantao(List<ConfiguracaoPlantaoDTO> configuracoesPlantoes) {
		Long idPlantao = gerenciamentoPlantaoRepository.findOneByPeriodoFechadoFalse().get().getId();
		for (ConfiguracaoPlantaoDTO configuracaoPlantao : configuracoesPlantoes) {
			if (configuracaoPlantao.getQuantidadePlantoes() != null) {
				for (int i = 0; i < configuracaoPlantao.getQuantidadePlantoes().intValue(); i++) {
					EventoDTO evento = new EventoDTO();
					evento.setOwnerId(configuracaoPlantao.getVendedorSelecionado().getIdUser());
					evento.setTaskId(eventoRepository.buscarUltimoTaskId());
					evento.setPlantaoId(idPlantao);
					evento.setIsAllDay(true);
					evento.setStart(i == 0 ? configuracaoPlantao.getPeriodoInicial() : this.atribuirDia(configuracaoPlantao.getPeriodoInicial(), configuracaoPlantao.getPeriodoFinal(), configuracaoPlantao.getVendedorSelecionado().getIdUser()));
					evento.setEnd(evento.getStart());
					evento.setTitle(configuracaoPlantao.getVendedorSelecionado().getFirstName());
					evento.setDescription("Plantão do dia " + evento.getStart());
					this.criarEvento(evento);
				}
			}
		}
		
	}

	@SuppressWarnings("serial")
	private ZonedDateTime atribuirDia(ZonedDateTime periodoInicial, ZonedDateTime periodoFinal, Long idUser) {
		ZonedDateTime dataDisponivel = periodoInicial;
		List<Evento> findOneByStart = null;
		boolean ultrapassouPeriodoFinal = false;
		boolean comecarAPularUmDiaSomente = false;
		do {
			dataDisponivel = ultrapassouPeriodoFinal || DayOfWeek.SUNDAY.equals(dataDisponivel.getDayOfWeek()) || comecarAPularUmDiaSomente ? dataDisponivel.plusDays(1) : dataDisponivel.plusDays(2);
			ultrapassouPeriodoFinal = dataDisponivel.getDayOfYear() > periodoFinal.getDayOfYear();
			if (ultrapassouPeriodoFinal) {
				findOneByStart = eventoRepository.findOneByStart(periodoInicial);
				dataDisponivel = periodoInicial;
				comecarAPularUmDiaSomente = true;
			} else {
				findOneByStart = eventoRepository.findOneByStart(dataDisponivel);
				if (findOneByStart != null && !findOneByStart.isEmpty()) {
					List<Evento> eventosDoCorretorEmQuestao = findOneByStart.stream().filter(evento -> evento.getOwnerId() == idUser).collect(Collectors.toList());
					if (eventosDoCorretorEmQuestao == null || eventosDoCorretorEmQuestao.isEmpty()) {
						if (!DayOfWeek.SATURDAY.equals(dataDisponivel.getDayOfWeek()) || !DayOfWeek.SUNDAY.equals(dataDisponivel.getDayOfWeek()) || !this.isFeriado(dataDisponivel)) {
							findOneByStart = null;
						}
					}
				} else {
					if (DayOfWeek.SATURDAY.equals(dataDisponivel.getDayOfWeek()) || DayOfWeek.SUNDAY.equals(dataDisponivel.getDayOfWeek()) || this.isFeriado(dataDisponivel)) {
						findOneByStart = new ArrayList<Evento>() {{	add(new Evento());	}};
					}
				}
			}
		} while (findOneByStart != null && !findOneByStart.isEmpty());
		
		return dataDisponivel;
	}

	private boolean isFeriado(ZonedDateTime dataDisponivel) {
		
		TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

		SSLContext sslContext = null;
		try {
			sslContext = org.apache.http.ssl.SSLContexts.custom()
			        .loadTrustMaterial(null, acceptingTrustStrategy)
			        .build();
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

		CloseableHttpClient httpClient = HttpClients.custom()
		        .setSSLSocketFactory(csf)
		        .build();

		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();

		requestFactory.setHttpClient(httpClient);
		
		
		boolean resultado = false;
		String key = "key=22535b21-3f89-4032-981c-78083c0649e4";
		String country = "&country=BR";
		String year = "&year="+String.valueOf(ZonedDateTime.now().getYear());
		String month = "&month="+String.valueOf(ZonedDateTime.now().getMonthValue());
		
		String urlFinal = "https://holidayapi.com/v1/holidays?".concat(key).concat(country).concat(year).concat(month);
		
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		ResponseEntity<RespostaApiFeriado> resposta = restTemplate.getForEntity(urlFinal, RespostaApiFeriado.class);
		RespostaApiFeriado body = resposta.getBody();
		
		for (Feriado feriado : body.getHolidays()) {
			if (ZonedDateTime.parse(feriado.getDate().concat("T00:00:00+00:00[America/Sao_Paulo]")).getDayOfYear() == dataDisponivel.getDayOfYear()) {
				resultado = true;
			}
		}
		return resultado;
	}

}
