package br.com.serracorretora.plataformaserra.service.dto;

import java.time.ZonedDateTime;

import br.com.serracorretora.plataformaserra.domain.Lead.Status;
import br.com.serracorretora.plataformaserra.domain.Repescagem;

public class RepescagemDTO {
	
	private Long id;
	
	private ZonedDateTime inicio;
	
	private ZonedDateTime fim;
	
	private Status status;
	
	private boolean aprovada;
	
	public RepescagemDTO(Repescagem repescagem) {
		this(repescagem.getId(), repescagem.getInicio(), repescagem.getFim(), repescagem.getStatus(), repescagem.isAprovada());
	}
	

	public RepescagemDTO(Long id, ZonedDateTime inicio, ZonedDateTime fim, Status status, boolean aprovada) {
		super();
		this.id = id;
		this.inicio = inicio;
		this.fim = fim;
		this.status = status;
		this.aprovada = aprovada;
	}
	
	public RepescagemDTO(ZonedDateTime inicio, ZonedDateTime fim, Status status) {
		this.inicio = inicio;
		this.fim = fim;
		this.status = status;
		this.aprovada = false;
	}	

	public ZonedDateTime getInicio() {
		return inicio;
	}

	public void setInicio(ZonedDateTime inicio) {
		this.inicio = inicio;
	}

	public ZonedDateTime getFim() {
		return fim;
	}

	public void setFim(ZonedDateTime fim) {
		this.fim = fim;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public boolean isAprovada() {
		return aprovada;
	}

	public void setAprovada(boolean aprovada) {
		this.aprovada = aprovada;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
