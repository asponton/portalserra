package br.com.serracorretora.plataformaserra.service.dto;

import java.time.ZonedDateTime;

public class ConfiguracaoPlantaoDTO {
	
	private UserDTO vendedorSelecionado;
	
	private ZonedDateTime periodoInicial;
	
	private ZonedDateTime periodoFinal;
	
	private Long quantidadePlantoes;

	public UserDTO getVendedorSelecionado() {
		return vendedorSelecionado;
	}

	public void setVendedorSelecionado(UserDTO vendedorSelecionado) {
		this.vendedorSelecionado = vendedorSelecionado;
	}

	public ZonedDateTime getPeriodoInicial() {
		return periodoInicial;
	}

	public void setPeriodoInicial(ZonedDateTime periodoInicial) {
		this.periodoInicial = periodoInicial;
	}

	public ZonedDateTime getPeriodoFinal() {
		return periodoFinal;
	}

	public void setPeriodoFinal(ZonedDateTime periodoFinal) {
		this.periodoFinal = periodoFinal;
	}

	public Long getQuantidadePlantoes() {
		return quantidadePlantoes;
	}

	public void setQuantidadePlantoes(Long quantidadePlantoes) {
		this.quantidadePlantoes = quantidadePlantoes;
	}

}
