package br.com.serracorretora.plataformaserra.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.serracorretora.plataformaserra.domain.Repescagem;
import br.com.serracorretora.plataformaserra.repository.LeadRepository;
import br.com.serracorretora.plataformaserra.repository.RepescagemRepository;
import br.com.serracorretora.plataformaserra.service.dto.LeadDTO;
import br.com.serracorretora.plataformaserra.service.dto.RepescagemDTO;

@Service
@Transactional
public class RepescagemService {
	
	@Inject
	LeadRepository leadRepository;
	
	@Inject
	RepescagemRepository repescagemRepository;
	

    private final Logger logger = LoggerFactory.getLogger(RepescagemService.class);
    
    public List<LeadDTO> trazerLeadsParaRepescagem(RepescagemDTO repescagemDTO) {
    	logger.info("Buscando leads....");
    	return leadRepository.findByStatusAndDataEntradaBetween(repescagemDTO.getStatus(), repescagemDTO.getInicio(), repescagemDTO.getFim()).stream().map(lead -> new LeadDTO(lead)).collect(Collectors.toList());
    }
    
    public Repescagem criarCampanhaRepescagem(RepescagemDTO repescagemDTO) {
    	Repescagem repescagem = null;
    	if (repescagemRepository.findByInicioAfterAndFimBeforeAndStatus(repescagemDTO.getInicio(), repescagemDTO.getFim(), repescagemDTO.getStatus()) == null) {
	        repescagem = new Repescagem();
	        repescagem.setInicio(repescagemDTO.getInicio());
	        repescagem.setFim(repescagemDTO.getFim());
	        repescagem.setAprovada(false);
	        repescagem.setStatus(repescagemDTO.getStatus());
	        
	        repescagemRepository.save(repescagem);
	        logger.debug("Created Information for Repescagem: {}", repescagem);
    	}
        return repescagem;
    }
    
    public Repescagem updateRepescagem(RepescagemDTO repescagemDTO) {
        return repescagemRepository.findOneById(repescagemDTO.getId()).map(repescagem -> {
            
            repescagem.setAprovada(true);
            
            repescagemRepository.save(repescagem);
            logger.debug("Created Information for Repescagem: {}", repescagem);
            
            return repescagem;
            
        }).orElseGet(null);
    }    
    
    
       
    
    
    

}
