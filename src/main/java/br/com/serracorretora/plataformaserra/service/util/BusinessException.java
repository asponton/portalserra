package br.com.serracorretora.plataformaserra.service.util;

public class BusinessException extends Exception {

    private static final long serialVersionUID = 1L;
    
    private String errorKey;

    public BusinessException(String errorKey, String message) {
        super(message);
        this.errorKey = errorKey;
    }

    public String getErrorKey() {
        return errorKey;
    }

    
    
}
