package br.com.serracorretora.plataformaserra.service.dto;

import java.time.ZonedDateTime;

import br.com.serracorretora.plataformaserra.domain.Notificacao;
import br.com.serracorretora.plataformaserra.domain.Notificacao.StatusPendencia;
import br.com.serracorretora.plataformaserra.domain.User;

public class NotificacoesDTO {
	
    private Long id;
    
    private ZonedDateTime dataEntrada;
    
    private User responsavel;
    
    private StatusPendencia statusPendencia;
    
    private String mensagemDiretoria;
    
    private String mensagemJustificativa;
    
    private Long numeroLead;
    
    public NotificacoesDTO() {}
    
    public NotificacoesDTO(Notificacao notificacoes) {
    	this(notificacoes.getId(), notificacoes.getDataEntrada(), notificacoes.getCobranca(), notificacoes.getJustificativa(), notificacoes.getResponsavel(), notificacoes.getStatusPendencia(), notificacoes.getNumeroLead());
    }

	public NotificacoesDTO(Long id, ZonedDateTime dataEntrada, String mensagemDiretoria, String mensagemJustificativa, User responsavel, StatusPendencia statusPendencia, Long numeroLead) {
		this.id = id;
		this.dataEntrada = dataEntrada;
		this.responsavel = responsavel;
		this.statusPendencia = statusPendencia;
		this.mensagemDiretoria = mensagemDiretoria;
		this.mensagemJustificativa = mensagemJustificativa;
		this.numeroLead = numeroLead;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(ZonedDateTime dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public User getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(User responsavel) {
		this.responsavel = responsavel;
	}

	public StatusPendencia getStatusPendencia() {
		return statusPendencia;
	}

	public void setStatusPendencia(StatusPendencia statusPendencia) {
		this.statusPendencia = statusPendencia;
	}

	public String getMensagemDiretoria() {
		return mensagemDiretoria;
	}

	public void setMensagemDiretoria(String mensagemDiretoria) {
		this.mensagemDiretoria = mensagemDiretoria;
	}

	public String getMensagemJustificativa() {
		return mensagemJustificativa;
	}

	public void setMensagemJustificativa(String mensagemJustificativa) {
		this.mensagemJustificativa = mensagemJustificativa;
	}

	public Long getNumeroLead() {
		return numeroLead;
	}

	public void setNumeroLead(Long numeroLead) {
		this.numeroLead = numeroLead;
	}


}
