package br.com.serracorretora.plataformaserra.service.dto;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import br.com.serracorretora.plataformaserra.domain.Ranking;


public class RankingDTO {

    private Long id;
    
    private ZonedDateTime dataCompetencia;    
    
    private BigDecimal valorBase;
    
    private Long pontos;
    
    private BigDecimal valorVendas;
    
    private Long quantidadeVendas;
    

	public RankingDTO() {
		super();
	}

	public RankingDTO(Ranking ranking) {
		this(ranking.getId(), ranking.getDataCompetencia(), ranking.getValorBase(), ranking.getPontos(), ranking.getValorVendas(), ranking.getQuantidadeVendas());
	}

	public RankingDTO(Long id, ZonedDateTime dataCompetencia, BigDecimal valorBase, Long pontos, BigDecimal valorVendas, Long quantidadeVendas) {
		this.id = id;
		this.dataCompetencia = dataCompetencia;
		this.valorBase = valorBase;
		this.pontos = pontos;
		this.valorVendas = valorVendas;
		this.quantidadeVendas = quantidadeVendas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getDataCompetencia() {
		return dataCompetencia;
	}

	public void setDataCompetencia(ZonedDateTime dataCompetencia) {
		this.dataCompetencia = dataCompetencia;
	}

	public BigDecimal getValorBase() {
		return valorBase;
	}

	public void setValorBase(BigDecimal valorBase) {
		this.valorBase = valorBase;
	}

	public Long getPontos() {
		return pontos;
	}

	public void setPontos(Long pontos) {
		this.pontos = pontos;
	}

	public BigDecimal getValorVendas() {
		return valorVendas;
	}

	public void setValorVendas(BigDecimal valorVendas) {
		this.valorVendas = valorVendas;
	}

	public Long getQuantidadeVendas() {
		return quantidadeVendas;
	}

	public void setQuantidadeVendas(Long quantidadeVendas) {
		this.quantidadeVendas = quantidadeVendas;
	}

}
