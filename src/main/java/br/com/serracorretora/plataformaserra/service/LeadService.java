package br.com.serracorretora.plataformaserra.service;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.serracorretora.plataformaserra.domain.Cargo;
import br.com.serracorretora.plataformaserra.domain.Evento;
import br.com.serracorretora.plataformaserra.domain.GerenciamentoPlantoes;
import br.com.serracorretora.plataformaserra.domain.Lead;
import br.com.serracorretora.plataformaserra.domain.Lead.Status;
import br.com.serracorretora.plataformaserra.domain.Lead.TipoPlano;
import br.com.serracorretora.plataformaserra.domain.Notificacao.StatusPendencia;
import br.com.serracorretora.plataformaserra.domain.Ranking;
import br.com.serracorretora.plataformaserra.domain.User;
import br.com.serracorretora.plataformaserra.repository.EventoRepository;
import br.com.serracorretora.plataformaserra.repository.GerenciamentoPlantaoRepository;
import br.com.serracorretora.plataformaserra.repository.LeadRepository;
import br.com.serracorretora.plataformaserra.repository.NotificacoesRepository;
import br.com.serracorretora.plataformaserra.repository.RankingRepository;
import br.com.serracorretora.plataformaserra.repository.UserRepository;
import br.com.serracorretora.plataformaserra.service.dto.EventoDTO;
import br.com.serracorretora.plataformaserra.service.dto.LeadDTO;
import br.com.serracorretora.plataformaserra.service.dto.UserDTO;

/**
 * Service class for managing leads.
 */
@Service
@Transactional
public class LeadService {

    private final Logger log = LoggerFactory.getLogger(LeadService.class);

    @Inject
    private LeadRepository leadRepository;
    
    @Inject
    private UserRepository userRepository;
    
    @Inject
    private RankingRepository rankingRepository;
    
    @Inject
    private EventoRepository eventoRepository;
    
    @Inject
    private NotificacaoService notificacaoService;    
    
    @Inject
    private UserService userService;       
    
    @Inject
    private NotificacoesRepository notificacoesRepository;
    
    @Inject
    private GerenciamentoPlantaoRepository gerenciamentoPlantaoRepository;
    
    @Inject
    private MailService mailService;
    

    public Lead createLead(LeadDTO leadDTO) {
        Lead lead = new Lead();
        lead.setCreatedBy("MARKETING");
        lead.setNome(leadDTO.getNome());
        lead.setEmail(leadDTO.getEmail());
        lead.setDdd(leadDTO.getDdd());
        lead.setTelefone(leadDTO.getTelefone());
        lead.setMotivoStatus(leadDTO.getMotivoStatus());
        lead.setTipoPlano(leadDTO.getTipoPlano());
        lead.setStatus(Status.DISPONIVEL);
        lead.setFaixaEtaria(leadDTO.getFaixaEtaria());
        lead.setDataEntrada(ZonedDateTime.now(ZoneId.of("America/Sao_Paulo")));
        lead.setDataEncaminhamento(ZonedDateTime.now(ZoneId.of("America/Sao_Paulo")));
        lead.setObservacoes(leadDTO.getObservacoes());
        lead.setOperadora(leadDTO.getOperadora());
        lead.setValor(leadDTO.getValor() == null ? BigDecimal.ZERO : leadDTO.getValor());
        lead.setResponsavel(this.atribuirLeadAoMelhorRankeado(leadDTO));
        lead.setResponsavelOriginal(lead.getResponsavel() != null ? lead.getResponsavel().getId() : null);
        leadDTO.setResponsavel(lead.getResponsavel() != null ? new UserDTO(lead.getResponsavel()) : null);
        leadDTO.setIdResponsavelOriginal(leadDTO.getResponsavel() != null ? leadDTO.getResponsavel().getIdUser() : null);
        leadRepository.save(lead);
        notificacaoService.inserirNotificacao(leadDTO, StatusPendencia.INFO);
        log.debug("Created Information for Lead: {}", lead);
        return lead;
    }

	private User atribuirLeadAoMelhorRankeado(LeadDTO leadDTO) {
		TreeMap<Long, User> vendasDosPlantonistasPorPeriodo = new TreeMap<Long, User>(new Comparator<Long>() {
			@Override
			public int compare(Long o1, Long o2) {
				return o1.compareTo(o2);
			}
		});

		User userEscolhido = null;
		int ano = ZonedDateTime.now().getYear();
		ZonedDateTime fimDia = ZonedDateTime.of(ano, ZonedDateTime.now().getMonthValue(), ZonedDateTime.now().getDayOfMonth(), 18, 0, 0, 0, ZoneId.of("America/Sao_Paulo"));
		
		List<GerenciamentoPlantoes> periodosFechados = gerenciamentoPlantaoRepository.findOneByPeriodoFechadoTrue();
		Optional<GerenciamentoPlantoes> plantaoMensal = periodosFechados.stream().filter(periodoPlantao -> periodoPlantao.getPeriodoInicial().getMonth().equals(fimDia.getMonth())).findAny();
		
		Set<User> usuariosDoPlantaoMensal = new HashSet<User>();
		User user = null;
		if (plantaoMensal.isPresent()) {
			List<EventoDTO> eventos = eventoRepository.findAllByPlantaoId(plantaoMensal.get().getId()).stream().map(evento -> new EventoDTO(evento)).collect(Collectors.toList());
			for (EventoDTO eventoDTO : eventos) {
				user = userService.getUserWithAuthorities(eventoDTO.getOwnerId());
				usuariosDoPlantaoMensal.add(user);
			}
		} else {
			Optional<User> gerente = userRepository.findAllByCargo(new Cargo("GERENTE")).stream().findFirst();
			return gerente.get();
		}
		
		this.retirarPlantonistasComPendencias(usuariosDoPlantaoMensal);
		
		userEscolhido = this.escolherMelhorVendedor(vendasDosPlantonistasPorPeriodo, ano, fimDia, usuariosDoPlantaoMensal, leadDTO);
		
		return userEscolhido;
	}

	private void retirarPlantonistasComPendencias(Set<User> plantonistas) {
		User supervisor = null;
		if (plantonistas != null && !plantonistas.isEmpty()) {
			List<User> plantonistasComPendencias = new ArrayList<User>();
			for (User user : plantonistas) {
				if (!notificacoesRepository.findAllByResponsavelAndStatusPendencia(user, StatusPendencia.PENDENTE).isEmpty()) {
					plantonistasComPendencias.add(user);
				}
				supervisor = user.getResponsavel();
			}
			
			for (User plantonistaExcluido : plantonistasComPendencias) {
				plantonistas.remove(plantonistaExcluido);
			}
			
			if (plantonistas.isEmpty()) {
				plantonistas.add(supervisor);
			}
		} else {
			plantonistas.add(userRepository.findAllByCargo(new Cargo("GERENTE")).get(0));
		}
	}

	private User escolherMelhorVendedor(TreeMap<Long, User> vendasDosPlantonistasPorPeriodo, int ano, ZonedDateTime fimDia, Set<User> plantonistasCategoria, LeadDTO leadDTO) {
		User userEscolhido;
		Long quantidadePontosPlantonista;
		boolean limitePlantonistaAtingido = false;
		for (User plantonista : plantonistasCategoria) {
			if (leadDTO.getDdd() >= plantonista.getRangeDddMinimo() && leadDTO.getDdd() <= plantonista.getRangeDddMaximo()) {
				quantidadePontosPlantonista = 0L;
				 Evento plantaoDoDia = eventoRepository.findAllByOwnerId(plantonista.getId()).stream().filter(evento -> evento.getStart().getDayOfYear() == ZonedDateTime.now().getDayOfYear()).findAny().get();
				
				 List<Ranking> rankingsDoMes = rankingRepository.findAllByUsuarioAndDataCompetenciaBetween(plantonista, ZonedDateTime.of(ano, ZonedDateTime.now().getMonthValue(),1, 0, 0, 0, 0, ZoneId.of("America/Sao_Paulo")), fimDia);
				 if (rankingsDoMes != null && !rankingsDoMes.isEmpty()) {
					 for (Ranking rankingPlantaoDoDia : rankingsDoMes) {
						if (plantaoDoDia.getQuantidadeIndicacoes() <= plantonista.getLimiteLeads()) {
							quantidadePontosPlantonista = quantidadePontosPlantonista + rankingPlantaoDoDia.getPontos();
						} else {
							limitePlantonistaAtingido = true;
						}
					 }
				 }
					 
					switch (leadDTO.getTipoPlano()) {
					case INDIVIDUAL:
						if (plantaoDoDia.getQuantidadeLeadsIndividual() <= plantonista.getQuantidadeLeadsIndividual()) {
							if (plantonista.getQuantidadeLeadsIndividual() != 0L) {
								break;
							} else {
								continue;
							}
						} else {
							continue;
						}
					case FAMILIAR:
						if (plantaoDoDia.getQuantidadeLeadsFamiliar() <= plantonista.getQuantidadeLeadsFamiliar()) {
							if (plantonista.getQuantidadeLeadsFamiliar() != 0L) {
								break;
							} else {
								continue;
							}
						} else {
							continue;
						}
					case PME:
						if (plantaoDoDia.getQuantidadeLeadsPme() <= plantonista.getQuantidadeLeadsPme()) {
							if (plantonista.getQuantidadeLeadsPme() != 0L) {
								break;
							} else {
								continue;
							}
						} else {
							continue;
						}
					case EMPRESARIAL:
						if (plantaoDoDia.getQuantidadeLeadsCorporativo() <= plantonista.getQuantidadeLeadsEmpresarial()) {
							if (plantonista.getQuantidadeLeadsEmpresarial() != 0L) {
								break;
							} else {
								continue;
							}
						} else {
							continue;
						}
					default:
						break;
					}
					
					if (!limitePlantonistaAtingido) {
						vendasDosPlantonistasPorPeriodo.put(quantidadePontosPlantonista, plantonista);
					} else {
						continue;
					}
			}
		}
		userEscolhido = vendasDosPlantonistasPorPeriodo.isEmpty() ? null : vendasDosPlantonistasPorPeriodo.get(vendasDosPlantonistasPorPeriodo.firstKey());
		return userEscolhido == null ? userRepository.findAllByCargo(new Cargo("SUPERVISOR")).stream().findFirst().get() : userEscolhido;
	}


	public Collection<Lead> buscarLeadsPorResponsavel(UserDTO usuario) {
		User user = null;
		if (usuario.getCargo().getName().equals("DIRETOR")) {
			user = userRepository.findAllByCargo(new Cargo("GERENTE")).get(0);
		} else {
			 user = userRepository.findOneById(usuario.getIdUser()).get();
		}
		return leadRepository.findAllByResponsavelOrderByDataEncaminhamentoDesc(user);
	}
	
	public Collection<Lead> buscarLeadsPorResponsavelEStatus(UserDTO usuario, Status status) {
		User user = null;
		if (usuario.getCargo().getName().equals("DIRETOR")) {
			user = userRepository.findAllByCargo(new Cargo("GERENTE")).get(0);
		} else {
			 user = userRepository.findOneById(usuario.getIdUser()).get();
		}
		return leadRepository.findAllByResponsavelAndStatusOrderByDataEncaminhamentoDesc(user, status);
	}
	

	public Lead updateLead(LeadDTO leadDTO, String baseUrl) {	
		Optional<User> user = userRepository.findOneById(leadDTO.getResponsavel().getIdUser());
		return leadRepository.findOneById(leadDTO.getId()).map(lead -> {
		
        lead.setCreatedBy(this.isDiretoria(leadDTO) ? "GERENCIA" : lead.getCreatedBy());
        lead.setNome(leadDTO.getNome());
        lead.setEmail(leadDTO.getEmail());
        lead.setDdd(leadDTO.getDdd());
        lead.setTelefone(leadDTO.getTelefone());
        lead.setMotivoStatus(leadDTO.getMotivoStatus());
        lead.setTipoPlano(leadDTO.getTipoPlano());
        lead.setStatus(this.isDiretoria(leadDTO) && leadDTO.getDataEncaminhamento() != null ? Lead.Status.DISPONIVEL : leadDTO.getStatus());
        lead.setFaixaEtaria(leadDTO.getFaixaEtaria());
        lead.setDataAlteracaoStatus(ZonedDateTime.now(ZoneId.of("America/Sao_Paulo")));
        lead.setDataEncaminhamento(this.isDiretoria(leadDTO) ? ZonedDateTime.now(ZoneId.of("America/Sao_Paulo")) : leadDTO.getDataEncaminhamento());
        lead.setDataRetorno(Lead.Status.EM_ANDAMENTO.equals(leadDTO.getStatus()) ? leadDTO.getDataRetorno() : null);
        lead.setLastModifiedDate(ZonedDateTime.now(ZoneId.of("America/Sao_Paulo")));
        lead.setOperadora(leadDTO.getOperadora());
        lead.setValor(leadDTO.getValor() == null ? BigDecimal.ZERO : leadDTO.getValor());
        lead.setObservacoes(leadDTO.getObservacoes());
        if (Status.VENDIDO.equals(leadDTO.getStatus()) || Status.SEM_INTERESSE.equals(leadDTO.getStatus()) || Status.EM_ANDAMENTO.equals(leadDTO.getStatus()) && !leadDTO.getStatus().equals(lead.getStatus())) {
	        lead.setResponsavel(this.atualizarPlantaoERankingVendedor(leadDTO));
	        if (Status.VENDIDO.equals(leadDTO.getStatus()) || Status.SEM_INTERESSE.equals(leadDTO.getStatus())) {
	        	mailService.sendPesquisaQualidadeDoAtendimento(leadDTO, baseUrl);
	        }
        } else if (Status.EM_ATRASO.equals(leadDTO.getStatus()) || Status.REPESCAGEM.equals(leadDTO.getStatus())){
        	lead.setRepescado(Status.REPESCAGEM.equals(leadDTO.getStatus()));
        	leadDTO.setStatus(Status.DISPONIVEL);
        	leadDTO.setDataEncaminhamento(ZonedDateTime.now(ZoneId.of("America/Sao_Paulo")));
        	lead.setStatus(leadDTO.getStatus());
        	lead.setDataEncaminhamento(leadDTO.getDataEncaminhamento());
        	lead.setResponsavel(this.atribuirLeadAoMelhorRankeado(leadDTO));
        } else {
        	lead.setResponsavel(user.get());
        	lead.setResponsavelOriginal(user.get().getId());
        	leadDTO.setIdResponsavelOriginal(user.get().getId());
        	notificacaoService.inserirNotificacao(leadDTO, StatusPendencia.INFO);
        }
        leadRepository.saveAndFlush(lead);
        log.debug("Updated Information for Lead: {}", lead);
        return lead;
        
		}).orElseGet(null);
	}

	private User atualizarPlantaoERankingVendedor(LeadDTO leadDTO) {
		return userRepository.findOneById(leadDTO.getResponsavel().getIdUser()).map(user -> {
		Ranking rankingVendedor = new Ranking();
		rankingVendedor.setDataCompetencia(ZonedDateTime.now());
		rankingVendedor.setPontos(Status.VENDIDO.equals(leadDTO.getStatus()) ? 1L : 0L);
		rankingVendedor.setQuantidadeVendas(Status.VENDIDO.equals(leadDTO.getStatus()) ? 1L :  0L);
		rankingVendedor.setValorVendas(Status.VENDIDO.equals(leadDTO.getStatus()) ? leadDTO.getValor() :  BigDecimal.ZERO);
		rankingVendedor.setUsuario(user);
		rankingRepository.save(rankingVendedor);
        userRepository.save(user);
        Optional<Evento> plantao = eventoRepository.findAllByOwnerId(user.getId()).stream().filter(evento -> evento.getStart().getDayOfYear()== ZonedDateTime.now().getDayOfYear()).findFirst();
        if (plantao.isPresent()) {
        	Evento plantaoDoDia = plantao.get();
	        plantaoDoDia.setQuantidadeIndicacoes(plantaoDoDia.getQuantidadeIndicacoes() + 1L);
	        plantaoDoDia.setQuantidadeLeadsIndividual(TipoPlano.INDIVIDUAL.equals(leadDTO.getTipoPlano()) ?  plantaoDoDia.getQuantidadeLeadsIndividual() + 1L : plantaoDoDia.getQuantidadeLeadsIndividual());
	        plantaoDoDia.setQuantidadeLeadsFamiliar(TipoPlano.FAMILIAR.equals(leadDTO.getTipoPlano()) ?  plantaoDoDia.getQuantidadeLeadsFamiliar() + 1L : plantaoDoDia.getQuantidadeLeadsFamiliar());
	        plantaoDoDia.setQuantidadeLeadsPme(TipoPlano.PME.equals(leadDTO.getTipoPlano()) ?  plantaoDoDia.getQuantidadeLeadsPme() + 1L : plantaoDoDia.getQuantidadeLeadsPme());
	        plantaoDoDia.setQuantidadeLeadsCorporativo(TipoPlano.EMPRESARIAL.equals(leadDTO.getTipoPlano()) ?  plantaoDoDia.getQuantidadeLeadsCorporativo() + 1L : plantaoDoDia.getQuantidadeLeadsCorporativo());
	        eventoRepository.saveAndFlush(plantaoDoDia);
        }
	        
        log.debug("Created Sale Information for User: {}", user);
        return user;
		}).orElseGet(null);
	}

	private boolean isDiretoria(LeadDTO leadDTO) {
		if (leadDTO.getFromAuthority().equals("DIRETOR") || leadDTO.getFromAuthority().equals("GERENTE")) {
			return true;
		}
		return false;
	}

}
