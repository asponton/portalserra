package br.com.serracorretora.plataformaserra.service.dto;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

import br.com.serracorretora.plataformaserra.domain.Faixa;
import br.com.serracorretora.plataformaserra.domain.Lead;
import br.com.serracorretora.plataformaserra.domain.Lead.Status;
import br.com.serracorretora.plataformaserra.domain.Lead.TipoPlano;
import br.com.serracorretora.plataformaserra.domain.User;

public class LeadDTO {
	
	private Long id;

    private String nome;

    private String email;

    private Long ddd;
    
    private String telefone;
    
    private String motivoStatus;
    
    private TipoPlano tipoPlano;
    
    private String operadora;

    private Lead.Status status;

    private ZonedDateTime dataEntrada;
    
    private ZonedDateTime dataAlteracaoStatus;
    
    private ZonedDateTime dataEncaminhamento;
    
    private ZonedDateTime dataRetorno;
    
    private Faixa faixaEtaria;
    
    private String observacoes;
    
    private UserDTO responsavel;
    
    private Long idResponsavelOriginal;
    
    private boolean isLimiteAtingido;
    
    private String fromAuthority;
    
    private BigDecimal valor;
    
    private boolean repescado;
    
    private boolean leadTransferido;
        
    public LeadDTO() {
        super();
    }

    public LeadDTO(Lead lead) {
        this(lead.getId(), lead.getNome(), lead.getEmail(), lead.getDdd(), lead.getTelefone(),
                lead.getMotivoStatus(), lead.getTipoPlano(), lead.getOperadora(), lead.getStatus(), lead.getDataEntrada(),
                lead.getDataAlteracaoStatus(), lead.getDataEncaminhamento(), lead.getDataRetorno(),
                lead.getFaixaEtaria(), lead.getObservacoes(), lead.getValor(),
                Optional.ofNullable(lead.getResponsavel()), lead.getResponsavelOriginal(), lead.isRepescado());
    }

    public LeadDTO(Long id, String nome, String email, Long ddd, String telefone, String motivoStatus, 
    		TipoPlano tipoPlano, String operadora, Lead.Status status, ZonedDateTime dataEntrada, ZonedDateTime dataAlteracaoStatus, ZonedDateTime dataEncaminhamento,
    		ZonedDateTime dataRetorno, Faixa faixaEtaria, String observacoes, BigDecimal valor, Optional<User> responsavel, Long idResponsavelOriginal, boolean repescado) {
        super();
        this.id = id;
        this.nome = nome;
        this.email = email;
        this.ddd = ddd;
        this.telefone = telefone;
        this.motivoStatus = motivoStatus;
        this.tipoPlano = tipoPlano;
        this.operadora = operadora;
        this.status = status;
        this.dataEntrada = dataEntrada;
        this.dataAlteracaoStatus = dataAlteracaoStatus;
        this.dataEncaminhamento = dataEncaminhamento;
        this.dataRetorno = dataRetorno;
        this.faixaEtaria = faixaEtaria;
        this.observacoes = observacoes;
        this.valor = valor;
        this.responsavel = responsavel.map(UserDTO::new).orElse(null);
        this.idResponsavelOriginal = idResponsavelOriginal;
        this.isLimiteAtingido = this.isTempoLimiteAtingido();
        this.fromAuthority = "";
        this.repescado = repescado;
        this.leadTransferido = this.responsavel.getIdUser().equals(this.idResponsavelOriginal);
    }
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getDdd() {
		return ddd;
	}

	public void setDdd(Long ddd) {
		this.ddd = ddd;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getMotivoStatus() {
		return motivoStatus;
	}

	public void setMotivoStatus(String motivoStatus) {
		this.motivoStatus = motivoStatus;
	}

	public TipoPlano getTipoPlano() {
		return tipoPlano;
	}

	public void setTipoPlano(TipoPlano tipoPlano) {
		this.tipoPlano = tipoPlano;
	}

	public Lead.Status getStatus() {
		return status;
	}
	
	public void setStatus(Lead.Status status) {
		this.status = status;
	}

	public ZonedDateTime getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(ZonedDateTime dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public ZonedDateTime getDataAlteracaoStatus() {
		return dataAlteracaoStatus;
	}

	public void setDataAlteracaoStatus(ZonedDateTime dataAlteracaoStatus) {
		this.dataAlteracaoStatus = dataAlteracaoStatus;
	}

	public ZonedDateTime getDataEncaminhamento() {
		return dataEncaminhamento;
	}

	public void setDataEncaminhamento(ZonedDateTime dataEncaminhamento) {
		this.dataEncaminhamento = dataEncaminhamento;
	}

	public ZonedDateTime getDataRetorno() {
		return dataRetorno;
	}

	public void setDataRetorno(ZonedDateTime dataRetorno) {
		this.dataRetorno = dataRetorno;
	}

	public Faixa getFaixaEtaria() {
		return faixaEtaria;
	}

	public void setFaixaEtaria(Faixa faixaEtaria) {
		this.faixaEtaria = faixaEtaria;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public UserDTO getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(UserDTO responsavel) {
		this.responsavel = responsavel;
	}
	
	public Long getIdResponsavelOriginal() {
		return idResponsavelOriginal;
	}

	public void setIdResponsavelOriginal(Long idResponsavelOriginal) {
		this.idResponsavelOriginal = idResponsavelOriginal;
	}

	public String getDescricaoStatus(){
		return this.status.getDescricao();
	}

	public String getFromAuthority() {
		return fromAuthority;
	}

	public void setFromAuthority(String fromAuthority) {
		this.fromAuthority = fromAuthority;
	}

	public boolean isLimiteAtingido() {
		return isLimiteAtingido;
	}

	public void setLimiteAtingido(boolean isLimiteAtingido) {
		this.isLimiteAtingido = isLimiteAtingido;
	}
	
	public String getOperadora() {
		return operadora;
	}

	public void setOperadora(String operadora) {
		this.operadora = operadora;
	}	

	public BigDecimal getValor() {
		return valor;
	}

	public boolean isRepescado() {
		return repescado;
	}

	public boolean isLeadTransferido() {
		return leadTransferido;
	}

	public void setLeadTransferido(boolean leadTransferido) {
		this.leadTransferido = leadTransferido;
	}

	public void setRepescado(boolean repescado) {
		this.repescado = repescado;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	private boolean isTempoLimiteAtingido() {
    	boolean atingiu = false;
    	ZonedDateTime agora = ZonedDateTime.now(ZoneId.of("America/Sao_Paulo"));
    	switch (this.status) {
		case DISPONIVEL:
				if (this.dataAlteracaoStatus != null) {
					atingiu = agora.minusHours(1).isAfter(this.dataAlteracaoStatus);
				} else {
					atingiu = agora.minusHours(1).isAfter(this.dataEncaminhamento);
				}
			break;
		case EM_ANDAMENTO:
			atingiu =  agora.minusDays(2).isAfter(this.dataRetorno) && agora.minusDays(1).isAfter(this.dataAlteracaoStatus);
			break;
		default:
			break;
    	}
    	if (atingiu) {
    		this.status = Status.EM_ATRASO;
    	}
    	return atingiu;
    }
    
}
