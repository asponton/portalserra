package br.com.serracorretora.plataformaserra.service.dto;

import java.math.BigDecimal;

public class DadosGraficoGauge {
	
	private BigDecimal value;
	
	private String bgColor;
	
	private String bgAlpha;
	
	private String tooltext;
	
	public DadosGraficoGauge(BigDecimal value, String bgColor, String bgAlpha, String tooltext) {
		super();
		this.value = value;
		this.bgColor = bgColor;
		this.bgAlpha = bgAlpha;
		this.tooltext = tooltext + ": $value%";
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getBgColor() {
		return bgColor;
	}

	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}

	public String getBgAlpha() {
		return bgAlpha;
	}

	public void setBgAlpha(String bgAlpha) {
		this.bgAlpha = bgAlpha;
	}

	public String getTooltext() {
		return tooltext;
	}

	public void setTooltext(String tooltext) {
		this.tooltext = tooltext;
	}


}
