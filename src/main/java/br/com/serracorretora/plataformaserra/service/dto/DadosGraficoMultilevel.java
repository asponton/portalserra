package br.com.serracorretora.plataformaserra.service.dto;

public class DadosGraficoMultilevel {
	
	private String label;
	
	private String color;
	
	private Long value;

	public DadosGraficoMultilevel(String nome, String cor, Long quantidadeVendas) {
		super();
		this.label = nome;
		this.color = cor;
		this.value = quantidadeVendas;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

}
